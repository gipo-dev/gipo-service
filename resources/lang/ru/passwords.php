<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен содержать не менее 6-ти символов и должен быть повторен.',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Инструкция для сброса пароля была отправлена вам на почту!',
    'token' => 'Токен для сброса пароля неверен.',
    'user' => "Пользователь с таким E-mail не найден.",

];
