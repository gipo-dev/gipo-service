@extends('layouts.default')

@section('title') Вход | gipo.ru @endsection
@section('description') Войти в личный кабинет сервиса гипо.  @endsection

@section('content')
  <div class="bg-gradient-primary reg row" style="height: calc(100vh - 60px)">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
          <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
              <!-- Nested Row within Card Body -->
              <div class="row">
                <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                <div class="col-lg-6">
                  <div class="p-5" style="padding-top: 6rem !important;padding-bottom: 6rem !important;">
                    <div class="text-center">
                      <h1 class="h4 text-gray-900 mb-4">Добро пожаловать!</h1>
                    </div>
                    <form class="user" method="POST" action="{{ route('login') }}">
                      {{ csrf_field() }}
                      <div class="form-group">
                        <input type="email"
                               class="form-control form-control-user{{ $errors->has('email') ? '  is-invalid' : '' }}"
                               name="email"
                               aria-describedby="emailHelp" placeholder="Введите ваш Email...">
                        @if ($errors->has('email'))
                          <div class="invalid-feedback">
                            {{ $errors->first('email') }}
                          </div>
                        @endif
                      </div>
                      <div class="form-group">
                        <input type="password"
                               class="form-control form-control-user{{ $errors->has('password') ? ' is-invalid' : '' }}"
                               name="password"
                               placeholder="Пароль">
                        @if ($errors->has('password'))
                          <div class="invalid-feedback">
                            {{ $errors->first('password') }}
                          </div>
                        @endif
                      </div>
                      <div class="form-group">
                        <div class="custom-control custom-checkbox small">
                          <input type="checkbox" class="custom-control-input" name="remember" id="rememberMe">
                          <label class="custom-control-label" for="rememberMe" {{ old('remember') ? 'checked' : '' }}>Запомнить
                            меня</label>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary btn-user btn-block">
                        Вход
                      </button>
                    </form>
                    <hr>
                    <div class="text-center">
                      <a class="small" href="{{ route('password.request') }}">Забыли пароль?</a>
                    </div>
                    <div class="text-center">
                      <a class="small" href="/register">Создать новый аккаунт</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
