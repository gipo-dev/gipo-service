@extends('layouts.default')

@section('title') Регистрация | gipo.ru @endsection
@section('description') Регистрация в сервисе гипо.ру. Готовый интернет-магазин за 24 часа @endsection

@section('content')
  <div class="bg-gradient-primary reg row" style="height: calc(100vh - 60px)">
    <div class="container">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <div class="row">
            <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
            <div class="col-lg-7">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Присоединяйтесь к нам!</h1>
                </div>
                <form class="user" method="POST" action="{{ route('register') }}">
                  {{ csrf_field() }}
                  <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <input type="text"
                             class="form-control form-control-user{{ $errors->has('first_name') ? '  is-invalid' : '' }}"
                             name="first_name" placeholder="Имя" value="{{ old('first_name') }}" required>
                      @if ($errors->has('first_name'))
                        <div class="invalid-feedback">
                          {{ $errors->first('first_name') }}
                        </div>
                      @endif
                    </div>
                    <div class="col-sm-6">
                      <input type="text"
                             class="form-control form-control-user{{ $errors->has('second_name') ? '  is-invalid' : '' }}"
                             name="second_name" placeholder="Фамилия" value="{{ old('second_name') }}" required>
                      @if ($errors->has('second_name'))
                        <div class="invalid-feedback">
                          {{ $errors->first('second_name') }}
                        </div>
                      @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <input type="email"
                             class="form-control form-control-user{{ $errors->has('email') ? '  is-invalid' : '' }}"
                             name="email" placeholder="Email адрес" value="{{ old('email') }}" required>
                      @if ($errors->has('email'))
                        <div class="invalid-feedback">
                          {{ $errors->first('email') }}
                        </div>
                      @endif
                    </div>
                    <div class="col-sm-6">
                      <select name="phone_code" class="form-control{{ $errors->has('phone') ? '  is-invalid' : '' }}" required style="float: left;
                                                                                              width: 66px;
                                                                                              height: 50px;
                                                                                              border-bottom-left-radius: 10rem;
                                                                                              border-top-left-radius: 10rem;">
                        <option value="+7" selected>+7</option>
                      </select>
                      <input type="text"
                             class="form-control form-control-user{{ $errors->has('phone') ? '  is-invalid' : '' }}"
                             name="phone" placeholder="Номер телефона" minlength="10" maxlength="10" value="{{ old('phone') }}" required style="float: left;
                                                                                                                     width: calc(100% - 66px);
                                                                                                                     border-bottom-left-radius: 0;
                                                                                                                     border-top-left-radius: 0;
                                                                                                                     border-left: none;">
                      @if ($errors->has('phone'))
                        <div class="invalid-feedback">
                          {{ $errors->first('phone') }}
                        </div>
                      @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <input type="password" name="password" class="form-control form-control-user{{ $errors->has('password') ? '  is-invalid' : '' }}" placeholder="Пароль" required>
                      @if ($errors->has('password'))
                        <div class="invalid-feedback">
                          {{ $errors->first('password') }}
                        </div>
                      @endif
                    </div>
                    <div class="col-sm-6">
                      <input type="password" name="password_confirmation" class="form-control form-control-user{{ $errors->has('password_confirmation') ? '  is-invalid' : '' }}" placeholder="Повторите пароль" required>
                      @if ($errors->has('password_confirmation'))
                        <div class="invalid-feedback">
                          {{ $errors->first('password_confirmation') }}
                        </div>
                      @endif
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">
                    Зарегистрироваться
                  </button>
                </form>
                <hr>
                <div class="text-center">
                  <a class="small" href="/password/reset">Забыли пароль?</a>
                </div>
                <div class="text-center">
                  <a class="small" href="/login">Уже есть аккаунт?</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @push('styles')
    <style>
      .phone select {
        float: left;
        width: 64px;
        border-bottom-right-radius: 0;
        border-top-right-radius: 0;
      }

      .phone input {
        float: left;
        width: calc(100% - 64px);
        border-bottom-left-radius: 0;
        border-top-left-radius: 0;
        border-left: none;
      }
    </style>
  @endpush
@endsection
