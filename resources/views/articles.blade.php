@extends('layouts.landingapp')

@push('title') Статьи | гипо.ру @endpush

@section('content')
  <section class="hero-wrap hero-wrap-2" style="background-image: url('https://images.unsplash.com/photo-1517976384346-3136801d605d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80');"
           data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-2 bread">Статьи</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="/">Главная <i class="ion-ios-arrow-forward"></i></a></span>
            <span>Статьи <i class="ion-ios-arrow-forward"></i></span></p>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="row">
            @foreach($articles as $article)
              <div class="col-md-6 ftco-animate">
                <div class="blog-entry">
                  <a href="{{ route('article', $article->id) }}" class="block-20"
                     style="background-image: url('{{ $article->image }}');"></a>
                  <div class="text d-flex py-4">
                    <div class="meta mb-3">
                      <div><a>{{ Date::parse($article->created_at)->format('d, M. Y') }}</a></div>
                      <div><a>{{ $article->author_name }}</a></div>
                      <div><a class="meta-chat"><span class="icon-chat"></span> {{ $article->comments->count() }}</a></div>
                    </div>
                    <div class="desc pl-3">
                      @if(!isset(request()->category))
                        <div class="article-category"><a class="">{{ $article->category->name or '' }}</a></div>
                      @endif
                      <h3 class="heading"><a href="{{ route('article', $article->id) }}">{{ $article->title }}</a></h3>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="row no-gutters my-5">
            {{--<div class="col text-center">--}}
            {{--<div class="block-27">--}}
            {{--<ul>--}}
            {{--<li><a href="#">&lt;</a></li>--}}
            {{--<li class="active"><span>1</span></li>--}}
            {{--<li><a href="#">2</a></li>--}}
            {{--<li><a href="#">3</a></li>--}}
            {{--<li><a href="#">4</a></li>--}}
            {{--<li><a href="#">5</a></li>--}}
            {{--<li><a href="#">&gt;</a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{ $articles->appends($_GET)->links() }}
          </div>
        </div>
        <div class="col-lg-4 sidebar ftco-animate">
          <div class="sidebar-box ftco-animate">
            <h3>Категория</h3>
            <ul class="categories">
              <li>
                <a href="{{ route('articles') }}">Все статьи</a>
              </li>
              @foreach($categories as $category)
                <li>
                  <a href="{{ route('articles', ['category' => $category->id]) }}">{{ $category->name }} <span>({{ $category->articles->count() }})</span></a>
                </li>
              @endforeach
            </ul>
          </div>
          <div class="sidebar-box ftco-animate">
            <h3>Популярные статьи</h3>
            @foreach($populars as $article)
              <div class="block-21 mb-4 d-flex">
                <a href="{{ route('article', $article->id) }}" class="blog-img mr-4" style="background-image: url('{{ $article->image }}');"></a>
                <div class="text">
                  <h3 class="heading"><a href="{{ route('article', $article->id) }}">{{ $article->title }}</a>
                  </h3>
                  <div class="meta">
                    <div><a><span
                            class="icon-calendar"></span> {{ Date::parse($article->created_at)->format('d, M. Y') }}</a>
                    </div>
                    <div><a><span class="icon-person"></span> {{ $article->author_name }}</a></div>
                    <div><a><span class="icon-chat"></span> {{ $article->comments->count() }}</a></div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="sidebar-box ftco-animate">
            <h3>Облако тегов</h3>
            <ul class="tagcloud m-0 p-0">
              @foreach($tags as $tag)
                <a href="{{ route('articles', ['tag' => $tag->id]) }}" class="tag-cloud-link">{{ $tag->name }}</a>
              @endforeach
            </ul>
          </div>
          {{--<div class="sidebar-box ftco-animate">--}}
          {{--<h3>Архив</h3>--}}
          {{--<ul class="categories">--}}
          {{--<li><a href="#">December 2018 <span>(30)</span></a></li>--}}
          {{--<li><a href="#">Novemmber 2018 <span>(20)</span></a></li>--}}
          {{--<li><a href="#">September 2018 <span>(6)</span></a></li>--}}
          {{--<li><a href="#">August 2018 <span>(8)</span></a></li>--}}
          {{--</ul>--}}
          {{--</div>--}}
          {{--<div class="sidebar-box ftco-animate">--}}
          {{--<h3>Paragraph</h3>--}}
          {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate--}}
          {{--quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos--}}
          {{--fugit cupiditate numquam!</p>--}}
          {{--</div>--}}
        </div>
      </div>
    </div>
  </section>
@endsection

@push('styles')
  <style>
    .article-category > a {
      color: #b3b3b3;
      font-size: 13px;
    }
  </style>
@endpush