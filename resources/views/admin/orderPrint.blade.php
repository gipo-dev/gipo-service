@extends('admin.layouts.app')

@section('title') Счет №{{ $order->id }} @endsection

@section('content')
  <div style="page-break-after: always;">
    <h1>Счет #{{ $order->id }}</h1>
    <table class="table table-bordered">
      <thead>
      <tr>
        <td colspan="2">Детали заказа</td>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td style="width: 50%;"><address>
            <strong>{{ $order->order_data->store_name }}</strong><br>
            {{--Адрес--}}
          </address>
          <b>Телефон</b> +7 812 951-78-71<br>
          {{--<b>E-Mail</b> $order->order_data-> <br>--}}
          <b>Веб-сайт:</b> <a href="{{ $order->order_data->store_url }}">{{ $order->order_data->store_url }}</a></td>
        <td style="width: 50%;"><b>Дата добавления</b> {{ $order->created_at }}<br>
          <b>№ заказа:</b> {{ $order->id }}<br>
          <b>Способ оплаты</b> {{ $order->order_data->shipping_method }}<br>
          <b>Способ доставки</b> {{ $order->order_data->payment_method }}<br>
        </td>
      </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
      <thead>
      <tr>
        <td style="width: 50%;"><b>Адрес оплаты</b></td>
        <td style="width: 50%;"><b>Адрес доставки</b></td>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td><address>
            {{ $order->order_data->payment_firstname }} {{ $order->order_data->payment_lastname }}<br>
            {{ $order->order_data->payment_address_1 }}<br>
            {{ $order->order_data->payment_city }} {{ $order->order_data->payment_postcode }}<br>
            {{ $order->order_data->payment_country }}
          </address></td>
        <td><address>
            {{ $order->order_data->shipping_firstname }} {{ $order->order_data->shipping_lastname }}<br>
            {{ $order->order_data->shipping_address_1 }}<br>
            {{ $order->order_data->shipping_city }} {{ $order->order_data->shipping_postcode }}<br>
            {{ $order->order_data->shipping_country }}
          </address></td>
      </tr>
      </tbody>
    </table>
    <table class="table table-bordered">
      <thead>
      <tr>
        <td><b>Товар</b></td>
        <td><b>Код товара</b></td>
        <td class="text-right"><b>Количество</b></td>
        <td class="text-right"><b>Цена за единицу товара</b></td>
        <td class="text-right"><b>Итого</b></td>
      </tr>
      </thead>
      <tbody>
      @foreach($order->order_data->products as $product)
        <tr>
          <td>{{ $product->name }}</td>
          <td>{{ $product->model }}</td>
          <td class="text-right">{{ $product->quantity }}</td>
          <td class="text-right">{{ $product->price }}р.</td>
          <td class="text-right">{{ $product->total }}р.</td>
        </tr>
      @endforeach
      <tr>
        <td class="text-right" colspan="4"><b>Итого</b></td>
        <td class="text-right">{{ $order->order_data->totals[0]->value }}р.</td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>{{ $order->order_data->shipping_method }}</b></td>
        <td class="text-right">{{ $order->order_data->shipping_method == 'Доставка с фиксированной стоимостью' ? '290' : '0' }}р.</td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>Всего</b></td>
        <td class="text-right">{{ $order->order_data->totals[0]->value + ($order->order_data->shipping_method == 'Доставка с фиксированной стоимостью' ? 290 : 0) }}р.</td>
      </tr>
      </tbody>
    </table>
  </div>
@endsection

@push('styles')
  <style>
    #accordionSidebar, .topbar {
      display: none;
    }
  </style>
@endpush

@push('scripts')
  <script>
      window.print();
  </script>
@endpush