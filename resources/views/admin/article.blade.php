@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Редактирование статьи</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Статья</h6>
    </div>
    <div class="card-body">
      <form action="" method="POST" id="form-article" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row mb-3">
          <div class="col-lg-6">
            {{--Заголовок--}}
            <label class="control-label" for="title">Заголовок статьи:</label>
            <input type="text" name="title" id="title" value="{{ $article->title or '' }}" placeholder="Новая статья"
                   class="form-control" maxlength="100" required>
            {{--Изображение--}}
            <div class="form-group mt-3">
              <div>
                <label class="control-label" for="image">Изображение:</label>
              </div>
              @if(isset($article->image))
                <img src="{{ $article->image }}" alt="..." class="img-thumbnail">
                <div>
                  <label for="image" class="help-block"
                         style="text-decoration: underline;font-weight: 400;cursor: pointer;">Заменить
                    изображение</label>
                  <input type="file" id="image" name="image" accept="image/*" hidden class="hidden">
                </div>
              @else
                <input type="file" id="image" name="image" accept="image/*" required>
              @endif
            </div>
            {{--Категория--}}
            <div class="form-group mt-3">
              <label class="control-label">Категория:</label>
              <div>
                <select class="custom-select" name="category_id" required>
                  <option>Выбрать категорию</option>
                  @foreach($categories as $cat)
                    <option value="{{ $cat->id }}"
                    @if($article->category_id)
                      {{ $article->category_id == $cat->id ? 'selected' : '' }}
                        @endif
                    >{{ $cat->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            {{--Описание--}}
            <div class="form-group mt-3">
              <label class="control-label" for="input-description">Описание:</label>
              <div>
                <textarea name="description" rows="3" id="input-description" class="form-control" maxlength="150"
                          required>{{ $article->description or '' }}</textarea>
              </div>
            </div>
            {{--Теги--}}
            <div class="form-group mt-3">
              <label class="control-label">Теги:</label>
              <div>
                <select class="custom-select" name="tags[]" multiple>
                  @foreach($tags as $tag)
                    <option
                        value="{{ $tag->id }}" {{ isset($article->tags[$tag->id]) ? 'selected' : '' }}>{{ $tag->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        </div>
        {{--Текст--}}
        <label class="control-label" for="title">Статья:</label>
        <h2 id="h1">{{ $article->title or '' }}</h2>
        <input type="text" value="{{ $article->text or '' }}" hidden name="text">
        <div id="article">{!! $article->text or '' !!}</div>
        <button type="submit">Сохранить</button>
      </form>
    </div>
  </div>
@endsection

@push('scripts')
  <script>
      var editor = new FroalaEditor('#article', {
          // toolbarInline: true,
          placeholderText: 'Начните писать статью...',
          imageUploadParam: 'file',
          imageUploadMethod: 'post',
          imageUploadURL: '',
          imageUploadParams: {
              froala: 'true', // This allows us to distinguish between Froala or a regular file upload.
              _token: "{{ csrf_token() }}" // This passes the laravel token with the ajax request.
          },
          events: {
              'image.error': function (error, response) {
                  // Do something here.
                  // this is the editor instance.
                  console.log(response);
              }
          }
      });
      $('#title').change(function (e) {
          $('#h1').text($(this).val());
      });
      $('#form-article').submit(function (el) {
          $('input[name=text]').val(editor.html.get());
      });
  </script>
@endpush