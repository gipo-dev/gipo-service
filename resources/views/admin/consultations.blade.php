@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Заявки с лендинга</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Список заявок</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>№</th>
            <th>Имя</th>
            <th>Почтовый адрес</th>
            <th>Телефон</th>
            <th>Комментарий</th>
            <th>Статус</th>
            <th>Дата создания</th>
          </tr>
          </thead>
          <tbody>
          @foreach($consultationRequests as $req)
            <tr class="{{ $req->status_id == 1 ? 'border-left-success' : '' }}">
              <td>{{ $req->id }}</td>
              <td>{{ $req->name }}</td>
              <td>{{ $req->email }}</td>
              <td>{{ $req->phone }}</td>
              <td>{{ $req->comment }}</td>
              <td>{{ $req->status_name }}</td>
              <td>{{ $req->created_at->diffForHumans() }}</td>
              <td>
                <div class="btn-group">
                  <a href="{{ route('admin.consultation', ['id' => $req->id]) }}" class="btn btn-primary"><span
                      class="fa fa-eye" aria-hidden="true"></span></a>
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Список</span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#"><span class="fa fa-eye-dashed" aria-hidden="true"></span> Скрыть</a>
                    </li>
                  </ul>
                </div>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $consultationRequests->links() }}
      </div>
    </div>
  </div>
@endsection