@extends('admin.layouts.app')

@section('content')
  <h2 class="h3 mb-4 text-gray-800">Управление категориями</h2>
  <ul class="list-group">
    @include('admin.partials.category', ['categories' => $categories])
    <div class="mt-2">
      <a href="{{ route('admin.category.edit') }}" class="btn btn-dark text-xs">Добавить сюда</a>
    </div>
  </ul>
@endsection