@extends('admin.layouts.app')

@section('content')
  <h2 class="h3 mb-4 text-gray-800">
    {{ $category->name or 'Добавление категории' }}
    ({{ $category->id or '' }})
    @if(isset($category->id))
      <a href="{{ route('admin.category.delete', ['id' => $category->id]) }}" class="float-right btn btn-xs btn-danger">
        <span class="fa fa-trash-alt"></span>
      </a>
    @endif
  </h2>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row">
    <div>
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-user"></span> Категория</h6>
        </div>
        <div class="card-body">
          <form action="" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="name">Имя*</label>
              <input type="text" class="form-control" id="name" placeholder="Паровые станции" name="name" value="{{ $category->name or '' }}" required>
            </div>
            <div class="form-group">
              <label for="alias">Алиас*</label>
              <input type="text" class="form-control" id="alias" placeholder="parovye-stancii" name="alias" value="{{ $category->alias or '' }}" required>
            </div>
            <div class="form-group">
              <label>Родительская категория</label>
              <select name="parent_id" class="form-control">
                <option value="0">0 Не выбрана</option>
                @foreach($categories as $cat)
                  @if(isset($category->parent_id))
                    <option value="{{ $cat->id }}" {{ $category->parent_id == $cat->id ? 'selected' : '' }}>{{ $cat->name }} {{ $cat->id }}</option>
                  @else
                    <option value="{{ $cat->id }}" {{ request()->parent_id == $cat->id ? 'selected' : '' }}>{{ $cat->name }} {{ $cat->id }}</option>
                  @endif
                  <option value="{{ $cat->id }}">{{ $cat->id }} {{ $cat->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="custom-control custom-checkbox small">
              <input type="checkbox" class="custom-control-input" id="active" name="active" {{ (isset($category->active) ? $category->active : 1 ) ? 'checked' : '' }}>
              <label class="custom-control-label" for="active">Включена</label>
            </div>

            <button type="submit" class="btn btn-success float-right">
              <span class="fa fa-save"></span> Сохранить
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection