@extends('admin.layouts.app')

@section('content')
  <div class="row">
    <div class="mb-4 col-md-8 col-xl-9">
      <div class="shadow card">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Список товаров</h6>
        </div>
        <div class="card-body">
          @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
          @endif
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
              <tr>
                <th></th>
                <th>id</th>
                <th>Наименование</th>
                <th>Вкл</th>
                <th>Остаток</th>
                <th>Приход</th>
                <th>Опт</th>
                <th>Агентская</th>
                <th>РРЦ</th>
                <th>ЯМ</th>
                <th>Разница</th>
              </tr>
              </thead>
              <tbody>
              @foreach($products as $product)
                <tr
                  class="{{ $product->yandex_minimal_price > 0 && $product->agent_price > 0 && $product->agent_price / $product->yandex_minimal_price > 1 ? '' : 'border-right-success' }}">
                  <td>
                    <img src="{{ $product->image }}" alt="" style="max-height: 50px;max-width: 50px;">
                  </td>
                  <td>{{ $product->id }}</td>
                  <td>{{ $product->name }} <span style="font-size: .8em;"><i>{{ $product->provider_info->provider_id or 'нет' }}</i> {{ $product->provider_info->name or 'нет' }}</span></td>
                  <td>
                    <select data-gipo-id="{{ $product->id }}" class="custom-select status_select" style="width: 70px;">
                      <option value="0" {{ $product->enabled ? '' : 'selected' }}>Нет</option>
                      <option value="1" {{ $product->enabled ? 'selected' : '' }}>Да</option>
                    </select>
                  </td>
                  <td>{{ $product->count }}</td>
                  <td>{{ $product->in_transit }}</td>
                  <td>{{ $product->opt_price }}</td>
                  <td>{{ $product->agent_price }}</td>
                  <td>{{ $product->recommended_price }}</td>
                  <td>{{ $product->yandex_minimal_price }}</td>
                  <td>{{ $product->yandex_minimal_price > 0 && $product->agent_price > 0 ? (100 * round(1 - ($product->agent_price / $product->yandex_minimal_price), 2)) : 0 }}% ({{ $product->agent_price - $product->yandex_minimal_price }})</td>
                  <td>
                    <a href="https://market.yandex.ru/product--/{{ $product->yandex_id or '' }}" target="_blank" class="fab fa-yandex btn btn-primary btn-circle btn-sm"></a>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
            {{ $products->appends($_GET)->links() }}
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-cog"></span> Фильтр</h6>
        </div>
        <div class="card-body">
          <form action="">
            <div class="form-group">
              <label for="id">ID</label>
              <input type="number" class="form-control" id="id" name="id" min="1" value="{{ request('id') or '' }}">
            </div>
            <div class="form-group">
              <label for="name">Наименование</label>
              <input type="text" class="form-control" id="name" name="name" minlength="3" value="{{ request('name') }}">
            </div>
            <div class="form-group">
              <label for="method">Сортировать</label>
              <select class="custom-select" name="method" id="method">
                <option>Стандартно</option>
                <option value="1" {{ request('method') == 1 ? 'selected' : '' }}>Наибольший заработок, %</option>
                <option value="2" {{ request('method') == 2 ? 'selected' : '' }}>Наибольший заработок, Р</option>
              </select>
            </div>
            <button class="btn btn-primary">Показать</button>
            <a href="{{ route('admin.product.list') }}" class="btn btn-danger"><i class="fa fa-times"></i></a>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script>
    $('.status_select').change(function (e) {
        var id = $(this).data('gipo-id');
        var val = $(this).val();
        $.ajax({
            url: '',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                product_id: id,
                enabled: val,
            },
            success: function (resp) {
                console.log(resp);
            },
            error: function (resp) {
                console.log(resp);
            }
        });
    });
  </script>
@endpush