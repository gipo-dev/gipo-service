@extends('admin.layouts.app')

@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">Панель управления</div>

    <div class="panel-body">
      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif

      {{Auth::user()->first_name}}, добро пожаловать обратно!
    </div>
  </div>
@endsection