@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление заказами</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Список заказов</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        @verbatim
          <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
              <th>№ Заказа</th>
              <th>Покупатель</th>
              <th>Статус</th>
              <th>Итого</th>
              <th>Дата добавления</th>
              <th>Дата обновления</th>
              <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="order in orders" :class="{ 'bg-success text-white' : order.status_id == 1 }">
              <td>{{ order.id }}</td>
              <td>{{ order.name }}</td>
              <td>{{ order.status_name }}</td>
              <td>{{ order.total }}</td>
              <td>{{ order.created_at }}</td>
              <td>{{ order.updated_at }}</td>
              <td>
                <div class="btn-group">
                  <a :href="'/admin/order/' + order.id" class="btn btn-primary"><span class="fa fa-eye" aria-hidden="true"></span></a>
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Список</span>
                  </button>
                  <ul class="dropdown-menu">
                    <a href="#" class="dropdown-item"><span class="fa fa-eye-slash" aria-hidden="true"></span> Скрыть</a>
                  </ul>
                </div>
              </td>
            </tr>
            </tbody>
          </table>
        @endverbatim
        {{ $orders->links() }}
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script>
    $(function () {
        var app = new Vue({
            el: '#orders-list',
            data: function() {
                return {
                    orders: [],
                    timer: '',
                    new_orders: 0
                }
            },
            created: function () {
                this.fetchEventsList();
                this.timer = setInterval(this.fetchEventsList, 5000);
            },
            methods: {

                fetchEventsList: function() {
                    var el = this;
                    $.ajax({
                        url: '{{ route('ajax_order_list') }}',
                        type: 'GET',
                        data: {
                            page: '{{ app('request')->input('page') }}'
                        }
                    }).done(function (data) {
                        el.orders = data.data;
                        el.orders.forEach(function (elem, i) {
                            var or = elem.order_data;
                            el.orders[i].name = or.firstname +" "+ or.lastname;
                            el.orders[i].total = or.totals[2].value;
                        });
                        el.new_orders = el.orders.filter(order => order.status_id == 1).length;
                        el.updateFavicon();
                    })

                },
                cancelAutoUpdate: function() {
                    clearInterval(this.timer)
                },
                updateFavicon: function () {
                    var c = document.createElement( "canvas" ); // Используем тот же канвас
                    c.height = c.width = 16;
                    var cx = c.getContext( "2d" );
                    cx.beginPath();// рисуем голубенький квадратик и черный текст на нем
                    cx.rect( 0, 0, 16, 16 );// и помещаем их на канвас
                    cx.fillStyle = "#6DA3BD";
                    cx.fill();

                    cx.font = "12px Raleway";
                    cx.strokeStyle = "#fff";
                    cx.textAlign = 'center';
                    cx.strokeText(this.new_orders, 8, 11);
                    var oldicons = document.querySelectorAll( 'link[rel="icon"], link[rel="shortcut icon"]' );
                    for(var i = 0; i < oldicons.length; i++) {
                        oldicons[i].parentNode.removeChild( oldicons[i] );
                    }

                    var newicon = document.createElement( "link" );
                    newicon.setAttribute( "rel", "icon" );
                    newicon.setAttribute( "href", c.toDataURL() );
                    document.querySelector( "head" ).appendChild( newicon );
                }

            },
            beforeDestroy() {
                clearInterval(this.timer)
            }
        });
    });
  </script>
@endpush