@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление пользователем <span
      class="btn btn-sm btn-{{ $user->status_id == 4 ? 'success' : 'primary' }}">{{ $user->status->name }}</span></h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-12 col-xl-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-user"></span> Общая информация</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <tbody>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Имя</button>
              </td>
              <td>{{ $user->first_name }} {{ $user->second_name }}</td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Почта</button>
              </td>
              <td>{{ $user->email }} <span class="text-xs">({{ $user->email_confirmed == 1 ? 'подтверждена' : 'не подтверждена' }})</span>
              </td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Телефон</button>
              </td>
              <td>{{ $user->phone }} <span class="text-xs">({{ $user->phone_confirmed == 1 ? 'подтвержден' : 'не подтвержден' }})</span>
              </td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Юр. статус</button>
              </td>
              <td>{{ $user->legal_entity == 1 ? 'Юридическое лицо' : 'Физическое лицо' }}</td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Текущий баланс</button>
              </td>
              <td>{{ $user->wallet()->balance() }}руб.</td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Зарегистрирован</button>
              </td>
              <td>{{ $user->created_at }}</td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm text-xs">Был в сети</button>
              </td>
              <td class="{{ $user->is_online ? 'text-success' : '' }}">{{ $user->is_online ? 'Онлайн' : $user->last_activity }}</td>
            </tr>
            </tbody>
          </table>
          <a href="#" class="btn btn-success btn-sm btn-icon-split ml-2">
                    <span class="icon text-white">
                      <i class="fas fa-id-card"></i>
                    </span>
            <span class="text">Паспортные данные</span>
          </a>
          <a href="#" class="btn btn-success btn-sm btn-icon-split">
                    <span class="icon text-white">
                      <i class="fas fa-folder"></i>
                    </span>
            <span class="text">Документы</span>
          </a>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-12 col-xl-8">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-money-check"></span> Информация о балансе</h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
              <tr>
                <th>№</th>
                <th>Статус</th>
                <th>Сумма</th>
                <th>Создан</th>
                <th>Ответ</th>
              </tr>
              </thead>
              <tbody>
                @if($withdrawRequests)
                  @foreach($withdrawRequests as $req)
                    <tr>
                      <td>
                        <a href="{{ route('admin.widthdrawRequest', ['id' => $req->id]) }}">{{ $req->id }}</a>
                      </td>
                      <td>{{ $req->status->name or 'Не установлен' }}</td>
                      <td>{{ $req->sum }}</td>
                      <td>{{ $req->created_at }}</td>
                      <td>{{ $req->updated_at }}</td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
            @if($withdrawRequests)
              {{ $withdrawRequests->links() }}
            @endif
          </div>
        </div>
      </div>
    </div>
    <div class="col-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-search-dollar"></span> История транзакций
          </h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
              <tr>
                <th>№ транзакции</th>
                <th>Тип транзакции (№ заказа)</th>
                <th>Сумма</th>
                <th>Дата</th>
              </tr>
              </thead>
              <tbody>
              @if(count($transactions) > 0)
                @foreach($transactions as $transaction)
                  <tr
                    class="{{ $transaction->transaction_data->summ > 0 ? "border-left-success" : "border-left-danger" }}">
                    <td>{{ $transaction->id }}</td>
                    <td>{{ $transaction->type->name or '' }}
                      @if(isset($transaction->transaction_data->transaction->website_id))
                        (<a
                          href="{{ route('admin.website.edit', ['id' => $transaction->transaction_data->transaction->website_id]) }}">
                          вебсайт id{{ $transaction->transaction_data->transaction->website_id or '' }}
                        </a>)
                        (<a
                          href="{{ route('order', ['id' => $transaction->transaction_data->transaction->id]) }}">
                          заказ id{{ $transaction->transaction_data->transaction->id or '' }}
                        </a>)
                      @endif
                    </td>
                    <td>{{ $transaction->transaction_data->summ }}</td>
                    <td>{{ $transaction->created_at }}</td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="4">
                    <h4 class="text-center">История транзакций пользователя пока что пуста</h4>
                  </td>
                </tr>
              @endif
              </tbody>
            </table>
            @if($transactions)
              {{ $transactions->links() }}
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('modals')
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-documents">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Введенные данные пользователя</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          {{--@foreach(dd($user->documents))--}}

          {{--@endforeach--}}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
        </div>
      </div>
    </div>
  </div>
@endpush

@push('scripts')
  <script>

  </script>
@endpush
@push('styles')
  <style>
    .table {
      margin-bottom: 0;
    }

    .table tr:first-child td {
      border-top: none;
    }
  </style>
@endpush