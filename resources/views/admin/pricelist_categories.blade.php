@extends('admin.layouts.app')

@section('content')
  <h2 class="h3 mb-4 text-gray-800">Прайслисты</h2>
  @verbatim
    <div class="row" style="font-size: 15px;" id="app">
      <div class="col-12">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-list"></span> Настройка категорий
            </h6>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                  <th>id прайс</th>
                  <th>Имя прайс</th>
                  <th>id бд</th>
                  <th>Имя бд</th>
                </tr>
                </thead>
                <tbody>
                  <tr v-for="(category, index) in categories" v-bind:class="{ 'border-left-danger' : category.local_category_id == null }">
                    <td>{{ category.price_category_id }}</td>
                    <td>{{ category.price_category_name }}</td>
                    <td>{{ category.local_category_id }}</td>
                    <td v-if="category.local_category_id" class="position-relative">
                      <input type="text" v-on:input="findCategory(category, index)" v-model="category.find = category.local_category_name"
                             class="form-control form-control-sm d-inline-block mr-2" style="width: 170px;">
                      < {{ category.local_parent_name }}
                      <ul class="list-unstyled list-find-category shadow" v-if="category.results && category.results.length > 0 && category.find != ''">
                        <li v-for="fc in category.results" @click="setCategoryAlias(category, fc)">
                          {{ fc.name }} < {{ fc.parent_name }}
                        </li>
                        <li @click="setCategoryAlias(category, null)"><i class="fas fa-times"></i> Без категории</li>
                        <li @click="category.find = ''"><i class="fas fa-times"></i> Закрыть</li>
                      </ul>
                      <ul v-else-if="category.results && category.results == 0 && category.find != ''" class="list-unstyled list-find-category shadow">
                        <li>Ничего не найдено</li>
                      </ul>
                    </td>
                    <td v-else class="position-relative">
                      <input type="text" v-on:input="findCategory(category, index)" v-model="category.find"
                             class="form-control form-control-sm">
                      <ul class="list-unstyled list-find-category shadow" v-if="category.results && category.results.length > 0 && category.find != ''">
                        <li v-for="fc in category.results" @click="setCategoryAlias(category, fc)">
                          {{ fc.name }} < {{ fc.parent_name }}
                        </li>
                        <li @click="setCategoryAlias(category, null)"><i class="fas fa-times"></i> Без категории</li>
                        <li @click="category.find = ''"><i class="fas fa-times"></i> Закрыть</li>
                      </ul>
                      <ul v-else-if="category.results && category.results == 0 && category.find != ''" class="list-unstyled list-find-category shadow">
                        <li>Ничего не найдено</li>
                      </ul>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endverbatim
@endsection

@push('scripts')
  <script>
      var app = new Vue({
          el: "#app",
          data: function () {
              return {
                  token: '{{ csrf_token() }}',
                  provider_id: '{{ \request()->id }}',
                  categories: [],
              }
          },
          methods: {
              sendRequest: function (data = {}, callback) {
                  data._token = this.token;
                  axios.post('', data)
                      .then(function (response) {
                          return callback(response.data);
                      })
                      .catch(function (error) {
                          console.log(error);
                      });
              },
              findCategory: debounce(function (cat, i) {
                  var el = this;
                  this.sendRequest({
                      type: 'findCategories',
                      val: cat.find,
                  }, function (data) {
                      var c = cat;
                      c.results = data;
                      Vue.set(el.categories, i, c);
                  });
              }, 300),
              setCategoryAlias: function (cat, cat_alias) {
                  el = this;
                  this.sendRequest({
                      type: 'setCategoryAlias',
                      local_category_id: cat_alias == null ? null : cat_alias.id,
                      price_category_id: cat.price_category_id,
                      provider_id: el.provider_id,
                  }, function (data) {
                      console.log(data);
                    if(data.resp == 'ok') {
                        var c = cat;
                        if(cat_alias == null) {
                            c.local_category_id = null;
                            c.local_category_name = null;
                            c.local_parent_name = null;
                            c.results = null;
                            c.find = '';
                        } else {
                            c.local_category_id = cat_alias.id;
                            c.local_category_name = cat_alias.name;
                            c.local_parent_name = cat_alias.parent_name;
                            c.results = null;
                        }
                        Vue.set(el.categories, i, c);
                    }
                  });
              },
          },
          mounted: function () {
              var el = this;
              this.sendRequest({
                  type: 'getCategories',
                  provider_id: this.provider_id,
              }, function (data) {
                  el.categories = data;
              });
          },
      });
      function debounce(f, ms) {
          var timer = null;
          return function (...args) {
              const onComplete = () => {
                  f.apply(this, args);
                  timer = null;
              }
              if (timer) {
                  clearTimeout(timer);
              }
              timer = setTimeout(onComplete, ms);
          };
      };

  </script>
@endpush

@push('styles')
  <style>
    .list-find-category {
      position: absolute;
      top: 50px;
      background: #fff;
      z-index: 900;
    }
    .list-find-category li {
      padding: 10px 20px;
      cursor: pointer;
    }
    .list-find-category li:hover {
      background: #eee;
    }
  </style>
@endpush