@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление статьями
    <a href="{{ route('admin.articles.edit', 'new') }}" class="btn btn-primary float-right" title="Добавить статью"><span class="fa fa-plus"></span></a>
  </h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Статьи</h6>
    </div>
    <div class="card-body" id="blog-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th>ID</th>
            <th>Заголовок</th>
            <th>Описание</th>
            <th>Дата создания</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($articles as $article)
            <tr>
              <td>{{ $article->id }}</td>
              <td>{{ $article->title }}</td>
              <td>{{ $article->description }}</td>
              <td>{{ $article->created_at }}</td>
              <td>
                <a href="{{ route('admin.articles.edit', ['id' => $article->id]) }}" class="btn btn-primary"><span
                    class="fa fa-eye"></span></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $articles->links() }}
      </div>
    </div>
  </div>
@endsection