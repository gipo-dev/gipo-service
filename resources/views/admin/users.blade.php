@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление Сайтами</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Сайты агентов</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th>ID пользователя</th>
            <th>Имя пользователя</th>
            <th>Почтовый адрес</th>
            <th>Статус</th>
            <th>Был в сети</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($users as $user)
            <tr>
              <td>{{ $user->id }}</td>
              <td>{{ $user->first_name }} {{ $user->second_name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->status->name }}</td>
              <td class="{{ $user->is_online ? 'text-success' : '' }}">{{ $user->is_online ? 'Онлайн' : $user->last_activity->diffForHumans() }}</td>
              <td>
                <a href="{{ route('admin.user', ['id' => $user->id]) }}" class="btn btn-primary"><span
                    class="fa fa-eye"></span></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $users->links() }}
      </div>
    </div>
  </div>


  {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">Управление заказами</div>--}}

    {{--<div class="panel-body" id="orders-list">--}}
      {{--@if (session('status'))--}}
        {{--<div class="alert alert-success">--}}
          {{--{{ session('status') }}--}}
        {{--</div>--}}
      {{--@endif--}}
      {{--<div class="table-responsive">--}}
        {{--<table class="table table-striped table-hover">--}}
          {{--<thead>--}}
            {{--<tr>--}}
              {{--<th>ID сайта</th>--}}
              {{--<th>ID владельца</th>--}}
              {{--<th>URL сайта</th>--}}
              {{--<th>Статус сайта</th>--}}
              {{--<th>Действие</th>--}}
            {{--</tr>--}}
          {{--</thead>--}}
          {{--<tbody>--}}
          {{--@foreach($websites as $website)--}}
            {{--<tr>--}}
              {{--<td>{{ $website->id }}</td>--}}
              {{--<td>{{ $website->user_id }}</td>--}}
              {{--<td>{{ $website->url }}</td>--}}
              {{--<td>{{ $website->status_name }}</td>--}}
              {{--<td>--}}
                {{--<a href="{{ route('admin.website.edit', ['id' => $website->id]) }}" class="btn btn-primary"><span--}}
                    {{--class="glyphicon glyphicon-eye-open"></span></a>--}}
              {{--</td>--}}
            {{--</tr>--}}
            {{--@endforeach--}}
          {{--</tbody>--}}
        {{--</table>--}}
        {{--{{ $websites->links() }}--}}
      {{--</div>--}}
    {{--</div>--}}
  {{--</div>--}}
@endsection