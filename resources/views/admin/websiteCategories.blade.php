@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Вебсайт {{ $website->url }} категории</h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif

  <div class="row" style="font-size: 15px;">
    <div class="col-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">
            <div class="float-right">
              <p id="total-category-checked" class="d-inline-block">
                Всего выделено категорий: 0
              </p>
              <button type="submit" form="form-settings" class="btn btn-primary ml-3">
                <span class="fa fa-save"></span>
              </button>
            </div>
          </h6>
        </div>
        <div class="card-body">
          <div>
            <form action="" name="form-categories" method="POST" class="form-horizontal"
                  id="form-settings">
              {{ csrf_field() }}
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <td class="text-center" style="width: 1px;"><input type="checkbox" disabled>
                    </td>
                    <td class="text-left">Название категории</td>
                  </tr>
                  </thead>
                </table>
                <div class="table table-bordered table-categories" id="categories">
                  @include('admin.partials.wsCategories', ['item' => $categories, 'offset' => 16])
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('scripts')
  <script type="text/javascript">
      $('#categories li').mouseenter(function () {
          var parentId = $(this).attr('attr-parent-id');
          $('#categories li').removeClass('active');
          $('#categories li[attr-parent-id=\'' + parentId + '\']').addClass('active');
      });

      //выделяет дерево элементов
      $('#categories li input[type=checkbox]').change(function () {
          var newState = $(this).is(':checked');
          var id = $($(this).parents('li')[0]).attr('attr-id');
          $('#categories li[attr-parent-id=\'' + id + '\'] input[type=checkbox]').prop('checked', newState);
          if (newState) {
              var elem = $('#categories li[attr-id=\'' + id + '\'] + .list input[type=checkbox]');
              elem.prop('checked', newState);
              var i = 0;
              while (true) {
                  if (++i > 50)
                      return;
                  var elem = $('#categories li[attr-id=\'' + id + '\']');
                  if (elem.length == 0)
                      break;
                  id = elem.attr('attr-parent-id');
                  elem.find('input[type=checkbox]').prop('checked', true);
              }
          } else {
              var elem = $('#categories li[attr-parent-id=\'' + id + '\'] + .list input[type=checkbox]');
              elem.prop('checked', newState);
          }
          calculateTotalCheckedCategories();
      });

      //количество выделеных категорий
      $(function () {
          calculateTotalCheckedCategories();
      });

      function calculateTotalCheckedCategories() {
          var total = $('input[name="categories[]"]:checked').length;
          $('#total-category-checked').text("Всего выделено категорий: " + total);
      }

  </script>
@endpush

@push('styles')
  <style>
    .table {
      margin-bottom: 0;
      border-bottom: none;
    }

    .table-categories {
      border-top: none;
    }

    .table-categories ul {
      list-style: none;
      padding-left: 0;
    }

    .table-categories > ul {
      padding-left: 0;
      margin-bottom: 0;
    }

    .table-categories li:not(.list) {
      padding: 16px;
      border-bottom: 1px solid #ddd;
    }

    .table-categories li.active {
      background-color: #f5f5f5;
    }

    .table-categories li input {
      margin-right: 8px;
    }
  </style>
@endpush