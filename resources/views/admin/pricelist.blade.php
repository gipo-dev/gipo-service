@extends('admin.layouts.app')

@section('content')
  <h2 class="h3 mb-4 text-gray-800">Прайслисты</h2>
  @verbatim
    <div class="row" style="font-size: 15px;" id="app">
      <div class="col-6 col-xl-3">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-shopping-cart"></span> Выберите поставщика
            </h6>
          </div>
          <div class="card-body">
            <form action="" enctype="multipart/form-data" method="POST" id="provider_form">
              <input type="text" name="_token" :value="token" hidden>
              <label for="provider_id">Поставщик</label>
              <select name="provider_id" id="provider_id" class="custom-select custom-select-sm"
                      v-model:value="price_settings.provider_id">
                <option value="-1">--не выбран</option>
                <option v-for="provider in providers" :value="provider.id">{{ provider.name }}</option>
              </select>
              <div class="custom-file mt-3">
                <input type="file" class="custom-file-input" id="priceFile" accept=".yml, .xml" name="pricelist_file">
                <label class="custom-file-label" for="priceFile">Выберите yml или xml</label>
              </div>
              <button type="submit" class="btn btn-primary btn-sm mt-3 float-right"
                      :class="{ 'disabled' : price_settings.provider_id == -1 }">Загрузить новые товары</button>
            </form>
          </div>
        </div>
      </div>
      <div class="col-6 col-xl-3">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-list"></span> Управление
              категориями
            </h6>
          </div>
          <div class="card-body">
            <a :href="'/admin/pricelist/yml/categories/' + price_settings.provider_id"
                class="btn btn-primary btn-sm mt-3 float-right" :class="{ 'disabled' : price_settings.provider_id == -1 }">Управление категориями</a>
          </div>
        </div>
      </div>
      <div class="col-6 col-xl-3">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-shopping-cart"></span> Управление
              товарами
            </h6>
          </div>
          <div class="card-body">
            <a :href="'/admin/pricelist/yml/products/' + price_settings.provider_id"
               class="btn btn-primary btn-sm mt-3 float-right" :class="{ 'disabled' : price_settings.provider_id == -1 }">Залить новые товары</a>
          </div>
        </div>
      </div>
    </div>
  @endverbatim
@endsection

@push('scripts')
  <script>
      var app = new Vue({
          el: "#app",
          data: function () {
              return {
                  token: '{{ csrf_token() }}',
                  step: 0,
                  price_settings: {
                      provider_id: -1,
                      file: null,
                  },
                  providers: [],
              }
          },
          methods: {
              sendRequest: function (data = {}, callback) {
                  data._token = this.token;
                  axios.post('', data)
                      .then(function (response) {
                          return callback(response.data);
                      })
                      .catch(function (error) {
                          console.log(error);
                      });
              },
          },
          mounted: function () {
              var el = this;
              this.sendRequest({
                  type: 'providers',
              }, function (data) {
                  el.providers = data;
              });
          }
      });
  </script>
@endpush