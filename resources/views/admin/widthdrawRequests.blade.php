@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Запросы на вывод средств</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Список запросов</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr>
            <th>№ Запроса</th>
            <th>Агент</th>
            <th>Статус</th>
            <th>Сумма</th>
            <th>Дата добавления</th>
            <th>Дата обновления</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($requests as $req)
            <tr>
              <td>{{ $req->id }}</td>
              <td>
                <a href="/admin/user/{{ $req->user()->id }}">{{ $req->user()->first_name }} {{ $req->user()->second_name }}</a>
              </td>
              <td>{{ $req->status_name }}</td>
              <td>{{ $req->sum }}</td>
              <td>{{ $req->created_at }}</td>
              <td>{{ $req->updated_at }}</td>
              <td>
                <div class="btn-group">
                  <a href="{{ route('admin.widthdrawRequest', ['id' => $req->id]) }}" class="btn btn-primary"><span
                      class="fa fa-eye" aria-hidden="true"></span></a>
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Список</span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#"><span class="fa fa-eye-dashed" aria-hidden="true"></span> Скрыть</a>
                    </li>
                  </ul>
                </div>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $requests->links() }}
      </div>
    </div>
  </div>


  {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">Запросы на вывод средств</div>--}}

    {{--<div class="panel-body" id="orders-list">--}}
      {{--@if (session('status'))--}}
        {{--<div class="alert alert-success">--}}
          {{--{{ session('status') }}--}}
        {{--</div>--}}
      {{--@endif--}}
      {{--<div class="table-responsive">--}}
        {{--<table class="table table-striped table-hover">--}}
          {{--<thead>--}}
          {{--<tr>--}}
            {{--<th>№ Запроса</th>--}}
            {{--<th>Агент</th>--}}
            {{--<th>Статус</th>--}}
            {{--<th>Сумма</th>--}}
            {{--<th>Дата добавления</th>--}}
            {{--<th>Дата обновления</th>--}}
            {{--<th>Действие</th>--}}
          {{--</tr>--}}
          {{--</thead>--}}
          {{--<tbody>--}}
          {{--@foreach($requests as $req)--}}
            {{--<tr>--}}
              {{--<td>{{ $req->id }}</td>--}}
              {{--<td>--}}
                {{--<a href="/admin/user/{{ $req->user()->id }}">{{ $req->user()->first_name }} {{ $req->user()->second_name }}</a>--}}
              {{--</td>--}}
              {{--<td>{{ $req->status_name }}</td>--}}
              {{--<td>{{ $req->sum }}</td>--}}
              {{--<td>{{ $req->created_at }}</td>--}}
              {{--<td>{{ $req->updated_at }}</td>--}}
              {{--<td>--}}
                {{--<div class="btn-group">--}}
                  {{--<a href="{{ route('admin.widthdrawRequest', ['id' => $req->id]) }}" class="btn btn-primary"><span--}}
                      {{--class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>--}}
                  {{--<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"--}}
                          {{--aria-haspopup="true" aria-expanded="false">--}}
                    {{--<span class="caret"></span>--}}
                    {{--<span class="sr-only">Список</span>--}}
                  {{--</button>--}}
                  {{--<ul class="dropdown-menu">--}}
                    {{--<li><a href="#"><span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span> Скрыть</a>--}}
                    {{--</li>--}}
                  {{--</ul>--}}
                {{--</div>--}}
              {{--</td>--}}
            {{--</tr>--}}
          {{--@endforeach--}}
          {{--</tbody>--}}
        {{--</table>--}}
        {{--{{ $requests->links() }}--}}
      {{--</div>--}}
    {{--</div>--}}
  {{--</div>--}}
@endsection