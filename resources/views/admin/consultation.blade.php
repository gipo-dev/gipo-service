@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Запрос на вывод средств №{{ $request->id }} <span
      class="btn btn-sm btn-{{ $request->status_id == 3 ? 'success' : 'primary' }}">{{ $request->status_name }}</span>
  </h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-6 col-xl-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-user"></span> Пользователь</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <tbody>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Имя
                </button>
              </td>
              <td>{{ $request->name }}</td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  E-mail
                </button>
              </td>
              <td>{{ $request->email }}</td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Телефон
                </button>
              </td>
              <td>{{ $request->phone }}</td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Комментарий
                </button>
              </td>
              <td>{{ $request->comment }}</td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Заявка создана
                </button>
              </td>
              <td>{{ $request->created_at }}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-6 col-xl-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-cog"></span> Управление</h6>
        </div>
        <div class="card-body">
          <form method="POST" id="settings">
            {{ csrf_field() }}
            <div class="panel panel-default">
              <div class="panel-heading">
                <span class="glyphicon glyphicon-cog"></span> Управление
              </div>
              <table class="table">
                <tbody>
                <tr>
                  <td style="width: 1%;">
                    <button class="btn btn-info">
                      Статус
                    </button>
                  </td>
                  <td>
                    <select name="status_id" class="form-control" id="input-request-status">
                      @foreach($settings['status'] as $status)
                        <option
                          value="{{ $status->id }}" {{ $status->id == $request->status_id ? 'selected' : '' }}>{{ $status->name }}</option>
                      @endforeach
                    </select>
                  </td>
                </tr>
                <tr>
                  <td style="width: 1%;">
                    <button class="btn btn-info btn-sm text-xs">
                      Последнее обновления
                    </button>
                  </td>
                  <td>{{ $request->updated_at }}</td>
                </tr>
                </tbody>
              </table>
              <button type="submit" class="btn btn-success btn-sm float-right" form="settings"><span
                  class="fa fa-save"></span> Обновить статус
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('styles')
  <style>
    .table {
      margin-bottom: 0;
    }

    .table tr:first-child td {
      border-top: none;
    }
  </style>
@endpush