<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon">
            <i class="fas fa-cloud"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Админка</div>
    </a>

    <hr class="sidebar-divider my-0">

    @foreach(\App\Http\Controllers\AdminController::getLMenu() as $menu)
        @if(!isset($menu['items']))
          {{--{{ dd(request()->path()) }}--}}
            <li class="nav-item {{ request()->path().'/' == 'admin'.$menu['link'] ? 'active' : '' }}">
                <a class="nav-link" href="/admin{{ $menu['link'] }}">
                    <i class="fas fa-fw fa-{{ $menu['icon'] }}"></i>
                    <span>{{ $menu['name'] }}</span>
                    @if(isset($menu['count']) && $menu['count'] > 0)
                      <b class="badge badge-light ml-1">{{ $menu['count'] }}</b>
                    @endif
                </a>
            </li>
        @else
            <li class="nav-item {{ request()->url() == $menu['link'] ? 'active' : '' }}">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#{{ $menu['link'] }}" aria-expanded="false" aria-controls="collapseTwo">
                    <i class="fas fa-fw fa-{{ $menu['icon'] }}"></i>
                    <span>{{ $menu['name'] }}</span>
                    @if(isset($menu['count']) && $menu['count'] > 0)
                      <b class="badge badge-light ml-1">{{ $menu['count'] }}</b>
                    @endif
                </a>
                <div id="{{ $menu['link'] }}" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        @foreach($menu['items'] as $s_menu)
                            <a class="collapse-item" href="/admin{{ $s_menu['link'] }}">
                                <i class="fas fa-fw fa-{{ $s_menu['icon'] or '' }}"></i> {{ $s_menu['name'] }}
                                @if(isset($s_menu['count']) && $s_menu['count'] > 0)
                                  <b class="badge badge-light ml-1">{{ $menu['count'] }}</b>
                                @endif
                            </a>
                        @endforeach
                    </div>
                </div>
            </li>
        @endif
    @endforeach

    <hr class="sidebar-divider">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>