@foreach($categories as $item)
  @if(isset($item['sub']))
    <ul>
      <li style="padding-left: {{ $offset }}px" attr-id="{{ $item['id'] }}" attr-parent-id="{{ $item['parent_id'] }}">
        <input type="checkbox" name="categories[]" value="{{ $item['id'] }}"
          {{ isset($item['selected']) ? 'checked="true"' : "" }}>
        <b data-toggle="collapse" data-target="#ccategory{{ $item['id'] }}"
           aria-expanded="false" aria-controls="collapse{{ $item['id'] }}">{{ $item['name'] }}
          <i class="fa fa-sort-desc"></i>
        </b>
      </li>
      <li class="list collapse" id="ccategory{{ $item['id'] }}">
        @include('admin.partials.wsCategories', ['categories' => $item['sub'], 'offset' => $offset + 16])
      </li>
    </ul>
  @else
    <ul>
      <li style="padding-left: {{ $offset }}px" attr-id="{{ $item['id'] }}" attr-parent-id="{{ $item['parent_id'] }}">
        <input type="checkbox" name="categories[]" value="{{ $item['id'] }}" {{ isset($item['selected']) ? 'checked="true"' : "" }}>
        {{ $item['name'] }}
      </li>
    </ul>
  @endif
@endforeach