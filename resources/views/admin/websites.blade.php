@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление Сайтами</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Сайты агентов</h6>
    </div>
    <div class="card-body" id="orders-list">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <tr>
            <th>ID сайта</th>
            <th>ID владельца</th>
            <th>URL сайта</th>
            <th>Статус сайта</th>
            <th>Действие</th>
          </tr>
          </thead>
          <tbody>
          @foreach($websites as $website)
            <tr>
              <td>{{ $website->id }}</td>
              <td>{{ $website->user_id }}</td>
              <td>{{ $website->url }} <span class="small">({{ $website->name }})</span></td>
              <td>{{ $website->status_name }}</td>
              <td>
                <a href="{{ route('admin.website.edit', ['id' => $website->id]) }}" class="btn btn-primary"><span
                    class="fa fa-eye"></span></a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
        {{ $websites->links() }}
      </div>
    </div>
  </div>


  {{--<div class="panel panel-default">--}}
    {{--<div class="panel-heading">Управление заказами</div>--}}

    {{--<div class="panel-body" id="orders-list">--}}
      {{--@if (session('status'))--}}
        {{--<div class="alert alert-success">--}}
          {{--{{ session('status') }}--}}
        {{--</div>--}}
      {{--@endif--}}
      {{--<div class="table-responsive">--}}
        {{--<table class="table table-striped table-hover">--}}
          {{--<thead>--}}
            {{--<tr>--}}
              {{--<th>ID сайта</th>--}}
              {{--<th>ID владельца</th>--}}
              {{--<th>URL сайта</th>--}}
              {{--<th>Статус сайта</th>--}}
              {{--<th>Действие</th>--}}
            {{--</tr>--}}
          {{--</thead>--}}
          {{--<tbody>--}}
          {{--@foreach($websites as $website)--}}
            {{--<tr>--}}
              {{--<td>{{ $website->id }}</td>--}}
              {{--<td>{{ $website->user_id }}</td>--}}
              {{--<td>{{ $website->url }}</td>--}}
              {{--<td>{{ $website->status_name }}</td>--}}
              {{--<td>--}}
                {{--<a href="{{ route('admin.website.edit', ['id' => $website->id]) }}" class="btn btn-primary"><span--}}
                    {{--class="glyphicon glyphicon-eye-open"></span></a>--}}
              {{--</td>--}}
            {{--</tr>--}}
            {{--@endforeach--}}
          {{--</tbody>--}}
        {{--</table>--}}
        {{--{{ $websites->links() }}--}}
      {{--</div>--}}
    {{--</div>--}}
  {{--</div>--}}
@endsection