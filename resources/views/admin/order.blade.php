@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Заказ №{{ $order->id }} <span
      class="btn btn-sm btn-{{ $order->status_id == 4 ? 'success' : 'primary' }}">{{ $order->status_name }}</span>
    <div class="float-right">
      <a href="{{ route('order.print', $order->id) }}" class="btn btn-primary" title="Печать" target="_blank"><span class="fa fa-print"></span></a>
    </div>
  </h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-6 col-xl-3">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-shopping-cart"></span> Детали заказа</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <tbody>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm" title="Магазин"><i
                    class="fab fa-opencart"></i></button>
              </td>
              <td><a href="{{ $order->order_data->store_url or '#' }}"
                     target="_blank">{{ $order->order_data->store_url or 'Адрес магазина не установлен' }}</a></td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm" title="Дата добавления">
                  <i class="fa fa-calendar"></i></button>
              </td>
              <td>{{  $order->created_at->diffForHumans() }}</td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm" title="Способ оплаты"><i
                    class="fa fa-credit-card"></i></button>
              </td>
              <td>{{ $order->order_data->payment_method or $order->order_data->pay_type_id }}</td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm" title="Способ доставки">
                  <i class="fa fa-road"></i></button>
              </td>
              <td>{{ $order->order_data->shipping_method or "Не установлено" }}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-6 col-xl-3">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-user"></span> Покупатель</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <tbody>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm" title="Имя"><i
                    class="fa fa-user"></i></button>
              </td>
              <td>{{ $order->order_data->payment_firstname.' '.$order->order_data->payment_lastname }}</td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm" title="E-mail"><i
                    class="fa fa-envelope"></i></button>
              </td>
              <td>{{ $order->order_data->email }}</td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm" title="Номер телефона">
                  <i class="fa fa-phone"></i></button>
              </td>
              <td>{{ $order->order_data->telephone }}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-6 col-xl-3">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-truck"></span> Адрес доставки</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <tbody>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm" title="Адрес доставки"><i
                    class="fa fa-home"></i></button>
              </td>
              <td>{{ $order->order_data->shipping_address_1 }}</td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm" title="Комментарий"><i
                    class="fa fa-comment"></i></button>
              </td>
              <td>{{ $order->order_data->comment }}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-6 col-xl-3">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-user"></span> Продавец
            ({{ $website->tariff_name }})</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <tbody>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm" title="Имя агента"><i
                    class="fa fa-user"></i></button>
              </td>
              <td>
                <a href="#!">{{ $website->user->first_name }} {{ $website->user->second_name }}</a>
              </td>
            </tr>
            <tr>
              <td>
                <button class="btn btn-info btn-sm" title="Указанный адрес сайта, тариф и статус"><i
                    class="fa fa-globe"></i></button>
              </td>
              <td>
                <a href="{{ route('admin.website.edit', [ 'id' => $website->id ]) }}"
                   target="_blank">{{ $website->url }}
                  <b>{{ $website->tariff_name }}</b> ({{ $website->status_name }})</a>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Связанные товары</h6>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <thead>
        <tr>
          <th style="width: 1%;">id</th>
          <th>Товар</th>
          <th>Количество</th>
          <th>Цена для агента (коммиссия)</th>
          <th>Цена за еденицу (у продавца)</th>
          <th>Итого</th>
        </tr>
        </thead>
        <tbody>
        @php $total_products = 0 @endphp
        @foreach ($order->products as $product)
          @php $total_products += $product->price @endphp
          <tr>
            <td>{{ $product->gipu_id }}</td>
            <td><a href="/admin/product/list/?id={{ $product->gipu_id }}&name=&method=%D0%A1%D1%82%D0%B0%D0%BD%D0%B4%D0%B0%D1%80%D1%82%D0%BD%D0%BE" target="_blank">{{ $product->name }}</a></td>
            <td>{{ $product->quantity }}</td>
            <td>{{ $product->agent_price }} <i>({{ $product->user_reward }})</i></td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->total }}</td>
          </tr>
        @endforeach
        <tr>
          <td colspan="5" class="text-right"><b>Итого</b></td>
          <td>{{ $total_products }}</td>
        </tr>
        <tr>
          <td colspan="5" class="text-right"><b>{{ $order->order_data->shipping_method }}</b></td>
          <td>{{ $order->order_data->shipping_method == 'Доставка с фиксированной стоимостью' ? '290' : '0' }}</td>
        </tr>
        <tr>
          <td colspan="5" class="text-right"><b>Всего</b></td>
          <td><b>{{ $order->order_data->total }}</b></td>
        </tr>
        <tr>
          <td colspan="5" class="text-right"><b>Агентская награда</b></td>
          <td><b>{{ $order->total_agent_reward }}</b></td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">История заказа</h6>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <thead>
        <tr>
          <th>Дата добавления</th>
          <th>Комментарий</th>
          <th>Статус</th>
          <th>Покупатель уведомлен</th>
        </tr>
        </thead>
        <tbody>
        @if(count($order->order_history) > 0)
          @foreach ($order->order_history as $row)
            <tr>
              <td>{{ date_format(date_create($row->date), 'H:m:s d-m-Y') }}</td>
              <td>{{ $row->comment }}</td>
              <td><b>{{ $row->name }}</b></td>
              <td>{{ $row->buyer_notified ? "Да" : "Нет" }}</td>
            </tr>
          @endforeach
        @else
          <tr>
            <td colspan="4" class="text-center"><h4>История пока что пуста</h4></td>
          </tr>
        @endif
        </tbody>
      </table>
    </div>
  </div>

  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Изменить статус заказа</h6>
    </div>
    <div class="card-body">
      <fieldset>
        <form class="form-horizontal" name="form-status" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-order-status">Статус</label>
            <div class="col-sm-10">
              <select name="order_status_id" id="input-order-status" class="form-control">
                @foreach($status_list as $status)
                  <option
                    value="{{ $status->id }}" {{ $status->id == $order->status_id ? 'selected' : '' }}>{{ $status->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group col-12 text-lg">
            <div class="custom-control custom-checkbox small">
              <input type="checkbox" class="custom-control-input" value="1" name="notify" id="input-notify">
              <label class="custom-control-label" for="input-notify">Уведомить покупателя</label>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-comment">Комментарий</label>
            <div class="col-sm-10">
              <textarea name="comment" rows="8" id="input-comment" class="form-control"></textarea>
            </div>
          </div>
          <button type="submit" class="btn btn-success">
            <span class="fa fa-save"></span> Обновить статус
          </button>
        </form>
      </fieldset>
    </div>
  </div>
@endsection

@push('modals')
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-successed">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Подтвердите действие</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><b>Внимание: </b>при активации этого статуса партнерские деньги будут автоматически переведены агентам!
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
          <button type="button" class="btn btn-success" data-dismiss="modal" id="modal-successed-ok">Принять и
            отправить деньги
          </button>
        </div>
      </div>
    </div>
  </div>
@endpush

@push('scripts')
  <script>
      var last_selected = $('#input-order-status option:selected').val();
      $('#input-order-status').change(function (e) {
          if ($(this).val() == '4') {
              $(this).val(last_selected);
              $('#modal-successed').modal('show');
          } else {
              last_selected = $('#input-order-status option:selected').val();
          }
      });
      $('#modal-successed-ok').click(function () {
          $('#input-order-status').val('4');
      });
  </script>
@endpush
@push('styles')
  <style>
    .table {
      margin-bottom: 0;
    }

    .table tr:first-child td {
      border-top: none;
    }
  </style>
@endpush