@extends('admin.layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Вебсайт №{{ $website->id }} <span
      class="btn btn-sm btn-{{ $website->status_name == 3 ? 'success' : 'primary' }}">{{ $website->status_name }}</span></h1>
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif
  <div class="row" style="font-size: 15px;">
    <div class="col-6 col-xl-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-user"></span> Пользователь</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <tbody>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Имя пользователя
                </button>
              </td>
              <td>{{ $user->id }}.{{ $user->first_name }} {{ $user->second_name }}</td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Дата регистрации
                </button>
              </td>
              <td>{{ $user->created_at }}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-6 col-xl-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-globe"></span> Веб-сайт</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <tbody>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Адрес сайта
                </button>
              </td>
              <td>{{ $website->url }}</td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Текущий тариф
                </button>
              </td>
              <td>{{ $website->tariff_name }}</td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Дата создания
                </button>
              </td>
              <td>{{ $website->created_at }}</td>
            </tr>
            <tr>
              <td style="width: 1%;">
                <button class="btn btn-info btn-sm text-xs">
                  Оплачен до
                </button>
              </td>
              <td>{{ $website->paid_to or 'Не оплачен' }} <a href="" class="">+1 год</a></td>
            </tr>
            </tbody>
          </table>
          <a href="{{ route('admin.website.categories', $website->id) }}" class="btn btn-success btn-sm float-right"><span class="fa fa-list"></span> Выбрать категории</a>
        </div>
      </div>
    </div>
    <div class="col-6 col-xl-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-user"></span> Пользователь</h6>
        </div>
        <div class="card-body">
          <form method="POST" id="settings">
            {{ csrf_field() }}
            <div class="panel panel-default">
              <div class="panel-heading">
                <span class="glyphicon glyphicon-cog"></span> Управление
              </div>
              <table class="table">
                <tbody>
                <tr>
                  <td style="width: 1%;">
                    <button class="btn btn-info">
                      Статус
                    </button>
                  </td>
                  <td>
                    <select name="status_id" class="form-control">
                      @foreach($settings['status'] as $status)
                        <option value="{{ $status->id }}" {{ $status->id == $website->status_id ? 'selected' : '' }}>{{ $status->name }}</option>
                      @endforeach
                    </select>
                  </td>
                </tr>
                <tr>
                  <td style="width: 1%;">
                    <button class="btn btn-info btn-sm text-xs">
                      Последнее обновления
                    </button>
                  </td>
                  <td>{{ $website->updated_at }}</td>
                </tr>
                </tbody>
              </table>
              <button type="submit" class="btn btn-success btn-sm float-right" form="settings"><span class="fa fa-save"></span> Обновить статус</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


  {{--<div class="container-fluid page-header">--}}
    {{--<h1>Вебсайт №{{ $website->id }} <span--}}
        {{--class="btn btn-xs btn-{{ $website->status_name == 3 ? 'success' : 'primary' }}">{{ $website->status_name }}</span>--}}
    {{--</h1>--}}
    {{--<div class="pull-right">--}}
      {{--<button type="submit" class="btn btn-success" form="settings"><span class="glyphicon glyphicon-floppy-disk"></span></button>--}}
    {{--</div>--}}
  {{--</div>--}}
  {{--<div class="row">--}}
    {{--<div class="col-md-4">--}}
      {{--<div class="panel panel-default">--}}
        {{--<div class="panel-heading">--}}
          {{--<span class="glyphicon glyphicon-user"></span> Пользователь--}}
        {{--</div>--}}
        {{--<table class="table">--}}
          {{--<tbody>--}}
          {{--<tr>--}}
            {{--<td style="width: 1%;">--}}
              {{--<button class="btn btn-info btn-xs">--}}
                {{--Имя пользователя--}}
              {{--</button>--}}
            {{--</td>--}}
            {{--<td>{{ $user->id }}.{{ $user->first_name }} {{ $user->second_name }}</td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td style="width: 1%;">--}}
              {{--<button class="btn btn-info btn-xs">--}}
                {{--Дата регистрации--}}
              {{--</button>--}}
            {{--</td>--}}
            {{--<td>{{ $user->created_at }}</td>--}}
          {{--</tr>--}}
          {{--</tbody>--}}
        {{--</table>--}}
      {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-md-4">--}}
      {{--<div class="panel panel-default">--}}
        {{--<div class="panel-heading">--}}
          {{--<span class="glyphicon glyphicon-link"></span> Веб-сайт--}}
        {{--</div>--}}
        {{--<table class="table">--}}
          {{--<tbody>--}}
          {{--<tr>--}}
            {{--<td style="width: 1%;">--}}
              {{--<button class="btn btn-info btn-xs">--}}
                {{--Адрес сайта--}}
              {{--</button>--}}
            {{--</td>--}}
            {{--<td>{{ $website->url }}</td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td style="width: 1%;">--}}
              {{--<button class="btn btn-info btn-xs">--}}
                {{--Текущий тариф--}}
              {{--</button>--}}
            {{--</td>--}}
            {{--<td>{{ $website->tariff_name }}</td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td style="width: 1%;">--}}
              {{--<button class="btn btn-info btn-xs">--}}
                {{--Дата создания--}}
              {{--</button>--}}
            {{--</td>--}}
            {{--<td>{{ $website->created_at }}</td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td style="width: 1%;">--}}
              {{--<button class="btn btn-info btn-xs">--}}
                {{--Оплачен до--}}
              {{--</button>--}}
            {{--</td>--}}
            {{--<td>{{ $website->paid_to or 'Не оплачен' }} <a href="" class="">+1 год</a></td>--}}
          {{--</tr>--}}
          {{--</tbody>--}}
        {{--</table>--}}
      {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-md-4">--}}
      {{--<form method="POST" id="settings">--}}
        {{--{{ csrf_field() }}--}}
        {{--<div class="panel panel-default">--}}
          {{--<div class="panel-heading">--}}
            {{--<span class="glyphicon glyphicon-cog"></span> Управление--}}
          {{--</div>--}}
          {{--<table class="table">--}}
            {{--<tbody>--}}
            {{--<tr>--}}
              {{--<td style="width: 1%;">--}}
                {{--<button class="btn btn-info">--}}
                  {{--Статус--}}
                {{--</button>--}}
              {{--</td>--}}
              {{--<td>--}}
                {{--<select name="status_id" class="form-control">--}}
                  {{--@foreach($settings['status'] as $status)--}}
                    {{--<option value="{{ $status->id }}" {{ $status->id == $website->status_id ? 'selected' : '' }}>{{ $status->name }}</option>--}}
                  {{--@endforeach--}}
                {{--</select>--}}
              {{--</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
              {{--<td style="width: 1%;">--}}
                {{--<button class="btn btn-info btn-xs">--}}
                  {{--Последнее обновления--}}
                {{--</button>--}}
              {{--</td>--}}
              {{--<td>{{ $website->updated_at }}</td>--}}
            {{--</tr>--}}
            {{--</tbody>--}}
          {{--</table>--}}
        {{--</div>--}}
      {{--</form>--}}
    {{--</div>--}}
  {{--</div>--}}
@endsection

@push('styles')
  <style>
    .table {
      margin-bottom: 0;
    }

    .table tr:first-child td {
      border-top: none;
    }
  </style>
@endpush