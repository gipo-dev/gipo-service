@extends('layouts.landingapp')

@push('title') Дропшиппинг поставщик товаров для интернет магазина gipo.ru @endpush

@section('content')
  <section class="home-slider owl-carousel bg-white" style="display: block !important;">
    <div class="slider-item">
      <div class="overlay"></div>
      <div class="container">
        <div class="row d-md-flex no-gutters slider-text align-items-center justify-content-end"
             data-scrollax-parent="true">
          <div class="one-third order-md-last img js-fullheight"
               style="background-image:url('/img/landing/head-1.jpg');height: 100%;">
            <h3 class="vr d-sm-none">достигаем высот вместе</h3>
          </div>
          <div class="one-forth d-flex align-items-center ftco-animate"
               data-scrollax=" properties: { translateY: '70%' }">
            <div class="text">
              <h1 class="mb-4">Дропшиппинг поставщик для интернет-магазина</h1>
              <h2 class="h6" style="font-weight: 400;font-size: 18px;line-height: 2.5rem;color: rgba(0, 0, 0, 0.8);">
                Предостовляем широкий ассортимент товаров и категорий по системе дропшиппинг. Постоянно в наличии более
                50 000 товарных позиций в 300 категориях.</h2>
              <p><a href="#services-2" class="btn btn-primary px-4 py-3 mt-3">Подробнее</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section ftco-services sect-categories">
    {{--<h2 class="h2 text-center mb-3"></h2>--}}
    <div class="row justify-content-center mb-4">
      <div class="col-md-6 col-lg-4 text-center">
        <div class="section-heading">
          <h2 class="section-title">
            Популяные категории
          </h2>
          <p>Подробную информацию обо всех категориях и товарах вы можете узнать у нас запросив прайс-лист</p>
        </div>
      </div>
    </div>
    <div class="d-flex justify-content-center">
      <div class="col-12 col-lg-10 col-xl-8 flex-wrap justify-content-around d-flex">
        <span class="btn btn-primary mt-3">Бытовая техника</span>
        <span class="btn btn-primary mt-3">Электроника</span>
        <span class="btn btn-primary mt-3">Детские товары</span>
        <span class="btn btn-primary mt-3">Инструмент и оборудование</span>
      </div>
    </div>
    <div class="d-flex justify-content-center">
      <div class="col-12 col-lg-10 col-xl-8 flex-wrap justify-content-around d-flex">
        <span class="btn btn-primary mt-3">Велосипеды и самокаты</span>
        <span class="btn btn-primary mt-3">Компьютерная техника</span>
        <span class="btn btn-primary mt-3">Садовый инвентарь</span>
      </div>
    </div>
    <div class="d-flex justify-content-center">
      <div class="col-12 col-lg-10 col-xl-8 flex-wrap justify-content-around d-flex">
        <span class="btn btn-primary mt-3">Аудио- и видео- техника</span>
        <span class="btn btn-primary mt-3">Мобильные телефоны</span>
        <span class="btn btn-primary mt-3">Автоаксессуары</span>
        <span class="btn btn-primary mt-3">Сантехника</span>
      </div>
    </div>
    <p style="text-align: center;">
      <a href="#request" class="btn btn-success px-5 mt-5" style="cursor: pointer !important;">
        Получить прайс-лист</a>
    </p>
  </section>

  <h2 class="h2 text-center mb-4">Также мы делаем<br>интернет-магазины под ключ</h2>
  <section class="section bg-grey ftco-services" id="feature">
    <div class="container">
      <div class="row justy-content-center">
        <div class="col-lg-3 col-sm-6 col-md-6">
          <div class="text-center feature-block">
            <div class="img-icon-block">
              <i class="licon-first"></i>
            </div>
            <h4 class="mb-2">Первый в России сервис</h4>
            <p>С нами вы откроете интернет магазин, наполните его товарами из нашей базы, и сможете продавать уже через
              24
              часа!</p>
          </div>
        </div>

        <div class="col-lg-3 col-sm-6 col-md-6">
          <div class="text-center feature-block">
            <div class="img-icon-block">
              <i class="licon-warehouse"></i>
            </div>
            <h4 class="mb-2">Огромная база товаров</h4>
            <p>Наша база насчитывает более 300 категорий, и более 50,000 товаров по оптовым ценам. С нами вам не нужны
              поставщики!</p>
          </div>
        </div>

        <div class="col-lg-3 col-sm-6 col-md-6">
          <div class="text-center feature-block">
            <div class="img-icon-block">
              <i class="licon-village"></i>
            </div>
            <h4 class="mb-2">Бизнес из деревни</h4>
            <p>С нами вы можете управлять своим бизнесом откуда угодно. Наш сервис берет на себя обработку заказа и
              доставку товара.</p>
          </div>
        </div>

        <div class="col-lg-3 col-sm-6 col-md-6">
          <div class="text-center feature-block">
            <div class="img-icon-block">
              <i class="licon-education"></i>
            </div>
            <h4 class="mb-2">Бесплатные тренинги</h4>
            <p>Мы регулярно проводим бесплатные лекции и тренинги для наших клиентов. Мы вам всё наглядно покажем и
              расскажем.</p>
          </div>
        </div>
      </div>
    </div> <!-- / .container -->
  </section>

  <section class="ftco-section">
    <div class="container">
      <div class="row justify-content-center mb-4">
        <div class="col-md-8 col-lg-6 text-center">
          <div class="section-heading">
            <!-- Heading -->
            <h2 class="section-title">
              Готовые интернет-магазины
            </h2>
            <!-- Subheading -->
            <p>
              Также вы можете выбрать другой дизайн, товары и категории из нашей базы на своё усмотрение.
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="container-wrap">
      <div class="row no-gutters">
        <div class="col-md-6 col-xl-3 ftco-animate">
          <div class="project">
            <img src="/img/landing/demo1.jpg" class="img-fluid" alt="Colorlib Template">
            <div class="text">
              <span><a href="https://tehnof.ru" class="link" target="_blank">tehnof.ru</a></span>
              <h3>Огромный ассортимент бытовой электроники</h3>
            </div>
            {{--image-popup--}}
            <a href="https://tehnof.ru" class="icon d-flex justify-content-center align-items-center iframe">
              Подробнее
            </a>
          </div>
        </div>
        <div class="col-md-6 col-xl-3 ftco-animate">
          <div class="project">
            <img src="/img/landing/demo6.jpg" class="img-fluid" alt="Colorlib Template">
            <div class="text">
              <span><a href="https://tooltop.ru" class="link" target="_blank">tooltop.ru</a></span>
              <h3>Строительный инструмент и оборудование</h3>
            </div>
            <a href="https://tooltop.ru" class="icon d-flex justify-content-center align-items-center iframe">
              Подробнее
            </a>
          </div>
        </div>
        <div class="col-md-6 col-xl-3 ftco-animate">
          <div class="project">
            <img src="/img/landing/demo5.jpg" class="img-fluid" alt="Colorlib Template">
            <div class="text">
              <span><a href="https://citytehno.ru" class="link" target="_blank">citytehno.ru</a></span>
              <h3>Новинки гаджетов по доступным ценам</h3>
            </div>
            <a href="https://citytehno.ru" class="icon d-flex justify-content-center align-items-center iframe">
              Подробнее
            </a>
          </div>
        </div>
        <div class="col-md-6 col-xl-3 ftco-animate">
          <div class="project">
            <img src="/img/landing/demo2.jpg" class="img-fluid" alt="Colorlib Template">
            <div class="text">
              <span><a href="https://pitervelo.ru" class="link" target="_blank">pitervelo.ru</a></span>
              <h3>Магазин велосипедов и гироскутеров</h3>
            </div>
            <a href="https://pitervelo.ru" class="icon d-flex justify-content-center align-items-center iframe">
              Подробнее
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section ftco-services" id="services-2">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6 text-center">
          <div class="section-heading">
            <!-- Heading -->
            <h2 class="section-title mb-2 text-white">
              Наши услуги
            </h2>

            <!-- Subheading -->
            <p class="mb-5 text-white">
              Gipo - уникальный сервис, созданный для автоматизации всех основных трудностей при создании
              интернет-магазина.
              Самое эффективное решение для вашего старта.
            </p>
          </div>
        </div>
      </div> <!-- / .row -->

      <div class="row">
        <div class="col-lg-4 col-sm-6 col-md-6 mb-30">
          <a class="web-service-block" href="#!">
            <i class="licon-box"></i>
            <h3>Синхронизация товаров</h3>
            <p>Выбранные вами товары, их остатки и цены будут автоматически синхронизироваться с нашей базой.</p>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 mb-30">
          <a class="web-service-block" href="#!">
            <i class="licon-analysis"></i>
            <h3>Аналитика цен</h3>
            <p>Вы видите минимальную цену других интернет-магазинов на каждый товар. Информация - это ваше оружие.</p>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 mb-30">
          <a class="web-service-block" href="#!">
            <i class="licon-opencart" style="font-size: 90px;margin-bottom: -15px;margin-top: -20px;"></i>
            <h3>Создание сайта</h3>
            <p>Мы создаем сайты для интернет-магазинов на лучшей платформе для сайтов в сфере e-commerce Opencart</p>
          </a>
        </div>

        <div class="col-lg-4 col-sm-6 col-md-6 ">
          <a class="web-service-block" href="#!">
            <i class="licon-delivery"></i>
            <h3>Доставим за вас</h3>
            <p>Доставим товар вашему клиенту до порога или на пункт самовывоза Boxberry.</p>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 ">
          <a class="web-service-block" href="#!">
            <i class="licon-call"></i>
            <h3>Колл-центр</h3>
            <p>После оформления заказа, вашему клиенту перезвонит наш менеджер и, от лица Вашего магазина, подтвердит
              заказ.</p>
          </a>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 ">
          <a class="web-service-block" href="#!">
            <i class="licon-24-hours"></i>
            <h3>Техподдержка</h3>
            <p>Мы дорожим каждым своим клиентом и всегда рядом, чтобы помочь.</p>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="pt-5">
    <div class="container d-flex justify-content-center pt-3">
      <a href="#request" class="btn btn-primary p-3 px-4">Записаться на бесплатную консультацию</a>
    </div>
  </section>

  <section class="ftco-services ftco-no-pb">
    <div class="container">
      <div class="row justify-content-start mb-5 pb-2">
        <div class="ftco-animate fadeInUp ftco-animated" style="width: 100%;">
          <h2 class="mb-2 text-center section-title">Как вам стать нашим клиентом</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
          <div class="media block-6 services d-block">
            <div class="icon d-flex justify-content-center align-items-center">
              <span class="licon-support"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">1. Заявка на сайте</h3>
              <p>Первым делом вам следует оставить заявку в нашей системе.
                Получив вашу заявку, наш менеджер оперативно с вами свяжется, и ответит на все вопросы, которые вас
                интересуют.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
          <div class="media block-6 services d-block">
            <div class="icon d-flex justify-content-center align-items-center">
              <span class="licon-cloud"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">2. Регистрация в системе</h3>
              <p>Второй этап - регистрация в нашей системе. Здесь вы подтвердите ваши паспортные данные, создадите
                свой сайт, и выберете категории товаров, которые вам интересны.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 d-flex align-self-stretch ftco-animate fadeInUp ftco-animated">
          <div class="media block-6 services d-block">
            <div class="icon d-flex justify-content-center align-items-center">
              <span class="licon-contract"></span>
            </div>
            <div class="media-body p-2 mt-3">
              <h3 class="heading">3. Оплата сервиса</h3>
              <p>Последний этап - это оплата. После заключения договора и поступления оплаты, мы размещаем полностью
                готовый к работе интернет-магазин на вашем сервере.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-services ftco-no-pb" style="padding-left:15px;padding-right:15px;" id="partners">
    <div class="row justify-content-start mb-5 pb-2">
      <div class="ftco-animate fadeInUp ftco-animated" style="width: 100%;">
        <h2 class="mb-2 text-center section-title">Как зарабатывать больше</h2>
      </div>
    </div>
    <div class="row mb-4">
      <div class="col-4 col-lg-3">
        <div class="text-center feature-block float-right" style="width: 295px;max-width: 100%">
          <div class="img-icon-block">
            <i class="licon-high-five"></i>
          </div>
        </div>
      </div>
      <div class="col-7 col-lg-5">
        <h4 class="mb-2">Приглашайте друзей</h4>
        <p>Расскажите друзьям и знакомым.
          За каждого нового пользователя Вы получаете 25% от стоимости тарифа.
        </p>
        {{--Но это не все! Вы получаете также 10%, даже когда Ваш друг приглашает новых пользователей.</p>--}}
      </div>
    </div>
  </section>

  <section class="ftco-services" id="request">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-2">
        <div class="ftco-animate fadeInUp ftco-animated" style="width: 100%;">
          <h2 class="mb-2 text-center section-title px-4">
            Получите наш прайс-лист <br>или получите ответы на возникшие вопросы
          </h2>
        </div>
      </div>
      <div class="row justify-content-center">
        <form action="" id="ajax_form" method="POST" class="col-12 col-md-8">
          {{ csrf_field() }}
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Ваше имя" name="name" required>
          </div>
          <div class="form-group">
            <input type="email" class="form-control" placeholder="Ваш E-mail" name="email" required>
          </div>
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text">+7</div>
            </div>
            <input type="text" class="form-control" placeholder="Ваш телефон" name="phone" maxlength="10" minlength="10"
                   required>
          </div>
          <div class="form-group">
            <textarea name="comment" cols="30" rows="7" class="form-control" placeholder="Комментарий"></textarea>
          </div>
          <div class="form-group">
            <input type="submit" value="Отправить" id="btn_add_consultation_request"
                   class="btn btn-primary py-3 px-5 float-right">
          </div>
        </form>
      </div>
    </div>
  </section>
@endsection

@push('styles')
  <style>
    .home-slider h1 {
      font-size: 50px !important;;
    }

    .modal-content {
      height: 100%;
      border: none;
    }

    .modal-header {
      height: 0;
      padding: 0;
    }

    .modal-header .close {
      position: relative;
      right: -20px;
    }

    .project .iframe {
      padding: 0 20px;
      width: unset;
      color: #fff;
      border-radius: 2em;
    }

    .project .link {
      color: white;
      cursor: pointer;
    }

    .project .text {
      padding-top: 1em;
    }

    .project .text h3 {
      font-size: 1.5em;
    }

    #iframe-loader {
      position: absolute;
    }

    .sect-categories .btn {
      padding: 12px 24px;
      font-weight: 500;
      letter-spacing: 0.7px;
      cursor: default !important;
    }

    @media (min-width: 1440px) {
      .modal-xxl {
        max-width: 1400px;
      }
    }

    @media (min-width: 1240px) {
      .modal-xxl {
        max-width: 1220px;
      }
    }
    @media (max-width: 1440px) {
      .sect-categories .btn-primary {
        font-size: 13px;
      }
    }

    @media (max-width: 991.98px) {
      .home-slider .h6 {
        color: #fff !important;
      }
      .home-slider h1 {
        font-size: 40px !important;
      }
    }
  </style>
@endpush

@push('scripts')
  <script>
      $(document).ready(function () {
          $("#ajax_form").submit(
              function (e) {
                  e.preventDefault();
                  sendAjaxForm('ajax_form', '{{ route('createConsultationRequest') }}');
                  return false;
              }
          );
      });

      function sendAjaxForm(ajax_form, url) {
          $.ajax({
              url: url, //url страницы (action_ajax_form.php)
              type: "POST", //метод отправки
              dataType: "html", //формат данных
              data: $("#" + ajax_form).serialize(),  // Сеарилизуем объект
              beforeSend: function () {
                  $('#btn_add_consultation_request').prop('disabled', true).val('Отправка...');
              },
              success: function (response) { //Данные отправлены успешно
                  // result = $.parseJSON(response);
                  $('#ajax_form').html('<h3 class="text-center" style="color: #0033c7">Спасибо! Наш менеджер свяжется с вами в кратчайшие сроки.</h3>');
              },
              error: function (response) { // Данные не отправлены
                  $('#btn_add_consultation_request').prop('disabled', false).val('Ошибка отправки формы');
              }
          });
      }

      $('a.iframe').click(function (e) {
          e.preventDefault();
          $('#iframe-loader').show();
          $('#demo-iframe').attr('src', $(this).attr('href'));
          $('#preview').modal('show');
      });

      var frame = $('#demo-iframe');
      frame.load(function () {
          $('#iframe-loader').hide();
          // $('#loadingMessage').css('display', 'none');
      });
  </script>
@endpush

@push('modals')
  <div class="modal fade bd-example-modal-lg" id="preview" tabindex="-1" role="dialog"
       aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xxl">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
          <span aria-hidden="true" style="color: #fff;">&times;</span>
        </button>
      </div>
      <div class="modal-content">
        <div id="iframe-loader" class="show fullscreen">
          <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                    stroke="#F96D00"/>
          </svg>
        </div>
        <iframe src="" frameborder="0" id="demo-iframe" style="height: 100%;"></iframe>
      </div>
    </div>
  </div>
@endpush