@extends('layouts.app')

@section('content')
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Первоначальная настройка сайта</h6>
    </div>
    <div class="card-body">
      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div>
        <a href="{{ URL::previous() }}" class="btn btn-light btn-icon-split btn-sm mb-2">
          <span class="icon text-gray-600">
            <i class="fas fa-arrow-left"></i>
          </span>
          <span class="text">К выбору тарифа</span>
        </a>
        <form method="POST" action="{{ route('agent.website.create.setup.selectCategories') }}">
          {{ csrf_field() }}
          <input type="hidden" name="tariff" value="{{ $tariff }}">
          <div class="form-group">
            <label for="name">Название вашего сайта*</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Мой сайт" required>
          </div>
          <div class="form-group">
            <label for="url">Адрес вашего домена (если уже куплен)</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">http://</span>
              </div>
              <input type="text" class="form-control" id="url" name="url" placeholder="адрес-моего-сайта.ru">
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Создать сайт</button>
        </form>
      </div>
    </div>
  </div>
@endsection
