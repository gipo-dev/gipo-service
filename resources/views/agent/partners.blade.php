@extends('layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Партнерская программа</h1>
  <div class="row">
    <div class="col-xl-3 col-md-12 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Ваша партнерская ссылка
                <span class="toolt" data-toggle="tooltip"
                      title="Ваша партнерская ссылка может вести на любую из основных страниц сайта. Например на страницу описания тарифов: https://gipo.ru/tariff?ref={{ 380400 + auth()->user()->id }}">?</span>
              </div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">
                <input type="text" value="https://gipo.ru?ref={{ 380400 + auth()->user()->id }}" class="form-control"
                       style="font-size: 14px;" readonly>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-handshake fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Подключено пользователей</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">
                {{ count($partners) }}
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-user-friends fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Подключено сайтов</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">
                {{ $wc }}
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-globe fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Мои партнеры</h6>
    </div>
    <div class="card-body">
      <div>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
              <th>Имя пользователя</th>
              <th>Дата подключения</th>
              <th>Сайты пользователя</th>
            </tr>
            </thead>
            <tbody>
            @if(count($partners) > 0)
              @foreach($partners as $user)
                <tr>
                  <td>{{ $user->first_name }} {{ $user->second_name }}</td>
                  <td>{{ $user->created_at }}</td>
                  <td>
                    @if(count($user->websites()) > 0)
                      @foreach($user->websites() as $wes)
                        <a href="http://{{$wes->url}}" target="_blank">{{ $wes->url }}</a> <i class="text-xs">({{ $wes->status_name }})</i>,
                      @endforeach
                    @else
                      <i>нет сайтов</i>
                    @endif
                  </td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="3">
                  <h4 class="text-center">Подключенных партнеров нет</h4>
                </td>
              </tr>
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('styles')
  <style>
    .toolt {
      border: 2px solid #597be1;
      padding: 2px;
      border-radius: 50%;
      width: 16px;
      height: 16px;
      display: inline-block;
      text-align: center;
      line-height: 10px;
      margin-left: 3px;
    }
  </style>
@endpush

@push('scripts')
  <script>
      $(function () {
          $('[data-toggle="tooltip"]').tooltip()
      })
  </script>
@endpush