@extends('layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">Управление документами</h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      @if(!$user->legal_entity)
        <h6 class="m-0 font-weight-bold text-primary">Паспортные данные
          <a href="{{ route('agent.settings.changeJuristic') }}"
             class="btn btn-success btn-icon-split btn-sm float-right">
          <span class="icon">
            <i class="fas fa-user-graduate"></i>
          </span>
            <span class="text">Перейти на юридическое лицо</span>
          </a>
        </h6>
      @else
        <h6 class="m-0 font-weight-bold text-primary">Реквизиты юридического лица
          <a href="{{ route('agent.settings.changeJuristic') }}"
             class="btn btn-success btn-icon-split btn-sm float-right">
          <span class="icon">
            <i class="fas fa-user-tie"></i>
          </span>
            <span class="text">Перейти на физическое лицо</span>
          </a>
        </h6>
      @endif
    </div>
    <div class="card-body">
      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div>
        <form action="{{ route('agent.settings.documents.update') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          @if(!$user->legal_entity)
            <div class="form-group"></div>
            <div class="row">
              <div class="col-md-4 form-group">
                <label for="firstname">Имя</label>
                <input type="text" class="form-control" id="firstname" name="1" placeholder="Иван"
                       value="{{ $documents[1] or '' }}" required>
              </div>
              <div class="col-md-4 form-group">
                <label for="lastname">Фамилия</label>
                <input type="text" class="form-control" id="lastname" name="2" placeholder="Фомин"
                       value="{{ $documents[2] or '' }}" required>
              </div>
              <div class="col-md-4 form-group">
                <label for="patronymic">Отчество</label>
                <input type="text" class="form-control" id="patronymic" name="3" placeholder="Александрович"
                       value="{{ $documents[3] or '' }}" required>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 form-group">
                <label for="date">Число</label>
                <input type="number" class="form-control" id="date" name="4" placeholder="17"
                       value="{{ $documents[4] or '' }}" min="1" max="31" maxlength="2"
                       required>
              </div>
              <div class="col-md-4 form-group">
                <label for="date">Месяц</label>
                <input type="number" class="form-control" id="month" name="20" placeholder="12"
                       value="{{ $documents[20] or '' }}" min="1" max="12" maxlength="2"
                       required>
              </div>
              <div class="col-md-4 form-group">
                <label for="date">Год рождения</label>
                <input type="number" class="form-control" id="year" name="21" placeholder="1990"
                       value="{{ $documents[21] or '' }}" min="1900" max="{{ date("Y") }}"
                       maxlength="4" minlength="4" required>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <label for="birthplace">Место рождения</label>
                <input type="text" class="form-control" id="birthplace" name="5"
                       placeholder="гор. Санкт-Петербург" value="{{ $documents[5] or '' }}"
                       required>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 form-group">
                <label for="passport-number">Серия и номер паспорта</label>
                <input type="text" class="form-control" id="passport-number" name="7"
                       placeholder="3208000089" value="{{ $documents[7] or '' }}"
                       minlength="10" maxlength="10" required>
              </div>
              <div class="col-md-4 form-group">
                <label for="passport-date">Дата выдачи</label>
                <input type="text" class="form-control" id="passport-date" name="8"
                       placeholder="21.01.2015" value="{{ $documents[8] or '' }}" required>
              </div>
              <div class="col-md-4 form-group">
                <label for="passport-gave">Кем выдан</label>
                <input type="text" class="form-control" id="passport-gave" name="9"
                       placeholder="отделом УФМС по Санкт-Петербургу в Невском районе Санкт-Петербурга"
                       value="{{ $documents[9] or '' }}" required>
              </div>
            </div>
            <div class="form-group">
              <label for="passport-scan">Скан-копия (фотография высокого качества) третьей страницы паспорта</label>
              @if(isset($documents[22]))
                <div>
                  <img src="{{ asset(\App\Image::find($documents[22])->path) }}" alt="" class="img-thumbnail">
                  <div>
                    <label for="passport-scan" class="help-block"
                           style="text-decoration: underline;font-weight: 400;cursor: pointer;">Заменить фото
                      паспорта</label>
                    <input type="file" id="passport-scan" name="passport_scan" accept="image/*" hidden class="hidden">
                  </div>
                </div>
              @else
                <input type="file" id="passport-scan" name="passport_scan" accept="image/*" required>
              @endif
            </div>
            {{--<div class="form-group">--}}
            {{--<label for="passport-reg-scan">Скан-копия (фотография высокого качества) страницы с регистрацией в паспорте</label>--}}
            {{--@if(isset($documents['Кем выдан']->value))--}}
            {{--@endif--}}
            {{--<input type="file" id="passport-reg-scan" required>--}}
            {{--</div>--}}
          @else
            <div class="row">
              @foreach($required_documents as $doc)
                <div class="col-md-6 form-group">
                  <label for="passport-gave">{{ $doc->name }}</label>
                  <input type="text" class="form-control" id="{{ $doc->id }}" name="{{ $doc->id }}"
                         value="{{ $documents[$doc->id] or '' }}" required>
                </div>
              @endforeach
            </div>
          @endif
          <button type="submit" class="btn btn-primary btn-icon-split">
            <span class="icon">
              <i class="fas fa-save"></i>
            </span>
            <span class="text">Сохранить</span>
          </button>
        </form>
      </div>
    </div>
  </div>


  {{--<div class="panel panel-default">--}}
  {{--<div class="panel-heading">Юридическая информация</div>--}}

  {{--<div class="panel-body">--}}
  {{--@if (session('status'))--}}
  {{--<div class="alert alert-success">--}}
  {{--{{ session('status') }}--}}
  {{--</div>--}}
  {{--@endif--}}
  {{--<div>--}}
  {{--<form action="{{ route('agent.settings.documents.update') }}" method="POST" enctype="multipart/form-data">--}}
  {{--{{ csrf_field() }}--}}
  {{--<div>--}}
  {{--<ul class="nav nav-tabs" role="tablist">--}}
  {{--<li role="presentation" class="active">--}}
  {{--<a href="#passport" aria-controls="passport" role="tab" data-toggle="tab">Паспортные данные</a>--}}
  {{--</li>--}}
  {{--@if($user->legal_entity)--}}
  {{--<li role="presentation" class="">--}}
  {{--<a href="#juristic" aria-controls="juristic" role="tab"--}}
  {{--data-toggle="tab">Юридическая информация</a>--}}
  {{--</li>--}}
  {{--@else--}}
  {{--<a href="" class="btn btn-success" style="margin-top: 4px;">--}}
  {{--<i class="glyphicon glyphicon-education"></i> Сменить статус на юридический--}}
  {{--</a>--}}
  {{--@endif--}}
  {{--</ul>--}}
  {{--<div class="tab-content">--}}
  {{--<div role="tabpanel" class="tab-pane active" id="passport">--}}
  {{--<div class="form-group"></div>--}}
  {{--<div class="row">--}}
  {{--<div class="col-md-4 form-group">--}}
  {{--<label for="firstname">Имя</label>--}}
  {{--<input type="text" class="form-control" id="firstname" name="1" placeholder="Иван"--}}
  {{--value="{{ $documents[1] or '' }}" required>--}}
  {{--</div>--}}
  {{--<div class="col-md-4 form-group">--}}
  {{--<label for="lastname">Фамилия</label>--}}
  {{--<input type="text" class="form-control" id="lastname" name="2" placeholder="Фомин"--}}
  {{--value="{{ $documents[2] or '' }}" required>--}}
  {{--</div>--}}
  {{--<div class="col-md-4 form-group">--}}
  {{--<label for="patronymic">Отчество</label>--}}
  {{--<input type="text" class="form-control" id="patronymic" name="3" placeholder="Александрович"--}}
  {{--value="{{ $documents[3] or '' }}" required>--}}
  {{--</div>--}}
  {{--</div>--}}
  {{--<div class="row">--}}
  {{--<div class="col-md-4 form-group">--}}
  {{--<label for="date">Число</label>--}}
  {{--<input type="number" class="form-control" id="date" name="4" placeholder="17"--}}
  {{--value="{{ $documents[4] or '' }}" min="1" max="31" maxlength="2"--}}
  {{--required>--}}
  {{--</div>--}}
  {{--<div class="col-md-4 form-group">--}}
  {{--<label for="date">Месяц</label>--}}
  {{--<input type="number" class="form-control" id="month" name="20" placeholder="12"--}}
  {{--value="{{ $documents[20] or '' }}" min="1" max="12" maxlength="2"--}}
  {{--required>--}}
  {{--</div>--}}
  {{--<div class="col-md-4 form-group">--}}
  {{--<label for="date">Год рождения</label>--}}
  {{--<input type="number" class="form-control" id="year" name="21" placeholder="1990"--}}
  {{--value="{{ $documents[21] or '' }}" min="1900" max="{{ date("Y") }}"--}}
  {{--maxlength="4" minlength="4" required>--}}
  {{--</div>--}}
  {{--</div>--}}
  {{--<div class="row">--}}
  {{--<div class="col-md-12 form-group">--}}
  {{--<label for="birthplace">Место рождения</label>--}}
  {{--<input type="text" class="form-control" id="birthplace" name="5"--}}
  {{--placeholder="гор. Санкт-Петербург" value="{{ $documents[5] or '' }}"--}}
  {{--required>--}}
  {{--</div>--}}
  {{--</div>--}}
  {{--<div class="row">--}}
  {{--<div class="col-md-4 form-group">--}}
  {{--<label for="passport-number">Серия и номер паспорта</label>--}}
  {{--<input type="text" class="form-control" id="passport-number" name="7"--}}
  {{--placeholder="3208000089" value="{{ $documents[7] or '' }}"--}}
  {{--minlength="10" maxlength="10" required>--}}
  {{--</div>--}}
  {{--<div class="col-md-4 form-group">--}}
  {{--<label for="passport-date">Дата выдачи</label>--}}
  {{--<input type="text" class="form-control" id="passport-date" name="8"--}}
  {{--placeholder="21.01.2015" value="{{ $documents[8] or '' }}" required>--}}
  {{--</div>--}}
  {{--<div class="col-md-4 form-group">--}}
  {{--<label for="passport-gave">Кем выдан</label>--}}
  {{--<input type="text" class="form-control" id="passport-gave" name="9"--}}
  {{--placeholder="отделом УФМС по Санкт-Петербургу в Невском районе Санкт-Петербурга"--}}
  {{--value="{{ $documents[9] or '' }}" required>--}}
  {{--</div>--}}
  {{--</div>--}}
  {{--<div class="form-group">--}}
  {{--<label for="passport-scan">Скан-копия (фотография высокого качества) третьей страницы паспорта</label>--}}
  {{--@if(isset($documents[22]))--}}
  {{--<div>--}}
  {{--<img src="{{ asset(\App\Image::find($documents[22])->path) }}" alt="" class="img-thumbnail">--}}
  {{--<label for="passport-scan" class="help-block"--}}
  {{--style="text-decoration: underline;font-weight: 400;cursor: pointer;">Вы также можете--}}
  {{--заменить фото паспорта</label>--}}
  {{--<input type="file" id="passport-scan" name="passport_scan" accept="image/*" class="hidden">--}}
  {{--</div>--}}
  {{--@else--}}
  {{--<input type="file" id="passport-scan" name="passport_scan" accept="image/*" required>--}}
  {{--@endif--}}
  {{--</div>--}}
  {{--<div class="form-group">--}}
  {{--<label for="passport-reg-scan">Скан-копия (фотография высокого качества) страницы с регистрацией в паспорте</label>--}}
  {{--@if(isset($documents['Кем выдан']->value))--}}
  {{--@endif--}}
  {{--<input type="file" id="passport-reg-scan" required>--}}
  {{--</div>--}}
  {{--</div>--}}
  {{--@if($user->legal_entity)--}}
  {{--<div role="tabpanel" class="tab-pane" id="juristic">456</div>--}}
  {{--@endif--}}
  {{--</div>--}}
  {{--</div>--}}
  {{--<button type="submit" class="btn btn-primary">--}}
  {{--<span class="glyphicon glyphicon-floppy-disk"></span> Сохранить--}}
  {{--</button>--}}
  {{--</form>--}}
  {{--</div>--}}
  {{--</div>--}}
  {{--</div>--}}
@endsection

@push('styles')
  <style>
    .img-thumbnail {
      max-width: 300px;
      max-height: 500px;
    }
  </style>
@endpush