@extends('layouts.app')

@section('content')
  @verbatim
    <div id="products">
      <div class="row mb-3 px-3">
        <button class="btn btn-danger btn-sm" @click="unmarkAll()">
          Снять отметки со всех товаров
        </button>
        <div class="form-group ml-2 mb-0 justify-self-start">
          <select @change="withSelected" class="form-control form-control-sm btn-primary">
            <option disabled selected value="0">С выделеными</option>
            <option value="1">Добавить метку</option>
            <option value="2">Снять метку</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="mb-4 col-12" :class="{ 'col-md-8 col-md-9' : filters.show }">
          <div class="shadow card">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Список товаров для выгрузки в yml <span
                  class="text-xs">{{ website_url }}</span> <i
                  class="text-xs">(alpha)</i>
                <div class="float-right">
                  <span class="text-xs mr-1">Показывать по: </span>
                  <select v-model="limit" @change="getProducts">
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                  </select>
                </div>
              </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <div v-if="loading" class="loader d-flex justify-content-center">
                  <div class="spinner-border mt-5" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                </div>
                <table v-else class="table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Выгружать</th>
                    <th>Id</th>
                    <th>Наименование</th>
                    <th>Остаток</th>
                    <th>Опт</th>
                    <th>РРЦ</th>
                    <th>Конкурент</th>
                    <th>Цена</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr v-for="(product, i) in products.products" v-bind:key="product.id" :data-index="i"
                      @click.ctrl.exact="mark([product])" @click="select(i)" @click.shift="shiftSelect(i)"
                      :class="{'border-left-primary' : selected.items.indexOf(i) != -1}">
                    <td @click.exact="mark([product])">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" :id="'mark'+ product.id"
                               v-model="product.data.marked">
                        <label class="custom-control-label" :for="'mark'+ product.data.product_id"></label>
                      </div>
                    </td>
                    <td>{{ product.data.product_id }}</td>
                    <td>{{ product.name }}</td>
                    <td>{{ product.count }}</td>
                    <td>{{ product.opt_price }}</td>
                    <td>{{ product.recommended_price }}</td>
                    <td>{{ product.yandex_minimal_price }}</td>
                    <td>
                      <input type="number" min="0" v-model:value="product.data.price" @change="changePrice(product)"
                             class="numb">
                    </td>
                  </tr>
                  <tr v-if="products.products.length < 1">
                    <td colspan="8"><h4 class="text-center">Товаров нет</h4></td>
                  </tr>
                  </tbody>
                </table>
                <ul class="pagination">
                  <li class="page-item" v-bind:class="{ disabled: !prevNext.prev }" @click="changePage(-1)">
                    <a class="page-link" href="#">Назад</a>
                  </li>
                  <li class="page-item disabled">
                    <select class="p-select" v-if="totalPages" v-model:value="page" @change="getProducts">
                      <option v-for="index in totalPages"
                              :value="index">{{ index }} {{ index == page ? '/' + totalPages : '' }}</option>
                    </select>
                  </li>
                  <li class="page-item" v-bind:class="{ disabled: !prevNext.next }" @click="changePage(1)">
                    <a class="page-link" href="#">Вперед</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <span class="btn-filter btn btn-primary" v-if="!filters.show" @click.prevent="filters.show = true">
          <span class="fa fa-cog"></span>
        </span>
        </div>
        <div class="col-xl-3 col-md-4" v-if="filters.show">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary d-inline-block"><span class="fa fa-cog"></span> Фильтр</h6>
              <a href="!" @click.prevent="filters.show = false" class="text-xs">Скрыть</a>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="method">Показать</label>
                <select class="custom-select" id="method" v-model:value="filters.method">
                  <option value="0">Все</option>
                  <option value="1">Только отмеченные</option>
                  <option value="2">Только не отмеченные</option>
                </select>
              </div>
              <button class="btn btn-primary" @click="getProducts()">Показать</button>
              <a href="" class="btn btn-danger"><i class="fa fa-times"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endverbatim
@endsection

@push('scripts')
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script>
      var app = new Vue({
          el: '#products',
          data: function () {
              return {
                  website_url: '{{ $website->f_url or '' }}',
                  products: [],
                  page: 1,
                  limit: 20,
                  loading: true,
                  selected: {
                      last: null,
                      items: [],
                  },
                  prevNext: {
                      prev: false,
                      next: true,
                  },
                  filters: {
                      show: true,
                      id: '',
                      method: 0,
                  }
              }
          },
          methods: {
              getProducts: function () {
                  this.loading = true;

                  var _where = '';

                  if (this.filters.method != '0') {
                      switch (this.filters.method) {
                          case '1':
                              _where += " WHERE `isbn` = '1' ";
                              break;
                          case '2':
                              _where += " WHERE `isbn` = '' ";
                              break;
                      }
                  }

                  // if(this.filters.id != '' || this.filters.method != 0) {
                  //     _where += ' WHERE';
                  //     var and = false;
                  //
                  //     if(this.filters.id != '') {
                  //         _where += ' `id` = '+ this.filters.id;
                  //     }
                  //     _where += ' ';
                  // }
                  console.log(_where);

                  axios.post('', {
                      type: 'products',
                      to: 'products',
                      page: this.page - 1,
                      limit: this.limit,
                      where: _where,
                  })
                      .then((response) => {
                          console.log(response);
                          this.products = response.data;
                          this.loading = false;
                      })
                      .catch((error) => {
                          console.log(error);
                          this.loading = false;
                      });
              },
              sendRequest: function (dat = {}) {
                  axios.post('', dat)
                      .then((response) => {
                          return response.data;
                      })
                      .catch((error) => {
                          console.log(error);
                      });
              },
              changePage: function (count) {
                  if (count == -1 && !this.prevNext.prev)
                      return;
                  else if (count == 1 && !this.prevNext.next)
                      return;

                  this.page += count;
                  var changed = true;
                  if (this.page < 1)
                      this.page = 1;
                  if (this.page > this.totalPages)
                      this.page = this.totalPages;
                  if (this.page == 1)
                      this.prevNext.prev = false;
                  else
                      this.prevNext.prev = true;
                  if (this.page == this.totalPages)
                      this.prevNext.next = false;
                  else
                      this.prevNext.next = true;
                  this.getProducts();
              },
              mark: function (itemArray, mark = null) {
                  itemArray.forEach(function (item, i) {
                      if(mark == null) {
                        if (item.data.marked)
                            item.data.marked = false;
                        else
                            item.data.marked = true;
                      } else {
                          item.data.marked = mark == 1 ? true : false;
                      }
                      axios.post('', {
                          to: 'mark',
                          products: [
                              item.id,
                          ],
                          mark: mark != null ? mark : (item.data.marked ? 1 : 0),
                      })
                          .then((response) => {
                              console.log(response);
                          })
                          .catch((error) => {
                              console.log(error);
                          });
                  });
              },
              changePrice: function (item) {
                  if (item.data.price < item.opt_price || item.data.price == "") {
                      item.data.price = item.recommended_price;
                      return;
                  }
                  axios.post('', {
                      to: 'set_price',
                      product_id: item.id,
                      price: item.data.price,
                  })
                      .then((response) => {
                          console.log(response);
                      })
                      .catch((error) => {
                          console.log(error);
                      });
              },
              unmarkAll: function () {
                  if (confirm('Метки со всех выгружаемых будут сняты!')) {
                      axios.post('', {
                          to: 'unmark_all',
                          agree: 1,
                      })
                          .then((response) => {
                              console.log(response);
                          })
                          .catch((error) => {
                              console.log(error);
                          });
                      this.getProducts();
                  }
              },
              select: function (i) {
                  if (event.shiftKey)
                      return;
                  this.selected.last = i;
                  this.selected.items = [i];
              },
              shiftSelect: function (i) {
                  if (window.getSelection) {
                      window.getSelection().removeAllRanges();
                  } else {
                      document.selection.empty();
                  }

                  this.selected.items = [];
                  var min = this.selected.last > i ? i : this.selected.last;
                  var max = this.selected.last > i ? this.selected.last : i;

                  var v = this;
                  $('tr[data-index]').each(function (i, elem) {
                      var elIn = $(elem).data('index');
                      if (elIn >= min && elIn <= max) {
                          v.selected.items.push(elIn);
                      }
                  });
              },
              withSelected: function (e) {
                  var el = this;
                  var ev = $(e.target).val();
                  $(e.target).val(0);

                  var toMark = [];
                  this.selected.items.forEach(function (elem, i) {
                      toMark.push( el.products.products[elem] );
                  });
                  if(ev == 1) {
                      el.mark(toMark, 1);
                  }
                  else if(ev == 2) {
                      el.mark(toMark, 0);
                  }
              },
          },
          computed: {
              totalPages: function () {
                  return Math.round(this.products.total / this.limit);
              },
          },
          created: function () {
              this.getProducts();
          }
      });
  </script>
@endpush

@push('styles')
  <style>
    .loader {
      position: absolute;
      left: 0;
      z-index: 999;
      background: #ffffff;
      width: 100%;
      height: 100%;
    }

    .numb {
      width: 90px;
      text-align: center;
      border: none;
      border-bottom: 2px solid #d1d3da;
      background: none;
    }

    .p-select {
      padding: 0.5rem 0.75rem;
      border: 1px solid #dddfeb;
      color: #858796;
      border-left: none;
      height: 38px;
    }

    .table-hover tbody tr.selected {
      color: #858796;
      background-color: rgba(0, 0, 0, 0.075);
    }

    .btn-filter {
      position: fixed;
      right: 10px;
      top: 75px;
      z-index: 1000;
    }
  </style>
@endpush