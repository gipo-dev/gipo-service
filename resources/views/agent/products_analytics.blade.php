@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="mb-4 col-md-8 col-xl-9">
      <div class="shadow card">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Список товаров</h6>
        </div>
        <div class="card-body">
          @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
          @endif
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="table-products">
              <thead>
              <tr>
                <th>Выгружать</th>
                <th></th>
                <th>id</th>
                <th>Наименование</th>
                <th>Остаток</th>
                <th>Опт</th>
                <th>РРЦ</th>
                <th>Конкурент</th>
                <th>Разница</th>
              </tr>
              </thead>
              <tbody>
              @foreach($products as $product)
                <tr
                  class="{{ $product->yandex_minimal_price > 0 && $product->agent_price > 0 && $product->agent_price / $product->yandex_minimal_price > 1 ? '' : 'border-right-success' }}">
                  <td>
                    @if(isset($product->marked))
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="mark{{ $product->id }}"
                               {{$product->marked == '1' ? 'checked' : '' }} data-pid="{{ $product->id }}">
                        <label class="custom-control-label" for="mark{{ $product->id }}"></label>
                      </div>
                    @else
                      <i>нет</i>
                    @endif
                  </td>
                  <td>
                    <img src="{{ $product->image }}" alt="" style="max-height: 50px;max-width: 50px;">
                  </td>
                  <td>{{ $product->id }}</td>
                  <td>{{ $product->name }}</td>
                  <td>{{ $product->count }}</td>
                  <td>{{ $product->agent_price }}</td>
                  <td>{{ $product->recommended_price }}</td>
                  <td>{{ $product->yandex_minimal_price }}</td>
                  <td>{{ $product->yandex_minimal_price > 0 && $product->agent_price > 0 ? (100 * round(1 - ($product->agent_price / $product->yandex_minimal_price), 2)) : 0 }}
                    % ({{ $product->agent_price - $product->yandex_minimal_price }})
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
            {{ $products->appends($_GET)->links() }}
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary"><span class="fa fa-cog"></span> Фильтр</h6>
        </div>
        <div class="card-body">
          <form action="">
            <div class="form-group">
              <label for="id">ID</label>
              <input type="number" class="form-control" id="id" name="id" min="1"
                     value="{{ \Illuminate\Support\Facades\Input::get('id') }}">
            </div>
            <div class="form-group">
              <label for="name">Наименование</label>
              <input type="text" class="form-control" id="name" name="name" minlength="3" value="{{ request('name') }}">
            </div>
            <div class="form-group">
              <label for="method">Сортировать</label>
              <select class="custom-select" name="method" id="method">
                <option value="0">Стандартно</option>
                <option value="1" {{ request('method') == 1 ? 'selected' : '' }}>Наибольший заработок, %</option>
                <option value="2" {{ request('method') == 2 ? 'selected' : '' }}>Наибольший заработок, Р</option>
              </select>
            </div>
            <button class="btn btn-primary">Показать</button>
            <a href="{{ route('agent.product.analytics', request()->route()->id) }}" class="btn btn-danger"><i
                class="fa fa-times"></i></a>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script>
    $('#table-products input[type=checkbox]').change(function (e) {
        var mark = $(this).prop('checked');
        $.ajax({
            url: '',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                to: 'mark',
                products: [
                    $(this).data('pid'),
                ],
                mark: mark ? '1': '0',
            },
            success: function (data) {
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });
  </script>
@endpush