@extends('layouts.app')

@section('content')
  <h1 class="h3 mb-4 text-gray-800">{{ $website->name }}
    <small>
      <a href="{{ $website->url != '' ? 'http://'.$website->url : '' }}"
         target="_blank">{{ $website->url != '' ? '('.$website->url.')' : '' }}</a>
    </small>
  </h1>
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Управление сайтом</h6>
    </div>
    <div class="card-body">

      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <div>
        @if($website->status_id == 1)
          <h2 class="text-center">Для начала работы необходимо оплатить тариф</h2>
          <p class="text-center">Для оплаты свяжитесь с менеджером сервиса</p>
        @elseif($website->status_id == 3)
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Id</label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control-plaintext" value="{{ $website->id }}">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Статус</label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control-plaintext" value="{{ $website->status_name }}">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Оплачен до</label>
            <div class="col-sm-10">
              <input type="text" readonly class="form-control-plaintext"
                     value="{{ $website->paid_to->format('d.m.Y') }}">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Текущий тариф</label>
            <div class="col-sm-10">
              <span class="btn btn-primary btn-sm">{{ $website->tariff_name }}</span>
              @if($website->tariff_id != 3)
                <a href="#!" class="btn btn-success btn-sm" title="Повысить тариф"><span
                    class="fa fa-level-up-alt"></span></a>
              @endif
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Ваш api-ключ</label>
            <div class="col-sm-10">
              <input type="text" readonly value="{{ $website->api_token or 'Не создан' }}"
                     class="form-control form-control-sm" style="display: inline-block;width: unset">
              <btn class="btn btn-primary btn-sm" data-toggle="modal" data-target="#new-token">Генерировать новый
              </btn>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Наценка на опт. цену</label>
            <form action="{{ route('agent.website.setOptMarkup', request()->id) }}" method="POST" class="col-sm-10">
              {{ csrf_field() }}
              <div class="input-group">
                <input type="number" min="0" class="form-control col-lg-1 col-sm-2"
                       value="{{ $settings['opt_markup']->value or $settings['opt_markup']->def }}" name="markup">
                <div class="input-group-append">
                  <div class="input-group-text">%</div>
                </div>
                <input type="submit" class="btn btn-primary ml-1" value="Сохранить"></input>
              </div>
            </form>
          </div>
          <a href="{{ route('agent.product.analytics', request()->id) }}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-chart-area"></i>
                    </span>
            <span class="text">Аналитика цен</span>
          </a>
          <a href="{{ route('agent.product.yml', request()->id) }}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-list"></i>
                    </span>
            <span class="text">Выгрузка yml</span>
          </a>
        @else
          <h3 class="text-center">Вебсайт {{ $website->status_name }}</h3>
        @endif
      </div>
    </div>
  </div>

  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">История заказов</h6>
    </div>
    <div class="card-body">
      <div>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
              <th style="width: 1%;">№</th>
              <th>Id заказа в вашем магазине</th>
              <th>Сумма</th>
              <th>Статус заказа</th>
              <th>Дата обновления</th>
              <th>Дата добавления</th>
            </tr>
            </thead>
            <tbody>
            @if($orders)
              @foreach($orders as $order)
                <tr>
                  <td>{{ $order->id }}</td>
                  <td>{{ $order->website_order_local_id }}</td>
                  <td>{{ $order->order_data->totals[2]->value }}</td>
                  <td>{{ $order->status_name }}</td>
                  <td>{{ $order->updated_at }}</td>
                  <td>{{ $order->created_at }}</td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="5">
                  <h5>Заказов в этом магазине пока что нет</h5>
                </td>
              </tr>
            @endif
            </tbody>
          </table>
          @if($orders)
            {{ $orders->links() }}
          @endif
        </div>
      </div>
    </div>
  </div>
@endsection

@push('modals')
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
       aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="new-token" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Генерация нового api-ключа</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><b>Внимание!</b> При генерации нового api ключа, <b>прошлый api-ключ будет неработоспособен.</b> Не
            забудьте обновить api-ключ в модуле синхронизации товаров в вашем интернет-магазине.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Отмена</button>
          <a href="{{ route('agent.website.generateApiToken', ['id' => $website->id]) }}"
             class="btn btn-danger btn-icon-split">
            <span class="icon text-white-50">
              <i class="fa fa-sync-alt"></i>
            </span>
            <span class="text">Сгенерировать новый</span>
          </a>
        </div>
      </div>
    </div>
  </div>
@endpush

@push('scripts')
  <script>
      $('#generate-token').click(function (e) {
          e.preventDefault();
      });
  </script>
@endpush
