@extends('layouts.app')

@section('content')
  <h1 class="h1 mb-5 text-center text-gray-800">Выберите подходящий вам тарифный план</h1>
  <div class="row justify-content-md-center">
    <div class="pricing card-deck flex-column flex-md-row mb-3 col-xl-9">
      @foreach($tariffs as $k => $t)
        <div class="card card-pricing text-center px-3 mb-4 col-md-6 {{ $k == 2 ? 'popular shadow' : '' }}">
            <span
              class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">{{ $t['name'] }}</span>
          <div class="bg-transparent card-header pt-4 border-0">
            <h1 class="h1 font-weight-normal text-primary text-center mb-0" data-pricing-value="15"><span
                class="price">{{ $t['price'] }}</span><span class="fa fa-ruble-sign"
                                                            style="font-size: 24px;"></span><span
                class="h6 text-muted ml-2">/ в год</span></h1>
          </div>
          <div class="card-body pt-0">
            <ul class="list-unstyled mb-4">
              @foreach($t['features'] as $f)
                <li>{{ $f['name'] }}</li>
              @endforeach
            </ul>
            <a href="{{ route('agent.website.create.setup', [ 'tariff' => $k ]) }}"
               class="btn btn-{{ $k == 2 ? 'primary' : 'outline-secondary' }} mb-3">Заказать</a>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection

@push('styles')
  <style>
    .pricing {
      align-items: flex-start;
    }
    .card-pricing.popular {
      z-index: 1;
      border: 3px solid #4e73df;
    }

    .card-pricing .list-unstyled li {
      padding: .5rem 0;
      color: #6c757d;
    }
  </style>
@endpush
