@extends('layouts.landingapp')

@push('title') {{ $article->title }} | гипо.ру @endpush

@section('content')
  <section class="hero-wrap hero-wrap-2 overflow-hidden">
    <div class="overlay"
         style="background-image: url('{{ $article->image or '/img/landing/head-1.jpg' }}');filter: blur(5px);-webkit-background-size: cover;background-size: cover;background-position: center;"
         data-stellar-background-ratio="0.5"></div>
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-2 bread">{{ $article->title }}</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="/">Главная <i class="ion-ios-arrow-forward"></i></a></span>
            <span><a href="{{ route('articles') }}">Статьи <i class="ion-ios-arrow-forward"></i></a></span></p>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 ftco-animate">
          <p>
            <img src="{{ $article->image }}" alt="" class="img-fluid">
          </p>
          <div class="text-dark" style="font-size: 20px">
            {!! $article->text !!}
          </div>
          <div class="tag-widget post-tag-container mb-5 mt-5">
            <div class="tagcloud">
              @foreach($article->tags as $tag)
                <a href="{{ route('articles', ['tag' => $tag->id]) }}" class="tag-cloud-link">{{ $tag->name }}</a>
              @endforeach
            </div>
          </div>
          <div><a class="mr-3"><span
                  class="icon-calendar"></span> {{ Date::parse($article->created_at)->format('d, M. Y') }}</a>
            <a><span class="icon-person"></span> {{ $article->author_name }}</a></div>
          {{--<div class="about-author d-flex p-4 bg-light">--}}
          {{--<div class="bio mr-5">--}}
          {{--<img src="images/person_1.jpg" alt="Image placeholder" class="img-fluid mb-4">--}}
          {{--</div>--}}
          {{--<div class="desc">--}}
          {{--<h3>George Washington</h3>--}}
          {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus--}}
          {{--voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique,--}}
          {{--inventore eos fugit cupiditate numquam!</p>--}}
          {{--</div>--}}
          {{--</div>--}}
          <div class="pt-5 mt-5" id="comments">
            <h3 class="mb-5 h4 font-weight-bold">{{ $comments_count }} Комментариев</h3>
            <ul class="comment-list">
              @include('partials.user_comment', ['comments' => $article->comments, 'parent_id' => ''])
            </ul>

            <div class="comment-form-wrap pt-5">
              <h3 class="mb-5 h4 font-weight-bold">Оставить комментарий
                <span class="btn btn-dark btn-sm ml-3 px-3" id="cancel-reply" style="display: none;">Отменить ответ</span></h3>
              <form action="" class="p-5 bg-light" id="left_comment" method="POST">
                {{ csrf_field() }}
                <div class="form-group" style="display: none;" id="reply_to_area">
                  <label for="reply_to">Ответить на комментарий</label>
                  <input type="text" class="form-control" id="reply_to" name="reply_to" readonly>
                </div>
                @if(!Auth::user())
                  <div class="form-group">
                    <label for="name">Имя *</label>
                    <input type="text" class="form-control" id="name" name="name" minlength="3" maxlength="50" required>
                  </div>
                @endif
                <div class="form-group">
                  <label for="message">Комментарий</label>
                  <textarea name="message" id="message" cols="30" rows="10" class="form-control" minlength="6"
                            maxlength="500" required></textarea>
                </div>
                <div class="form-group">
                  <input type="submit" value="Оставить комментарий" class="btn py-3 px-4 btn-primary">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-lg-4 sidebar ftco-animate">
          <div class="sidebar-box ftco-animate">
            <h3>Категория</h3>
            <ul class="categories">
              <li>
                <a href="{{ route('articles') }}">Все статьи</a>
              </li>
              @foreach($categories as $category)
                <li>
                  <a href="{{ route('articles', ['category' => $category->id]) }}">{{ $category->name }} <span>({{ $category->articles->count() }})</span></a>
                </li>
              @endforeach
            </ul>
          </div>
          <div class="sidebar-box ftco-animate">
            <h3>Популярные статьи</h3>
            @foreach($populars as $article)
              <div class="block-21 mb-4 d-flex">
                <a href="{{ route('article', $article->id) }}" class="blog-img mr-4"
                   style="background-image: url('{{ $article->image }}');"></a>
                <div class="text">
                  <h3 class="heading"><a href="{{ route('article', $article->id) }}">{{ $article->title }}</a>
                  </h3>
                  <div class="meta">
                    <div><a><span
                            class="icon-calendar"></span> {{ Date::parse($article->created_at)->format('d, M. Y') }}</a>
                    </div>
                    <div><a><span class="icon-person"></span> {{ $article->author_name }}</a></div>
                    <div><a><span class="icon-chat"></span> {{ $article->comments->count() }}</a></div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="sidebar-box ftco-animate">
            <h3>Облако тегов</h3>
            <ul class="tagcloud m-0 p-0">
              @foreach($tags as $tag)
                <a href="{{ route('articles', ['tag' => $tag->id]) }}" class="tag-cloud-link">{{ $tag->name }}</a>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@push('styles')
  <style>
    .article-category > a {
      color: #b3b3b3;
      font-size: 13px;
    }

    .comment-list li .comment-body .meta {
      color: #8e8e8e;
    }
  </style>
@endpush

@push('scripts')
  <script>
      $('.reply-button').click(function () {
          var to = $(this).data('comment_id');
          $('#reply_to').val(to);
          $('#reply_to_area').show();
          $('#cancel-reply').show();
          $('html, body').animate({
              scrollTop: $("#left_comment").offset().top - 200
          }, 500);
      });
      $('#cancel-reply').click(function () {
          $('#reply_to').val('');
          $('#reply_to_area').hide();
          $('#cancel-reply').hide();
      });
  </script>
@endpush