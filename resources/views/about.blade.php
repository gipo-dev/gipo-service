@extends('layouts.landingapp')

@push('title') О компании гипо @endpush

@section('content')
  <section class="hero-wrap hero-wrap-2" style="background-image: url('/img/landing/head-1.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-2 bread">Контактная информация</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="/">Главная <i class="ion-ios-arrow-forward"></i></a></span> <span>Контакты <i class="ion-ios-arrow-forward"></i></span></p>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section contact-section bg-light">
    <div class="container">
      <div class="row d-flex mb-5 contact-info">
        <div class="col-md-12 mb-4">
          <h2 class="h4">Контакты</h2>
        </div>
        <div class="w-100"></div>
        <div class="col-md-4">
          <p><span>Адрес:</span> г.Санкт-Петербург, ул.Магнитогорская 51 лит.Р, офис 201</p>
        </div>
        <div class="col-md-4">
          <p><span>Телефон:</span> <a href="tel://88129517871">8 812 951-78-71</a></p>
        </div>
        <div class="col-md-4">
          <p><span>E-mail:</span> <a href="mailto:info@gipo.ru">info@gipo.ru</a></p>
        </div>
      </div>
      <div class="row d-flex mb-5 contact-info">
        <div class="col-md-12 mb-4">
          <h2 class="h4">Юридическая информация</h2>
        </div>
        <div class="w-100"></div>
        <div class="col-12 col-lg-6">
          <p>Общество с ограниченной ответственностью "ГИПО"</p>
          <p>ИНН: 7801656750</p>
          <p>КПП: 780101001</p>
          <p>ОГРН: 1187847379312</p>
          <p>ОКПО: 35034426</p>
          <p>Юр.адрес: Почтовый индекс 199155, г. Санкт-Петербург, ул. Уральская, дом 12, корпус 5 литер Д, помещение 87 офис 87</p>
          <p>Р/С: № 40702810755000041015</p>
          <p>К/сч: № 30101810500000000653</p>
          <p>БИК 044030653</p>
          <p>ПАО Сбербанк, 191023, г. Санкт-Петербург, ул. Садовая дом 28-30 корпус 3 ИНН банка 7707083893, КПП: 784243001, ОГРН: 1027700132195, ОКПО: 09171401</p>
        </div>
      </div>
    </div>
  </section>
@endsection