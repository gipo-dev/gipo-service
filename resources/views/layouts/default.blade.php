<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title')</title>
  <title>@yield('description')</title>

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,700,900&amp;subset=cyrillic" rel="stylesheet">
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  @stack('styles')
</head>
<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(53733157, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/53733157" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<div id="app">
  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">

        <!-- Collapsed Hamburger -->
        {{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">--}}
          {{--<span class="sr-only">Toggle Navigation</span>--}}
          {{--<span class="icon-bar"></span>--}}
          {{--<span class="icon-bar"></span>--}}
          {{--<span class="icon-bar"></span>--}}
        {{--</button>--}}

        <!-- Branding Image -->
        <a class="navbar-brand" href="/">
          gipo.ru
        </a>
      </div>

      {{--<div class="collapse navbar-collapse" id="app-navbar-collapse">--}}
        {{--<!-- Left Side Of Navbar -->--}}
        {{--<ul class="nav navbar-nav">--}}
          {{--&nbsp;--}}
        {{--</ul>--}}

        {{--<!-- Right Side Of Navbar -->--}}
        {{--<ul class="nav navbar-nav navbar-right">--}}
          {{--<!-- Authentication Links -->--}}
          {{--@guest--}}
            {{--<li><a href="{{ route('login') }}">Вход</a></li>--}}
            {{--<li><a href="{{ route('register') }}">Регистрация</a></li>--}}
          {{--@else--}}
            {{--<li>--}}
              {{--<a href="#!"><b style="font-size: 18px;">{{ Auth::user()->balance }}</b><span class="glyphicon glyphicon-rub"></span></a>--}}
            {{--</li>--}}
            {{--<li class="dropdown">--}}
              {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>--}}
                {{--{{ Auth::user()->first_name }} <span class="caret"></span>--}}
              {{--</a>--}}

              {{--<ul class="dropdown-menu">--}}
                {{--<li>--}}
                  {{--<a href="{{ route('logout') }}"--}}
                     {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                    {{--Выход--}}
                  {{--</a>--}}

                  {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                    {{--{{ csrf_field() }}--}}
                  {{--</form>--}}
                {{--</li>--}}
              {{--</ul>--}}
            {{--</li>--}}
          {{--@endguest--}}
        {{--</ul>--}}
      {{--</div>--}}

      <div class="top-right links">
        @auth
          <a class="small" href="{{ url('/home') }}">Личный кабинет</a>
        @else
          <a class="small mr-3" href="{{ route('login') }}">Вход</a>
          <a class="small" href="{{ route('register') }}">Регистрация</a>
        @endauth
      </div>
    </div>
  </nav>
  <div class="">
    {{--<div class="row">--}}
      {{--<div class="col-md-12">--}}
        @yield('content')
      {{--</div>--}}
    {{--</div>--}}
  </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
@stack('scripts')
</body>
</html>
