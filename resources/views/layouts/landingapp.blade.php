<html lang="ru">
<head>
  <title>@stack('title')</title>
  <meta name="keywords" content="Готовый интернет магазин, бизнес под ключ, интернет магазин под ключ, сайт под ключ, готовый бизнес, купить готовый бизнес, бизнес в интернете, заработок в интернете,">
  <meta name="description" content="Первый в России сервис для оптимизации всех основных трудностей при создании интернет магазина">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,400i,500,700&display=swap&subset=cyrillic"
        rel="stylesheet">

  <link rel="stylesheet" href="/css/landing/open-iconic-bootstrap.min.css">
  <link rel="stylesheet" href="/css/landing/animate.css">

  <link rel="stylesheet" href="/css/landing/owl.carousel.min.css">
  <link rel="stylesheet" href="/css/landing/owl.theme.default.min.css">
  <link rel="stylesheet" href="/css/landing/magnific-popup.css">

  <link rel="stylesheet" href="/css/landing/aos.css">

  <link rel="stylesheet" href="/css/landing/ionicons.min.css">

  <link rel="stylesheet" href="/css/landing/bootstrap-datepicker.css">
  <link rel="stylesheet" href="/css/landing/jquery.timepicker.css">


  <link rel="stylesheet" href="/css/landing/flaticon.css">
  <link rel="stylesheet" href="/css/fonts/landing/flaticon/_flaticon.css">
  <link rel="stylesheet" href="/css/landing/icomoon.css">
  <link rel="stylesheet" href="/css/landing/style.css?v=3712">
  <style
    type="text/css">.scrollax-performance, .scrollax-performance *, .scrollax-performance *:before, .scrollax-performance *:after {
      pointer-events: none !important;
      -webkit-animation-play-state: paused !important;
      animation-play-state: paused !important;
    }</style>
  @stack('styles')
</head>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(53733157, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/53733157" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<body data-aos-easing="slide" data-aos-duration="800" data-aos-delay="0">

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
  <div class="container">
    <a class="navbar-brand m-0" href="/">
      <img src="/img/logotype.svg" alt="gipo.ru" style="max-width: 200px; width: 100%; height: 40px;">
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav"
            aria-expanded="false" aria-label="Toggle navigation">
      <span class="oi oi-menu"></span> Меню
    </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
      <ul class="navbar-nav ml-auto">
        {{--<li class="nav-item active"><a href="/dropshipping" class="nav-link">Дропшиппинг</a></li>--}}
        <li class="nav-item"><a href="/#feature" class="nav-link">Сервисы</a></li>
        <li class="nav-item"><a href="/tariff" class="nav-link">Тарифы</a></li>
        {{--<li class="nav-item"><a href="/#partners" class="nav-link">Партнерская программа</a></li>--}}
        <li class="nav-item"><a href="/about" class="nav-link">Контакты</a></li>
        {{--<li class="nav-item"><a href="/blog" class="nav-link">Статьи</a></li>--}}
        @if(Auth::user())
          <li class="nav-item"><a href="/login" class="nav-link">Личный кабинет</a></li>
        @else
          <li class="nav-item"><a href="/login" class="nav-link">Вход</a></li>
        @endif
      </ul>
    </div>
  </div>
</nav>
<!-- END nav -->

@yield('content')

<footer class="ftco-footer ftco-bg-dark ftco-section">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md">
        <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2">Контакты</h2>
          <p class="text-white">Мы находимся г.Санкт-Петербург, ул.Магнитогорская 51 лит.Р, офис 201</p>
          {{--<p class="h3 text-white">8 812 951-78-71</p>--}}
          <p><a href="tel:+78129517871" class="h3 text-white">8 (812) 951-78-71</a></p>
          <p><a href="tel:+79217859020" class="h5">8 921 785-90-20</a></p>
          {{--<p class="h5">8 921 954-61-82</p>--}}
          <p class="h5"><a href="mailto:info@gipo.ru">info@gipo.ru</a></p>
        </div>
      </div>
      <div class="col-md">
        <div class="ftco-footer-widget mb-4 ml-md-4">
          <h2 class="ftco-heading-2">Ссылки</h2>
          <ul class="list-unstyled">
            <li><a href="/"><span class="icon-long-arrow-right mr-2"></span>На главную</a></li>
            <!--<li><a href="https://market.gipo.ru"><span class="icon-long-arrow-right mr-2"></span>Маркет</a></li>-->
            <li><a href="#feature"><span class="icon-long-arrow-right mr-2"></span>Сервисы</a></li>
            <li><a href="#pricing"><span class="icon-long-arrow-right mr-2"></span>Тарифы</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md">
        <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2">Сервисы</h2>
          <ul class="list-unstyled">
            <li><a href="/about"><span class="icon-long-arrow-right mr-2"></span>Контакты</a></li>
            <li><a href="/login"><span class="icon-long-arrow-right mr-2"></span>Вход</a></li>
            <!--<li><a href="#feature"><span class="icon-long-arrow-right mr-2"></span>Товары</a></li>-->
            <!--<li><a href="#feature"><span class="icon-long-arrow-right mr-2"></span>Платформа магазина</a></li>-->
            <!--<li><a href="#feature"><span class="icon-long-arrow-right mr-2"></span>Доставка</a></li>-->
            <!--<li><a href="#feature"><span class="icon-long-arrow-right mr-2"></span>Call-центр</a></li>-->
          </ul>
        </div>
      </div>
      <div class="col-md">
        <div class="ftco-footer-widget mb-4">
          <h2 class="ftco-heading-2">Подписаться на рассылку</h2>
          <p>Получайте спецпредложения и новости первым.</p>
          <form action="#" class="subscribe-form">
            <div class="form-group">
              <input type="text" class="form-control mb-2 text-center" placeholder="Введите ваш E-mail">
              <input type="submit" value="Подписаться" class="form-control submit px-3">
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">

        <p>
          Общество с ограниченной ответственностью "Гипо" ©
          <script>document.write(new Date().getFullYear());</script>
        </p>
      </div>
    </div>
  </div>
</footer>

@stack('modals')

<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

<script src="/js/landing/jquery.min.js"></script>
<script src="/js/landing/jquery-migrate-3.0.1.min.js"></script>
<script src="/js/landing/popper.min.js"></script>
<script src="/js/landing/bootstrap.min.js"></script>
<script src="/js/landing/jquery.easing.1.3.js"></script>
<script src="/js/landing/jquery.waypoints.min.js"></script>
<script src="/js/landing/jquery.stellar.min.js"></script>
<script src="/js/landing/owl.carousel.min.js"></script>
<script src="/js/landing/jquery.magnific-popup.min.js"></script>
<script src="/js/landing/aos.js"></script>
<script src="/js/landing/jquery.animateNumber.min.js"></script>
<script src="/js/landing/bootstrap-datepicker.js"></script>
<script src="/js/landing/scrollax.min.js"></script>
<script
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&amp;sensor=false"></script>
<script src="/js/landing/google-map.js"></script>
<script src="/js/landing/main.js"></script>

@stack('scripts')

</body>
</html>