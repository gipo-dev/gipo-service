@if(isset($comments[$parent_id]))
  @foreach($comments[$parent_id] as $comment)
    <li class="comment">
      <div class="vcard bio">
        <img src="/img/user_default_avatar.jpeg" alt="Image placeholder">
      </div>
      <div class="comment-body">
        <p class="h3"><span class="meta">#{{ $comment->id }}</span>
          {{ $comment->author_name }}
          @if($comment->author_admin == 1)
            <span class="text-info font-italic meta">админ</span>
          @endif
          @if($comment->reply_to)
            <span class="meta">ответил на #{{ $comment->reply_to }}</span>
          @endif
        </p>
        <div class="meta mb-2">{{ Date::parse($comment->created_at)->format('d, M. Y H:m') }}</div>
        <p class="text-dark" style="font-size: 20px;">{{ $comment->text }}</p>
        <p><span class="reply reply-button" data-comment_id="{{ $comment->id }}"
                 style="cursor: pointer;">Ответить</span>
        </p>
      </div>
      @if(isset($comments[$comment->id]))
        <ul class="children">
          @include('partials.user_comment', ['comments' => $comments, 'parent_id' => $comment->id])
        </ul>
      @endif
    </li>
  @endforeach
@endif