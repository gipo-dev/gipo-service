<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title')</title>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
  <!-- Styles -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  @stack('styles')
</head>
<body>
<div class="container-fluid d-flex justify-content-center" style="height: 100vh;width: 100%;">

  <!-- 404 Error Text -->
  <div class="text-center align-self-center">
    <div class="error mx-auto" data-text="{{ $exception->getStatusCode() }}">{{ $exception->getStatusCode() }}</div>
    <p class="lead text-gray-800 mb-5">Внутренняя ошибка сервера</p>
    <p class="text-gray-500 mb-0">Судя по всему, кто-то снова пытается взломать матрицу...</p>
    <a href="/">← На главную</a>
  </div>

</div>
</body>