@extends('layouts.landingapp')

@section('content')
  <section class="ftco-section section ftco-no-pb" id="pricing">
    <!-- Content -->
    <div class="container">
      <div class="row justify-content-center mb-4">
        <div class="col-md-8 col-lg-6 text-center">
          <div class="section-heading">
            <h2 class="section-title">
              Доступные тарифные планы
            </h2>
            <p>
              Выберите тариф, который лучше всего для вас подходит.
            </p>
          </div>
        </div>
      </div>

      <div class="row justify-content-center">
        {{--<div class="col-lg-4 col-sm-6 col-md-6">--}}
        {{--<div class="pricing-box">--}}
        {{--<h3>Минимальный--}}
        {{--<span class="old-price">22,900руб</span>--}}
        {{--</h3>--}}
        {{--<div class="price-block">--}}
        {{--<h2>3,500<span>руб/мес</span></h2>--}}
        {{--</div>--}}

        {{--<ul class="price-features list-unstyled">--}}
        {{--<li>Интернет-магазин</li>--}}
        {{--<li>База товаров gipo</li>--}}
        {{--<li>Минимальные оптовые цены</li>--}}
        {{--<li>Автообновление товаров</li>--}}
        {{--<li>Аналитика цен конкурентов</li>--}}
        {{--</ul>--}}

        {{--<a href="#formmm" class="btn btn-outline-dark btn-circled px-4 py-3">Выбрать <i class="licon-right-arrow"></i></a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-lg-4 col-sm-6 col-md-6">--}}
        {{--<div class="pricing-box">--}}
        {{--<h3>Базовый--}}
        {{--<span class="old-price">12,000руб</span>--}}
        {{--</h3>--}}
        {{--<div class="price-block">--}}
        {{--<h2>9,600<span>руб/мес</span></h2>--}}
        {{--</div>--}}

        {{--<ul class="price-features list-unstyled">--}}
        {{--<li>Интернет-магазин</li>--}}
        {{--<li>База товаров gipo</li>--}}
        {{--<li>Минимальные оптовые цены</li>--}}
        {{--<li>Автообновление товаров</li>--}}
        {{--<li>Аналитика цен конкурентов</li>--}}
        {{--<li>Call-центр</li>--}}
        {{--<li>Служба доставки</li>--}}
        {{--<li>Не нужно закупать товар</li>--}}
        {{--</ul>--}}

        {{--<a href="#formmm" class="btn btn-primary btn-circled px-4 py-3">Выбрать <i class="licon-right-arrow"></i></a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-lg-4 col-sm-8 col-md-6">--}}
        {{--<div class="pricing-box ">--}}
        {{--<h3>Бизнес</h3>--}}
        {{--<div class="price-block">--}}
        {{--<h2>17,000<span>руб/мес</span></h2>--}}
        {{--</div>--}}

        {{--<ul class="price-features list-unstyled">--}}
        {{--<li>Интернет-магазин</li>--}}
        {{--<li>База товаров gipo</li>--}}
        {{--<li>Минимальные оптовые цены</li>--}}
        {{--<li>Автообновление товаров</li>--}}
        {{--<li>Аналитика цен конкурентов</li>--}}
        {{--<li>Call-центр</li>--}}
        {{--<li>Служба доставки</li>--}}
        {{--<li>Не нужно закупать товар</li>--}}
        {{--<li>Дизайн в подарок</li>--}}
        {{--<li>Услуги разработчика<br>10 часов в неделю</li>--}}
        {{--</ul>--}}

        {{--<a href="#formmm" class="btn btn-outline-dark btn-circled px-4 py-3">Выбрать <i class="licon-right-arrow"></i></a>--}}
        {{--</div>--}}
        {{--</div>--}}

        <div class="col-lg-3 col-sm-6 col-md-6">
          <div class="pricing-box">
            <h3>Минимальный
            </h3>
            <div class="price-block">
              <h2>95,000<span>руб</span></h2>
            </div>

            <ul class="price-features list-unstyled">
              <li>Подбор и выбор ниши</li>
              <li>Товарная матрица неограничена</li>
              <li>Регистрация домена и хостинга</li>
              <li>Адаптивный многостраничный сайт</li>
              <li>База товаров с поставщиками</li>
              <li>Обучение</li>
            </ul>

            <a href="#formmm" class="btn btn-outline-dark btn-circled px-4 py-3">Выбрать <i class="licon-right-arrow"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-md-6">
          <div class="pricing-box">
            <h3>Базовый
              {{--<span class="old-price">195,000руб</span>--}}
            </h3>
            <div class="price-block">
              <h2>195,000<span>руб</span></h2>
            </div>

            <ul class="price-features list-unstyled">
              <li>Подбор и выбор ниши</li>
              <li>Товарная матрица неограничена</li>
              <li>Регистрация домена и хостинга</li>
              <li>Адаптивный многостраничный сайт</li>
              <li>База товаров с поставщиками</li>
              <li>Автообновление товаров</li>
              <li>Аналитика цен конкурентов (программа)</li>
              <li>Подключение CRM</li>
              <li>Колл-центр</li>
              <li>Обучение</li>
            </ul>

            <a href="#formmm" class="btn btn-primary btn-circled px-4 py-3">Выбрать <i class="licon-right-arrow"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-sm-8 col-md-6">
          <div class="pricing-box ">
            <h3>Бизнес</h3>
            <div class="price-block">
              <h2>295,000<span>руб</span></h2>
            </div>

            <ul class="price-features list-unstyled">
              <li>Подбор и выбор ниши</li>
              <li>Товарная матрица неограничена</li>
              <li>Регистрация домена и хостинга</li>
              <li>Адаптивный многостраничный сайт</li>
              <li>База товаров с поставщиками</li>
              <li>Автообновление товаров</li>
              <li>Аналитика цен конкурентов (программа)</li>
              <li>Подключение CRM</li>
              <li>Колл-центр</li>
              <li>Обучение</li>
              <li>Логистика (отправка/доставка товара)</li>
              <li>Настройка и ведение 1 месяц Яндекс Директ</li>
              <li>Настройка и ведение 1 месяц Google Adwords</li>
              <li>Запуск VK (1.000 живых подписчиков + 10 постов)</li>
              <li>Бизнес аккаунт в instagram (3.000 живых подписчиков + 10 постов)</li>
              <li>Бизнес-план</li>
              <li>Профессиональное СЕО-продвижение 1 месяц</li>
            </ul>

            <a href="#formmm" class="btn btn-outline-dark btn-circled px-4 py-3">Выбрать <i class="licon-right-arrow"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-sm-8 col-md-6">
          <div class="pricing-box ">
            <h3>Премиум</h3>
            <div class="price-block">
              <h2>395,000<span>руб</span></h2>
            </div>

            <ul class="price-features list-unstyled">
              <li>Подбор и выбор ниши</li>
              <li>Товарная матрица неограничена</li>
              <li>Регистрация домена и хостинга</li>
              <li>Адаптивный многостраничный сайт</li>
              <li>База товаров с поставщиками</li>
              <li>Автообновление товаров</li>
              <li>Аналитика цен конкурентов (программа)</li>
              <li>Подключение CRM</li>
              <li>Колл-центр</li>
              <li>Обучение</li>
              <li>Логистика (отправка/доставка товара)</li>
              <li>Рекламные компании Яндекс Директ</li>
              <li>Рекламные компании Google Adwords</li>
              <li>Группы VK (3.000 чел + 10 постов)</li>
              <li>Бизнес аккаунт в Instagram (10.000 чел + 10 постов)</li>
              <li>Бизнес-план</li>
              <li>Профессиональное СЕО-продвижение 2 месяц</li>
              <li>Профессиональный запуск VK (3.000 живых подписчиков + 10 постов)</li>
              <li>Профессиональный бизнес аккаунт в instagram (10.000 живых подписчиков + 10 постов)</li>
            </ul>

            <a href="#formmm" class="btn btn-outline-dark btn-circled px-4 py-3">Выбрать <i class="licon-right-arrow"></i></a>
          </div>
        </div>
      </div>
    </div>
  </section>

  {{--<section class="ftco-section section">--}}
    {{--<div class="container">--}}
      {{--<div class="row justify-content-center mb-4">--}}
        {{--<div class="col-md-8 col-lg-6 text-center">--}}
          {{--<div class="section-heading">--}}
            {{--<h2 class="section-title">--}}
              {{--Подробная информация--}}
            {{--</h2>--}}
          {{--</div>--}}
        {{--</div>--}}
      {{--</div>--}}
      {{--<h3 class="mb-3"></h3>--}}
      {{--<div class="table-responsive">--}}
        {{--<table class="table">--}}
          {{--<thead>--}}
          {{--<tr>--}}
            {{--<th scope="col">Функции сервиса</th>--}}
            {{--<th scope="col" class="text-center">Минимальный</th>--}}
            {{--<th scope="col" class="text-center">Базовый</th>--}}
            {{--<th scope="col" class="text-center">Бизнес</th>--}}
          {{--</tr>--}}
          {{--</thead>--}}
          {{--<tbody>--}}
          {{--<tr>--}}
            {{--<td scope="row">Стоимость в месяц</td>--}}
            {{--<td class="text-center"><span class="h4">3,500руб</span></td>--}}
            {{--<td class="text-center"><span class="h4">9,600руб</span></td>--}}
            {{--<td class="text-center"><span class="h4">17,000руб</span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Сайт на CMS opencart <span class="tooltip-icon" data-toggle="tooltip" data-placement="top"--}}
                                                       {{--title="Мощная и функциональная CMS. Все необходимые функции для интернет-магазина из коробки, тысячи как платных, так и бесплатных расширений и тем.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Адаптивный дизайн <span class="tooltip-icon" data-toggle="tooltip" data-placement="top"--}}
                                                    {{--title="Ваш сайт будет одинаково отлично выглядеть как на больших экранах, так и на маленьких.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Яндекс Маркет и другие площадки <span class="tooltip-icon" data-toggle="tooltip"--}}
                                                                  {{--data-placement="top"--}}
                                                                  {{--title="Возможность выгрузки товаров в формат CSV и YML, который требуют такие сервисы как Яндекс.Маркет, Яндекс.Директ, Google ads и прочие">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Неограниченное число товаров <span class="tooltip-icon" data-toggle="tooltip"--}}
                                                               {{--data-placement="top"--}}
                                                               {{--title="Мы не ограничиваем вас ни в чем. Вы можете загрузить к себе на сайт из нашей базы данных столько товаров, сколько захотите.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Доступ к базе товаров gipo.ru <span class="tooltip-icon" data-toggle="tooltip"--}}
                                                                {{--data-placement="top"--}}
                                                                {{--title="Мы предоставляем вам доступ к нашей базе товаров, которая насчитывает более 50,000 наименований. Все товары представлены для вас по конкурентным оптовым ценам. Вы сами решаете, какой будет ваша розничная цена.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Модуль синхронизации товаров <span class="tooltip-icon" data-toggle="tooltip"--}}
                                                               {{--data-placement="top"--}}
                                                               {{--title="Товары, их остатки и цены на них всегда будут актуальны. Все данные обновляются ежедневно автоматически.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Аналитика цен конкурентов <span class="tooltip-icon" data-toggle="tooltip"--}}
                                                            {{--data-placement="top"--}}
                                                            {{--title="Мы предоставляем вам доступ к мощнейшему инструменту аналитики цен товаров. Вы сразу видите цены конкурентов на все товары, и можете разрабатывать собственные стратегии управления ценами.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Seo-оптимизация товаров <span class="tooltip-icon" data-toggle="tooltip"--}}
                                                          {{--data-placement="top"--}}
                                                          {{--title="Вы можете гибко настроить то, как будет формироваться описание товара и какие ключевые фразы будут использоваться.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Call-центр <span class="tooltip-icon" data-toggle="tooltip" data-placement="top"--}}
                                             {{--title="Все приходящие вам на сайт заказы будет обрабатывать наш call-центр. Менеджер в кратчайшие сроки свяжется с покупателем, и, представившись сотрудником вашего магазина, обработает заказ.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-close text-danger h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Служба доставки <span class="tooltip-icon" data-toggle="tooltip" data-placement="top"--}}
                                                  {{--title="Наша служба доставки доставит товар вашему покупателю до двери в течение 2-х дней, либо до пункта выдачи Boxberry.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-close text-danger h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Прием платежей наличными и картой <span class="tooltip-icon" data-toggle="tooltip"--}}
                                                                    {{--data-placement="top"--}}
                                                                    {{--title="Клиент может заплатить нашему курьеру за товар как наличными, так и картой.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-close text-danger h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Не нужно вносить предоплату за товар <span class="tooltip-icon" data-toggle="tooltip"--}}
                                                                       {{--data-placement="top"--}}
                                                                       {{--title="Gipo самостоятельно везет товар покупателю и принимает оплату, так что от вас не потребуется вносить залог за товар.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-close text-danger h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Дизайн на выбор в подарок <span class="tooltip-icon" data-toggle="tooltip"--}}
                                                            {{--data-placement="top"--}}
                                                            {{--title="Если вы оплатите тариф на год, мы сами купим и установим вам шаблон, который вы выберете.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-close text-danger h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Услуги разработчика 10 часов в неделю <span class="tooltip-icon" data-toggle="tooltip"--}}
                                                                        {{--data-placement="top"--}}
                                                                        {{--title="На 10 часов в неделю мы предоставляем вам наших разработчиков. Вам не придется обращаться к фрилансерам.">?</span>--}}
            {{--</td>--}}
            {{--<td class="text-center"><span class="ion-md-close text-danger h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-close text-danger h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<th scope="col" colspan="4">Функции интернет-магазина</th>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Неограниченое число товаров и категорий</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Автоматическая синхронизация цен и остатков</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Синхронизация с 1C</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Личный кабинет покупателя</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Подарочные сертификаты и промокоды</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Бонусные программы для покупателей</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Отзывы о товарах и рейтинги</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Автоматические фильтры товаров</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Мощный модуль SEO-оптимизации</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Функциональная админ-панель</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Разделение пользователей по ролям</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Отчеты о продажах и покупателях</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Резервное копирование и восстановление</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--<tr>--}}
            {{--<td scope="row">Огромный выбор как платных, так и бесплатных модулей</td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
            {{--<td class="text-center"><span class="ion-md-checkmark text-success h4"></span></td>--}}
          {{--</tr>--}}
          {{--</tbody>--}}
        {{--</table>--}}
      {{--</div>--}}
    {{--</div>--}}
  {{--</section>--}}

  <section class="ftco-services ftco-no-pt" id="formmm">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-2">
        <div class="ftco-animate fadeInUp ftco-animated" style="width: 100%;">
          <h2 class="mb-2 text-center section-title px-4">Запишитесь на бесплатную косультацию<br>и получите премиум
            шаблон магазина в подарок</h2>
        </div>
      </div>
      <div class="row justify-content-center">
        <form action="" id="ajax_form" method="POST" class="col-12 col-md-8">
          {{ csrf_field() }}
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Ваше имя" name="name" required>
          </div>
          <div class="form-group">
            <input type="email" class="form-control" placeholder="Ваш E-mail" name="email" required>
          </div>
          <div class="input-group mb-2">
            <div class="input-group-prepend">
              <div class="input-group-text">+7</div>
            </div>
            <input type="text" class="form-control" placeholder="Ваш телефон" name="phone" maxlength="10" minlength="10"
                   required>
          </div>
          <div class="form-group">
            <textarea name="comment" cols="30" rows="7" class="form-control" placeholder="Комментарий"></textarea>
          </div>
          <div class="form-group">
            <input type="submit" value="Отправить" id="btn_add_consultation_request"
                   class="btn btn-primary py-3 px-5 float-right">
          </div>
        </form>
      </div>
    </div>
  </section>
@endsection

@push('scripts')
  <script>
      $(document).ready(function () {
          $("#ajax_form").submit(
              function (e) {
                  e.preventDefault();
                  sendAjaxForm('ajax_form', '{{ route('createConsultationRequest') }}');
                  return false;
              }
          );
      });

      function sendAjaxForm(ajax_form, url) {
          $.ajax({
              url: url, //url страницы (action_ajax_form.php)
              type: "POST", //метод отправки
              dataType: "html", //формат данных
              data: $("#" + ajax_form).serialize(),  // Сеарилизуем объект
              beforeSend: function () {
                  $('#btn_add_consultation_request').prop('disabled', true).val('Отправка...');
              },
              success: function (response) { //Данные отправлены успешно
                  // result = $.parseJSON(response);
                  $('#ajax_form').html('<h3 class="text-center" style="color: #0033c7">Спасибо! Наш менеджер свяжется с вами в кратчайшие сроки.</h3>');
              },
              error: function (response) { // Данные не отправлены
                  $('#btn_add_consultation_request').prop('disabled', false).val('Ошибка отправки формы');
              }
          });
      }
  </script>
@endpush