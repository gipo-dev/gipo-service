<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

//основые страницы сайта
Route::group(['prefix' => '/', 'middleware' => ['referral']], function(){

    Route::get('/', function () {
        return view('landing');
    });
    Route::get('/dropshipping', function () {
        return view('landingDropshipping');
    });
    Route::get('/about', function () {
        return view('about');
    });
    Route::get('/tariff', function () {
        return view('tariff');
    });


    Route::group(['prefix' => 'blog'], function () {
        Route::get('/', 'WebsiteController@articles')->name('articles');
        Route::match(['get', 'post'], '/{id}', 'WebsiteController@article')->name('article');
    });

});

Route::get('/telegram/'.\App\Telegram\TelegramBot::$token, 'TelegramBotController@index')->name('telegram.api');


Route::get('/mail', 'MailController@send')->name('mail.send');


Route::post('/create-consultation-request', 'WebsiteController@createConsultationRequest')->name('createConsultationRequest');

Auth::routes();

//личный кабинет
Route::get('/home/new', 'HomeController@new')->middleware('auth')->name('agent.new');
Route::group(['prefix' => 'home', 'middleware' => ['auth', 'userOnline']], function(){ //, 'documents'

    Route::get('/', 'HomeController@index')->name('agent.home');
    Route::get('/website/create', 'Agent\WebsiteController@createPage')->name('agent.website.create');
    Route::get('/website/create/setup', 'Agent\WebsiteController@setup')->name('agent.website.create.setup');
    Route::post('/website/create/setup/categories', 'Agent\WebsiteController@selectCategories')->name('agent.website.create.setup.selectCategories');

    Route::group(['prefix' => 'website', 'middleware' => ['isUserWebsite']], function () {
        Route::get('/{id}', 'Agent\WebsiteController@index')->name('agent.website');
        Route::get('/{id}/generate-token', 'Agent\WebsiteController@generateApiToken')->name('agent.website.generateApiToken');
        Route::post('/{id}/set-markup', 'Agent\WebsiteController@setOptMarkup')->name('agent.website.setOptMarkup');
        Route::match(['get', 'post'], '/{id}/analytics', 'Agent\ProductController@analytics')->name('agent.product.analytics');
        Route::match(['get', 'post'], '/{id}/yml', 'Agent\ProductController@yml')->name('agent.product.yml');
    });

    Route::group(['prefix' => 'wallet'], function() {
        Route::get('/', 'Agent\WalletController@index')->name('agent.wallet');
        Route::post('/', 'Agent\WalletController@createWidtdrawRequest')->name('agent.wallet.createWidtdrawRequest');
    });

    Route::group(['prefix' => 'settings'], function() {
        Route::get('/documents', 'Agent\SettingsController@documents')->name('agent.settings.documents');
        Route::post('/documents-update', 'Agent\SettingsController@documentsUpdate')->name('agent.settings.documents.update');
        Route::get('/change-juristic', 'Agent\SettingsController@changeJuristic')->name('agent.settings.changeJuristic');
    });

    Route::get('/partners', 'HomeController@partners')->name('agent.partners');
});

//админка
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'isAdmin', 'userOnline']], function(){
    Route::get('/', 'AdminController@index')->name('dashboard');
    Route::get('/orders', 'AdminController@orders')->name('orders');
    Route::get('/order/{id}', 'OrderController@index')->name('order');
    Route::post('/order/{id}', 'OrderController@addHistory')->name('order_post');
    Route::get('/order/{id}/print', 'OrderController@print')->name('order.print');
    Route::get('/websites', 'Admin\WebsiteController@list')->name('admin.websites');
    Route::match(['get', 'post'], '/website/{id}', 'Admin\WebsiteController@edit')->name('admin.website.edit');
    Route::match(['get', 'post'], '/website/{id}/categories', 'Admin\WebsiteController@categories')->name('admin.website.categories');
    Route::post('/website/{id}/prolong', 'Admin\WebsiteController@prolong')->name('admin.website.prolong');
    Route::get('/widthdraw-requests', 'AdminController@widthdrawRequests')->name('admin.widthdrawRequests');
    Route::get('/widthdraw-request/{id}', 'AdminController@widthdrawRequest')->name('admin.widthdrawRequest');
    Route::post('/widthdraw-request/{id}', 'AdminController@updateWidthdrawRequestStatus')->name('admin.updateWidthdrawRequestStatus');
    Route::get('/git', 'AdminController@git')->name('admin.git');
    Route::post('/git', 'AdminController@git')->name('admin.git.post');
    Route::get('/users', 'Admin\UserController@list')->name('admin.users');
    Route::get('/user/{id}', 'Admin\UserController@index')->name('admin.user');
    Route::get('/categories', 'Admin\CategoryController@list')->name('admin.categories');
    Route::match(['get', 'post'], '/category/{id?}', 'Admin\CategoryController@edit')->name('admin.category.edit');
    Route::get('/category/{id}/delete', 'Admin\CategoryController@delete')->name('admin.category.delete');
    Route::get('/consultations', 'Admin\ConsultationController@list')->name('admin.consultations');
    Route::match(['get', 'post'], '/consultation/{id}', 'Admin\ConsultationController@index')->name('admin.consultation');

    Route::group(['prefix' => 'product'], function () {
        Route::match(['get', 'post'], '/list', 'Admin\ProductController@list')->name('admin.product.list');
    });
    Route::group(['prefix' => 'pricelist'], function () {
        Route::get('/lapsi', 'Admin\PricelistController@lapsi')->name('admin.pricelist.lapsi');
        Route::group(['prefix' => 'yml'], function () {
            Route::match(['get', 'post'], '/', 'Admin\PricelistController@index')->name('admin.pricelist.index');
            Route::match(['get', 'post'], '/categories/{id}', 'Admin\PricelistController@categories')->name('admin.pricelist.categories');
            Route::match(['get', 'post'], '/products/{id}', 'Admin\PricelistController@uploadProducts')->name('admin.pricelist.products');
        });
        Route::get('/upload-updates', 'Admin\PricelistController@uploadUpdates')->name('admin.pricelist.uploadUpdates');
    });
    Route::group(['prefix' => 'articles'], function () {
        Route::get('/', 'Admin\BlogController@list')->name('admin.articles');
        Route::match(['get', 'post'], '/{id}', 'Admin\BlogController@edit')->name('admin.articles.edit');
    });

    Route::get('/ajax/order', 'OrderController@ajaxGetList')->name('ajax_order_list');
    Route::post('/ajax/getMonthsOrders', 'HomeController@getMonthsOrders')->name('dashboard.getMonthsOrders');
});