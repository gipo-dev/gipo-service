<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['apiKeyValidator']], function () {

    Route::post('check', 'ApiController@checkKey');
    Route::post('get-category-list', 'ApiController@getAllCategoriesList');
    Route::post('get-shop-categories', 'ApiController@getWebsiteCategories');
    Route::post('categories', 'ApiController@getWebsiteCategories');
//    Route::post('set-categories', 'ApiController@setWebsiteCategories');
    Route::post('products', 'ApiController@getWebsiteProductsList');
    Route::post('product/{id}', 'ApiController@getProduct')->where('id', '[0-9]+');
    Route::post('place-order', 'ApiController@placeOrder');

});
