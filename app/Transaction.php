<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function setTransactionDataAttribute($value)
    {
        $this->attributes['transaction_data'] = encrypt(json_encode($value));;
    }

    public function getTransactionDataAttribute()
    {
        $data = json_decode(decrypt($this->attributes['transaction_data']));
        $data->transaction = json_decode($data->transaction);
        return $data;
    }

    public function type() {
        return $this->belongsTo('App\TransactionType', 'transaction_type_id');
    }
}

class TransactionType extends Model
{
    protected $table = 'transaction_type';
}