<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WebsiteAdministratorMail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $order;
    public $website;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $website, $user)
    {
        $this->order = $order;
        $this->user = $user;
        $this->website = $website;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.to_website_admin')
            ->with([
                'user' => $this->user,
                'order' => $this->order,
                'website' => $this->website,
                ])
            ->subject('Оповещение о заказе');
    }
}
