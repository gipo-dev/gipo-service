<?php

namespace App;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Order extends Model
{
    protected $table = 'orders';

    public $data;
    public $history;

    public function getOrderDataAttribute() {
        return json_decode($this->attributes['order_data']);
    }

    public function getProductsListAttribute() {
        $_products = OrderProduct::order($this->id)->get();
        return Product::whereIn('id', array_pluck($_products, 'product_id'))->get();
    }
//
//    public function setProductsAttribute($data) {
//        $this->attributes['products'] = $data;
//    }

    public function history() {
        return DB::table('order_history as oh')->join('order_status as os', 'oh.status_id', '=', 'os.id')->select('oh.*', 'os.name')->where('oh.order_id', '=', $this->id)->orderBy('id', 'desc')->get();
    }

    public static function getStatusList() {
        return DB::table('order_status')->get();
    }

    public function addHistory($request) {
        DB::table('order_history')->insert([
            'order_id' => $this->id,
            'comment' => Input::get('comment', ''),
            'status_id' => Input::get('order_status_id'),
            'buyer_notified' => Input::get('notify', 0),
            'user_id' => Auth::user()->id,
        ]);
        $order = Order::find($this->id);
        $order->status_id = $request->order_status_id;
        $order->save();
    }

    public function addProducts($products) {
        DB::table('order_products')->insert($products);
    }

    public function products() {
        DB::table('order_products')->where('order_id', $this->id)->get();
    }

    public function calculateAgentRewards() {
        $this->products = $this->order_data->products;
        $total_agent_reward = 0;
//        dd($this->products);

        foreach ($this->products as $k => $product) {
            $_local = Product::find($product->gipu_id);
            if($_local == null)
                continue;
            $user_reward = $product->total - ($_local->agent_price * $product->quantity);
            $total_agent_reward += $user_reward;
            $product->user_reward = $user_reward;
            $product->agent_price = $_local->agent_price;
        }
        $this->total_agent_reward = $total_agent_reward;
        return $this;
    }

    public function website() {
        return $this->belongsTo(UserWebsite::class);
    }

}

class OrderProduct extends Model {

    protected $table = 'order_products';

    public function scopeOrder($query, $order_id) {
        return $query->where('order_id', $order_id);
    }

}