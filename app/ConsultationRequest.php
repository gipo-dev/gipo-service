<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsultationRequest extends Model
{
    protected $table = 'consultation_requests';

    public function getStatusNameAttribute() {
        return $this->belongsTo('App\ConsultationRequestStatus', 'status_id')->first()->name;
    }
}

class ConsultationRequestStatus extends Model
{
    protected $table = 'consultation_request_status';

    public $timestamps = false;
}

