<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WidthdrawRequest extends Model
{
    private $_user = null;

    public function getSumAttribute() {
        return decrypt($this->attributes['sum']);
    }

    public function getStateAttribute() {
        return decrypt($this->attributes['state']);
    }

    public function setStateAttribute($val) {
        $this->attributes['state'] = encrypt($val);
    }

    public function scopeWhereUser($query, $id)
    {
        return $query->where('user_id', $id);
    }

    public function user() {
        if($this->_user == null)
            $this->_user = User::find($this->user_id);
        return $this->_user;
    }

    public function status() {
        return $this->belongsTo('App\WidthdrawRequestStatus', 'status_id');
    }
}

class WidthdrawRequestStatus extends Model
{
    protected $table = 'widthdraw_request_state';
}