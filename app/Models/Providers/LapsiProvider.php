<?php

namespace App\Models\Providers;

use App\PriceList;
use Illuminate\Database\Eloquent\Model;

class LapsiProvider extends ProductProvider
{
    public $id = 113;
    public $name = 'Lapsi';

    private $pricelist;

    public function getPriceListAttribute() {
        if($this->pricelist == null) {
            $this->pricelist = new PriceList();
        }
        return $this->pricelist->products();
    }

}
