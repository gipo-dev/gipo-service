<?php

namespace App\Models\Providers;

use App\PriceList;
use App\Product;
use Illuminate\Database\Eloquent\Model;

abstract class ProductProvider extends Model
{
    public $id;
    public $name;

    private $pricelist;

    public function getPriceListAttribute() {
        if($this->pricelist == null) {
            $this->pricelist = new PriceList();
        }
        return $this->pricelist->products();
    }

}
