<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use App\ArticleComment;
use App\ArticleTag;
use App\ConsultationRequest;
use App\Filters\ArticlesFilter;
use App\Telegram\TelegramBot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;

class WebsiteController extends Controller
{
    public function createConsultationRequest(Request $request)
    {
        $consultationRequest = new ConsultationRequest();
        $consultationRequest->name = $request->name;
        $consultationRequest->email = $request->email;
        $consultationRequest->phone = $request->phone;
        $consultationRequest->comment = isset($request->comment) ? $request->comment : '';
        $consultationRequest->status_id = 1;
        $consultationRequest->save();
        TelegramBot::sendMessage('Новая заявка с лендинга. ' . $request->name . ' +7' . $request->phone . ' '. $consultationRequest->email . ' Комментарий: ' . $consultationRequest->comment);
        return ('ok');
    }

    public function articles(Request $request)
    {
        if (!isset($request->tag)) {
            $articles = Article::where('id', '>', 0);
            $articles = (new ArticlesFilter($articles, $request))->apply()->with('category')->with('comments')->paginate(6);
        } else {
            $articles = ArticleTag::find($request->tag)->articles()->with('comments')->paginate(6);
        }
        $categories = ArticleCategory::with('articles')->get();
        $tags = ArticleTag::all();
        $populars = Article::orderBy('popularity', 'desc')->with('comments')->limit(4)->get();

        return view('articles', ['articles' => $articles, 'categories' => $categories,
            'tags' => $tags, 'populars' => $populars]);
    }

    public function article(Request $request, $id)
    {
        if ($request->isMethod('POST')) {
            $comment = new ArticleComment();
            $comment->article_id = $id;
            if ($request->reply_to && $request->reply_to != '')
                $comment->reply_to = $request->reply_to;
            $comment->text = $request->message;
            if(Auth::user()) {
                $comment->author_name = Auth::user()->first_name.' '.Auth::user()->second_name;
                if(Auth::user()->isAdmin())
                    $comment->author_admin = 1;
            } else
                $comment->author_name = $request->name;
            $comment->save();
            return redirect(route('article', $id));
        }

        $article = Article::with('tags')->with('comments')->find($id);
        $article->popularity++;
        $article->save();
        $categories = ArticleCategory::with('articles')->get();
        $tags = ArticleTag::all();
        $populars = Article::with('comments')->orderBy('popularity', 'desc')->limit(4)->get();
        $comments_count = $article->comments->count();
        $article->comments = $article->comments->groupBy('reply_to');
//        dd($article->comments);

        return view('article', ['article' => $article, 'comments_count' => $comments_count, 'categories' => $categories,
            'tags' => $tags, 'populars' => $populars]);
    }
}
