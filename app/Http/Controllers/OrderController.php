<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\User;
use App\UserWallet;
use App\UserWebsite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    public function index($id) {
        if(!isset($id)) {
            return redirect('/admin/orders');
        }

        $order = Order::find($id);
        $order->order_history = $order->history();;
        $order->calculateAgentRewards();
        $status_list = Order::getStatusList();
        foreach ($status_list as $status) {
            if($status->id == $order->status_id)
                $order->status_name = $status->name;
        }
        $website = UserWebsite::find($order->website_id);
        $website->user = User::find($website->user_id);

        return view('admin.order', [ 'order' => $order, 'status_list' => $status_list, 'website' => $website ]);
    }

    public function ajaxGetList() {
        $orders = Order::join('order_status as os', 'orders.status_id', '=', 'os.id')
            ->select('orders.*', 'os.name as status_name')
            ->orderBy('orders.status_id', 'asc')
            ->orderBy('orders.id', 'desc')->paginate(20);
        return $orders;
    }

    public function addHistory($id) {
        $request = \request();
        $order = Order::findOrFail($id);
        if($request->order_status_id == "4" && decrypt($order->state) == 0) {
            $website = UserWebsite::find($order->website_id);
            $website->user = User::find($website->user_id);
            $order->state = encrypt(1);
            $order->save();
            $order->calculateAgentRewards();
//            dd($order->total_agent_reward);
            $website->user->wallet()->add($order->total_agent_reward, $order);
        }
        $order->addHistory($request, $id);
        return redirect()->action('OrderController@index', ['id' => $id]);
    }

    public function print($id) {
        $order = Order::find($id);
        return view('admin.orderPrint', ['order' => $order]);
    }
}
