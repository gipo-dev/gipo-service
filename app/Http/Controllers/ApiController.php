<?php

namespace App\Http\Controllers;

use App\Category;
use App\Order;
use App\Product;
use App\Telegram\TelegramBot;
use App\UserWebsite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\WebsiteAdministratorMail;

define('OPENCART_MODULE_VERSION', '0.8.2');
\Debugbar::disable();

class ApiController extends Controller
{

    public function checkKey(Request $request)
    {
        if ($request->module_version != OPENCART_MODULE_VERSION) {
            return self::errorMessage('Версия вашего модуля устарела. Администратор gipo поможет вам с его обновлением.');
        }
        return response(json_encode([
            'code' => 1
        ]), 200);
//        return json_encode(['code' => 1]);
    }

    public function getAllCategoriesList()
    {
        $categories = new Category();
        $categories = $categories->makeTree();
        return json_encode($categories);
    }

    public function getWebsiteCategories() {
        $_categories = \request()->website->categories();
        return $_categories;
    }

    public function setWebsiteCategories()
    {
        \request()->website->setCategories(\request()->categories);
    }

    public static function errorMessage($message = '')
    {
        return response(json_encode([
            'code' => 0,
            'message' => [
                'type' => 'error',
                'text' => $message,
            ]
        ]), 200);
    }

    public function getWebsiteProductsList() {
        $products = \request()->website->getProductsList();
        $_products = [];
        foreach ($products as $product) {
            $_products[] = $product->makeValues();
        }
        return json_encode($_products);
    }

    public function getProduct(Request $request, $id) {
        $product = Product::findOrFail($id);
        $category = Category::find($product->category_id);
        $_product = $product->makeAllValues();
        $_product['property_values'] = $product->properties();
//        dd($_product);
        $_product['properties'] = $category->properties();
        return $_product;
    }

    public function placeOrder() {
        $request = \request();
        $order = new Order();
        $order->website_id = $request->website->id;
        $order->website_order_local_id = $request->order_id;
        $order->order_data = json_encode($request->order);
        $order->status_id = 1;
        $order->save();
        $products = [];
        foreach ($request->order['products'] as $product) {
            $products[] = [
                'order_id' => $order->id,
                'count' => $product['quantity'],
                'product_id' => $product['gipu_id'],
            ];
        }
        $order->addProducts($products);
        
        
        Mail::to($order->website->user->email)
            ->send(new WebsiteAdministratorMail($order->order_data, $order->website, $order->website->user));
        TelegramBot::sendMessage('Поступил новый заказ');

        return 'ok';
    }

}