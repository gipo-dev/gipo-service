<?php

namespace App\Http\Controllers\Admin;

use App\Ccategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function list() {
        $categories = Ccategory::makeTree(Ccategory::get());
        return view('admin.categories', [ 'categories' => $categories ]);
    }

    public function edit(Request $request, $id = null) {
        $category = Ccategory::find($id);
        if($request->isMethod('post')) {
            if($category == null)
                $category = new Ccategory();
            $category->name = $request->name;
            $category->alias = $request->alias;
            $category->parent_id = $request->parent_id;
            $category->active = $request->active == 'on' ? 1 : 0;
            $category->save();
            return redirect(route('admin.categories'));
        }
        $categories = Ccategory::get();
        return view('admin.category', [ 'category' => $category, 'categories' => $categories ]);
    }

    public function delete($id) {
        $category = Ccategory::find($id);
        $category->delete();
        return redirect(route('admin.categories'));
    }
}
