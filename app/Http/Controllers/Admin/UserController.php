<?php

namespace App\Http\Controllers\Admin;

use App\Transaction;
use App\User;
use App\WidthdrawRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function list() {
        $users = User::paginate(20);
        return view('admin.users', ['users' => $users]);
    }

    public function index($id) {
        $user = User::find($id);
        $transactions = Transaction::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(10, ['*'], 'transactions');
        $withdrawRequests = WidthdrawRequest::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(5, ['*'], 'withdraw-requests');
        return view('admin.user', ['user' => $user, 'transactions' => $transactions, 'withdrawRequests' => $withdrawRequests]);
    }
}
