<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\ArticleCategory;
use App\ArticleTag;
use App\File;
use App\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    public function list()
    {
        $articles = Article::paginate(20);

        return view('admin.articles', ['articles' => $articles]);
    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('POST')) {
            if ($request->froala == 'true') {
                $completePath = File::find(Image::upload($request->file, '/articles'))->path;
                return stripslashes(response()->json(['link' => $completePath])->content());
            } else {
                if ($id == 'new') {
                    $article = new Article();
                    $article->author_id = Auth::user()->id;
                } else
                    $article = Article::find($id);
                $article->title = $request->title;
                $article->description = $request->description;
                $article->text = $request->text;
                $article->category_id = $request->category_id;
                if ($request->image)
                    $article->image = File::find(Image::upload($request->image))->path;
                $article->tags()->detach();
                $article->tags()->attach($request->tags);
                $article->save();
                return redirect(route('admin.articles.edit', $article->id));
            }
        }

        if ($id == 'new')
            $article = new Article();
        else
            $article = Article::with('tags')->find($id);

        $categories = ArticleCategory::all();
        $tags = ArticleTag::all();
        $article->tags = $article->tags->keyBy('id');
        return view('admin.article', ['article' => $article, 'categories' => $categories, 'tags' => $tags]);
    }
}
