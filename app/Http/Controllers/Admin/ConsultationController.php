<?php

namespace App\Http\Controllers\Admin;

use App\ConsultationRequest;
use App\ConsultationRequestStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConsultationController extends Controller
{
    public function index(Request $request, $id) {
        $cRequest = ConsultationRequest::findOrFail($id);
        if($request->isMethod('POST')) {
            $cRequest->status_id = $request->status_id;
            $cRequest->save();
            return redirect(route('admin.consultation', ['id' => $id]));
        }
        $cRequest = ConsultationRequest::findOrFail($id);
        $settings['status'] = ConsultationRequestStatus::get();
        return view('admin.consultation', ['settings' => $settings, 'request' => $cRequest]);
    }

    public function list(Request $request) {
        $consultationRequests = ConsultationRequest::orderBy('status_id')->orderBy('id', 'desc')->paginate(20);
        return view('admin.consultations', ['consultationRequests' => $consultationRequests]);
    }
}
