<?php

namespace App\Http\Controllers\Admin;

use App\Filters\ProductsFilter;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function list(Request $request) {
        if($request->isMethod('POST')) {
            $product = Product::find($request->product_id);
            $product->enabled = $request->enabled;
            $product->save();
            return;
        }

        $products = Product::where('id', '>', 0);
        $products = (new ProductsFilter($products, $request))->apply()->paginate(20);
        return view('admin.products', ['products' => $products]);
    }
}
