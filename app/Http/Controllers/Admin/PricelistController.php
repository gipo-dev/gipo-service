<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\File;
use App\Models\Providers\LapsiProvider;
use App\PriceListCategoryAlias;
use App\PriceListProduct;
use App\PriceListProductNew;
use App\PriceListProductOwner;
use App\PriceListProvider;
use App\Product;
use App\ProductImage;
use App\ProductProperties;
use App\ProductPropertyValue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Orchestra\Parser\Xml\Facade as XmlParser;
use phpDocumentor\Reflection\DocBlock\Tags\Property;

class PricelistController extends Controller
{
    public function lapsi()
    {
        $pr = new LapsiProvider();
        dd($pr->price_list);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://lapsi.ru/az/o/opt_xml_full.php');
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
//        $err = curl_error($ch);  //if you need
        curl_close($ch);
        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        dd($array);


//        $this - > response['response'] = json_decode($output);
//        $client = new \GuzzleHttp\Client();
//        $request = $client->request('GET', 'http://laravel.su');
//        dd($output);
        return view('admin.pricelist');
    }

    public function index(Request $request)
    {
        $provider = PriceListProvider::find($request->provider_id);
        $step = 0;
        if ($request->isMethod('POST')) {

            //ajax requests
            if ($request->type == 'providers') {
                $providers = PriceListProvider::get();
                return json_encode($providers);
            }

            //file upload
            $step = 1;
            Storage::disk('public')->deleteDirectory('/uploads/pricelists');
            $pricelist = File::find(File::upload($request->pricelist_file, '/pricelists'))->path;
            $xml = Storage::disk('public')->get(substr($pricelist, 8, strlen($pricelist)));

            $pricelist = XmlParser::extract($xml);
            $pricelist = $pricelist->parse([
                'categories' => ['uses' => 'shop.categories.category[::id>id,@>name]'],
                'products' => ['uses' => 'shop.offers.offer[::id>id,price,picture(@=@),name,param(::name=@)>property,article,categoryId,description]'],
            ]);
            //алиасы категорий
            $local_categories = [];
            foreach (PriceListCategoryAlias::where('provider_id', $request->provider_id)->get() as $l_cat) {
                $local_categories[$l_cat['price_category_id']] = $l_cat;
            }
            //новые категории
            $new_local_categories = [];
            if ($pricelist['categories']) {
                foreach ($pricelist['categories'] as $c) {
                    if (!isset($local_categories[$c['id']])) {
                        $c_name = trim($c['name']);
                        // совпадение локальной категории
                        $local_like_category = Category::select('id')->where('name', 'like', '%' . $c_name . '%')->first();
                        $new_local_categories[] = [
                            'provider_id' => $request->provider_id,
                            'price_category_id' => $c['id'],
                            'price_category_name' => $c_name,
                            'local_category_id' => $local_like_category ? $local_like_category->id : null,
                        ];
                    }
                }
            }
            if (count($new_local_categories) > 0) {
                PriceListCategoryAlias::insert($new_local_categories);
                $local_categories = [];
                foreach (PriceListCategoryAlias::where('provider_id', $request->provider_id)->get() as $l_cat) {
                    $local_categories[$l_cat['price_category_id']] = $l_cat;
                }
            }
            //добавляем в очередь новые товары
            if ($provider->padding == 1) {
                foreach ($pricelist['products'] as $k => $p) {
                    $pricelist['products'][$k]['article'] = $p['id'];
                }
            }
            foreach (array_chunk($pricelist['products'], 50) as $chunk) {
                $new_products = [];
                $to_find = array_pluck($chunk, 'article');
                $price_articles = [];
                foreach ($chunk as $p) {
                    $price_articles[$p['article']] = $p;
                }
                // dd($chunk);

                $finded_products = DB::connection('pricelists')
                    ->table('product_owners')
                    ->select('product_owners.*', 'product.gipo_index', 'price_product_new.id as is_new_exist')
                    ->where('product_owners.provider_id', $request->provider_id)
                    ->whereIn('product_owners.inner_index', $to_find)
                    ->join('product', 'product_owners.product_id', '=', 'product.id')
                    ->leftJoin('price_product_new', 'price_product_new.inner_index', '=', 'product_owners.inner_index')
                    ->get();
                // dd($finded_products);
                foreach ($finded_products as $product) {
                    if ($product->gipo_index == null && $product->is_new_exist == null) {
                        $new_products[] = [
                            'inner_index' => $product->inner_index,
                            'value' => json_encode($price_articles[strtolower($product->inner_index)]),
                            'provider_id' => $request->provider_id,
                        ];
                    }
                }
                PriceListProductNew::insert($new_products);
            }
        }
        return view('admin.pricelist', ['step' => $step]);
    }

    public function categories(Request $request, $id)
    {
        if ($request->isMethod('POST')) {
            if ($request->type == 'getCategories') {
                $categories = DB::select(DB::raw("SELECT `pca`.*, `pc`.`name`
                as `local_category_name`, 
                (SELECT `name` FROM `products`.`category` WHERE `id` = `pc`.`parent_id`) as `local_parent_name` 
                FROM `upload_base`.`price_category_alias` as `pca` LEFT JOIN `products`.`category` as `pc` ON `pc`.`id` = `pca`.`local_category_id` 
                WHERE `provider_id` = " . $request->provider_id . " ORDER BY `pca`.`local_category_id` ASC"));
                return json_encode($categories);
            } else if ($request->type == 'findCategories') {
                if (isset($request->val['find']))
                    $request->val = $request->val['find'];
                $categories = DB::select("SELECT `id`, `name`, `parent_id`,
                    (SELECT `name` FROM `products`.`category` WHERE `id` = `pc`.`parent_id`) as `parent_name`
                    FROM `products`.`category` as `pc` WHERE `name` LIKE '%$request->val%' LIMIT 5");
                return json_encode($categories);
            } else if ($request->type == 'setCategoryAlias') {
                $category = PriceListCategoryAlias::where('price_category_id', $request->price_category_id)
                    ->where('provider_id', $request->provider_id)
                    ->first();
                $category->local_category_id = $request->local_category_id;
                $category->save();
                return json_encode([
                    'resp' => 'ok',
                ]);
            }
        } else {
            return view('admin.pricelist_categories', []);
        }
    }

    public function uploadProducts(Request $request, $id)
    {
        if ($request->isMethod('POST')) {

        }

        $products = DB::connection('pricelists')->table('price_product_new')->where('provider_id', $id)->get();
        $categories = PriceListCategoryAlias::where('provider_id', $id)->whereNotNull('local_category_id')->get();
        $_temp_categories = [];
        foreach ($categories as $c) {
            $_temp_categories[$c->price_category_id] = $c->local_category_id;
        }
        $categories = $_temp_categories;
        unset($_temp_categories);

        $properties = DB::connection('main')
            ->table('properties')
            ->select('id', 'name', 'category_id')
            ->whereIn('category_id', $categories)
            ->get();
        $_temp_properties = [];
        foreach ($properties as $prop) {
            $_temp_properties[$prop->category_id][$prop->name] = $prop->id;
        }
        $properties = $_temp_properties;
        unset($_temp_properties);

        foreach ($products as $p) {
            $_product = json_decode($p->value);
            if (!isset($categories[$_product->categoryId]))
                continue;

            //товар
            $now = Carbon::now();
            $product = new Product();
            $product->name = trim($_product->name);
            $product->status_id = 1;
            $product->enabled = 1;
            $product->category_id = $categories[$_product->categoryId];
            $product->opt_price = 0;
            $product->agent_price = $_product->price;
            $product->recommended_price = $_product->price;
            $product->count = 0;
            $product->in_transit = 0;
            $product->alias = self::transliterate($_product->name) . '_' . $now->timestamp;
            $product->yandex_id = 0;
            $product->yandex_minimal_price = 0;
            $product->tag = 'yml' . $now;
            $product->created_at = $now;
            $product->save();

            //изображения товара
            if (isset($_product->picture)) {
                $images = [];
                foreach ($_product->picture as $img) {
                    $images[] = [
                        'product_id' => $product->id,
                        'image' => trim($img),
                    ];
                }
                ProductImage::insert($images);
            }

            //характеристики
            $product_properties = [];

            //если свойств нет, проверяется описание
            if($_product->description && $_product->description != '') {
                $_product->property['&nbsp;'] = trim($_product->description);
            }

            //свойтва привязываются к товару
            foreach ($_product->property as $prop_name => $prop_val) {
                $prop_name = trim($prop_name);
                $prop_val = trim($prop_val);
                if(isset($properties[$product->category_id][$prop_name])) {
                    //если свойство существует
                    $prop_id = $properties[$product->category_id][$prop_name];
                } else {
                    //если свойства не существует
                    $new_property = new ProductProperties();
                    $new_property->name = $prop_name;
                    $new_property->code = $product->category_id."_".$prop_name;
                    $new_property->active = 1;
                    $new_property->sort = 0;
                    $new_property->multi = 0;
                    $new_property->required = 0;
                    $new_property->filter = 1;
                    $new_property->property_description = '';
                    $new_property->main = 0;
                    $new_property->type = 'S';
                    $new_property->template = 'L';
                    $new_property->template_filter = 'CH';
                    $new_property->property_group_id = 2;
                    $new_property->description_field = 0;
                    $new_property->category_id = $product->category_id;
                    $new_property->redactor = 0;
                    $new_property->tag = 'yml';
                    $new_property->save();
                    $prop_id = $new_property->id;
                    $properties[$product->category_id][$prop_name] = $prop_id;
                }

                $product_properties[] = [
                    'property_id' => $prop_id,
                    'product_id' => $product->id,
                    'value' => $prop_val,
                    'description' => '',
                    'type' => 'text',
                    'value_enum' => -1,
                    'tag' => 'yml',
                    'show_filter' => 1,

                ];

            }
            ProductPropertyValue::insert($product_properties);

            //обновить индекс
            PriceListProduct::where('id', PriceListProductOwner::select('product_id')
                ->where('inner_index', $_product->article)->where('provider_id', $id)->first()->product_id)
                ->update([
                    'gipo_index' => $product->id,
                ]);

            DB::connection('pricelists')->table('price_product_new')->where('id', $p->id)->delete();
        }
//        dd($products);
        return redirect(route('admin.pricelist.index'));
    }

    private static function transliterate($string)
    {
        $str = mb_strtolower($string, 'UTF-8');

        $leter_array = array(
            'a' => 'а',
            'b' => 'б',
            'v' => 'в',
            'g' => 'г',
            'd' => 'д',
            'e' => 'е,э',
            'jo' => 'ё',
            'zh' => 'ж',
            'z' => 'з',
            'i' => 'и,i',
            'j' => 'й',
            'k' => 'к',
            'l' => 'л',
            'm' => 'м',
            'n' => 'н',
            'o' => 'о',
            'p' => 'п',
            'r' => 'р',
            's' => 'с',
            't' => 'т',
            'u' => 'у',
            'f' => 'ф',
            'kh' => 'х',
            'ts' => 'ц',
            'ch' => 'ч',
            'sh' => 'ш',
            'shch' => 'щ',
            '' => 'ъ',
            'y' => 'ы',
            '' => 'ь',
            'yu' => 'ю',
            'ya' => 'я',
        );

        foreach ($leter_array as $leter => $kyr) {
            $kyr = explode(',', $kyr); // кирилические строки разобьем в массив с разделителем запятая.
            // в строке $str мы пытаемся отыскать символы кирилицы $kyr и все найденные совпадения заменяем на ключи $leter
            $str = str_replace($kyr, $leter, $str);
        }

        // теперь необходимо учесть правильность формирования URL
        // поиск и замена по регулярному выражению.
        // перв. выраж. указываем рег выражение. втор.выраж. строка или массив строк для замены
        //   //  регуляр выражение  ()+  может повторяться 1 и более раз.,   \s пробельный символ сразу же заменяется на '-'
        // | Логическое или. либо то условие либо что указано справа от |  притом справа укажем диапазон [A-Za-z0-9-]
        //  ^ Логическое отрицание. т.е. заменяем либо пробельный символ на тире, либо любой другой символ, что не входит в указанный диапазон.
        $str = preg_replace('/(\s|[^A-Za-z0-9-])+/', '-', $str);
        $str = trim($str, '-'); // если в конце появится тире, то его удаляем.

        return $str;
    }

    public function uploadUpdates(Request $request) {
        $products = PriceListProductOwner::whereNotNull('product_id')
            ->with('product')
            ->limit(20)->get();
        dd($products);
    }
}
