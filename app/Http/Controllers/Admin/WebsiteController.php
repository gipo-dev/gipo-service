<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\User;
use App\UserWebsite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebsiteController extends Controller
{

    public function list() {
        $websites = UserWebsite::getList();
        return view('admin.websites', ['websites' => $websites]);
    }

    public function edit($id) {
        if(\request()->method() == 'POST')
            UserWebsite::where('id', '=', $id)
                ->update([ 'status_id' => \request()->status_id ]);
        $website = UserWebsite::find($id);
        $user = User::where('id', '=', $website->user_id)->first();
        $settings['status'] = UserWebsite::getStatusList();
//        dd($settings);
        return view('admin.website', ['website' => $website, 'user' => $user, 'settings' => $settings]);
    }

    public function categories(Request $request, $id) {
        $ws = UserWebsite::find($id);

        if($request->isMethod('POST')) {
            $ws->setCategories($request->categories);
            return redirect(route('admin.website.edit', $id));
        }

        $categories = new Category();
        $categories = $categories->makeTree($ws);
//        dd($categories);
        return view('admin.websiteCategories', ['website' => $ws, 'categories' => $categories]);
    }


}
