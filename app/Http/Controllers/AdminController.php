<?php

namespace App\Http\Controllers;

use App\Ccategory;
use App\Order;
use App\WidthdrawRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
    public static function getLMenu()
    {
        $news = self::howManyNews();
//        dd($news);
        return [
            [
                'name' => 'Главная',
                'link' => '/',
                'icon' => 'home',
            ],
            [
                'name' => 'Заказы',
                'link' => '/orders/',
                'icon' => 'shopping-cart',
                'count' => $news->new_orders,
            ],
            [
                'name' => 'Пользователи',
                'link' => '/users/',
                'icon' => 'user',
                'count' => $news->new_users,
            ],
            [
                'name' => 'Сайты агентов',
                'link' => '/websites/',
                'icon' => 'globe-europe',
                'count' => $news->new_websites,
            ],
            [
                'name' => 'Запросы на вывод',
                'link' => '/widthdraw-requests/',
                'icon' => 'comment-dollar',
                'count' => $news->new_widthdraw_requests,
            ],
            [
                'name' => 'Заявки с лендинга',
                'link' => '/consultations/',
                'icon' => 'smile',
                'count' => $news->new_consultations,
            ],
            [
                'name' => 'Товары',
                'link' => 'products',
                'icon' => 'box',
                'items' => [
                    [
                        'name' => 'Категории',
                        'link' => '/categories/',
                        'icon' => 'stream',
                    ],
                    [
                        'name' => 'Товары',
                        'link' => '/product/list/',
                        'icon' => 'box',
                    ],
                ]
            ],
            [
                'name' => 'Прайсы',
                'link' => 'prices',
                'icon' => 'box',
                'items' => [
//                    [
//                        'name' => 'Lapsi',
//                        'link' => '/pricelist/lapsi',
//                        'icon' => 'stream',
//                    ],
                    [
                        'name' => 'Загрузчик yml',
                        'link' => '/pricelist/yml',
                        'icon' => 'archive',
                    ],
                ]
            ],
            [
                'name' => 'Статьи',
                'link' => '/articles',
                'icon' => 'newspaper',
            ],
            [
                'name' => 'Вебмастер',
                'link' => 'webmaster',
                'icon' => 'bug',
                'items' => [
                    [
                        'name' => 'Обновления',
                        'link' => '/git/',
                        'icon' => 'code-branch',
                    ],
                ]
            ],
        ];
    }

    public static function howManyNews() {
        $count = DB::table('users')->select(DB::raw('
                        (SELECT COUNT(*) FROM `orders` WHERE `status_id` = 1) as `new_orders`,
                        (SELECT COUNT(*) FROM `users` WHERE `status_id` = 1) as `new_users`,
                        (SELECT COUNT(*) FROM `users_websites` WHERE `status_id` = 1) as `new_websites`,
                        (SELECT COUNT(*) FROM `widthdraw_requests` WHERE `status_id` = 1) as `new_widthdraw_requests`,
                        (SELECT COUNT(*) FROM `consultation_requests` WHERE `status_id` = 1) as `new_consultations`'))->first();
        return $count;
    }

    public function index() {
        return view('admin.dashboard');
    }

    public function orders() {
        $orders = Order::orderBy('id', 'desc')->paginate(20);
        return view('admin.orders')->with('orders', $orders);
    }

    public function widthdrawRequests() {
        $requests = WidthdrawRequest::select('widthdraw_requests.*', 'wrs.name as status_name')
            ->join('widthdraw_request_state as wrs', 'wrs.id', '=', 'widthdraw_requests.status_id')
            ->orderBy('id', 'desc')->paginate(20);
        return view('admin.widthdrawRequests')->with('requests', $requests);
    }

    public function widthdrawRequest($id) {
        $request = WidthdrawRequest::select('widthdraw_requests.*', 'wrs.name as status_name')
            ->join('widthdraw_request_state as wrs', 'wrs.id', '=', 'widthdraw_requests.status_id')
            ->where('widthdraw_requests.id', $id)->first();
        $settings = [
            'status' => DB::table('widthdraw_request_state')->get(),
        ];
        return view('admin.widthdrawRequest')->with(['request' => $request, 'user' => $request->user(), 'settings' => $settings]);
    }

    public function updateWidthdrawRequestStatus(Request $request, $id) {
        $req = WidthdrawRequest::find($id);
        if($request->status_id == 2 && $req->state == 0) {
            $req->user()->wallet()->add(($req->sum * -1), $req, 2);
            $req->state = 1;
        }
        $req->status_id = $request->status_id;
        $req->save();
        return redirect(route('admin.widthdrawRequest', ['id' => $id]));
    }

    public function git() {
        $message = '';
        if(\request()->isMethod('post')) {
            $message .= `cd /var/www/www-root/data/www/gipo.ru/`;
            $message .= `git pull`;
            dd($message);
        }
        return view('admin.git', ['message' => $message]);
    }

}
