<?php

namespace App\Http\Controllers;

use App\UserWebsite;
use Illuminate\Http\Request;

class UserWebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserWebsite  $userWebsite
     * @return \Illuminate\Http\Response
     */
    public function show(UserWebsite $userWebsite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserWebsite  $userWebsite
     * @return \Illuminate\Http\Response
     */
    public function edit(UserWebsite $userWebsite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserWebsite  $userWebsite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserWebsite $userWebsite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserWebsite  $userWebsite
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserWebsite $userWebsite)
    {
        //
    }
}
