<?php

namespace App\Http\Controllers;

use App\Order;
use App\UserWebsite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $user = Auth::user();
        if($user != null) {
            switch ($user->role_id) {
                case 0:
                    return redirect('admin');
                default:
                    return $this->home();
            }
        }
    }

    public function home() {
        $user_id = Auth::user()->id;
        $cards['new_orders'] = Order::where('status_id', 1)->whereIn('website_id', UserWebsite::select('id')->where('user_id', $user_id)->get())->count();
        $cards['all_orders'] = Order::whereIn('website_id', UserWebsite::select('id')->where('user_id', $user_id)->get())->count();
        $cards['months_orders'] = json_encode($this->getMonthsOrders());

        return view('home', ['cards' => $cards]);
    }

    private function getMonthsOrders() {
        $m = Carbon::now()->month;
        $min = $this->calcMonths($m,  6);

        $mn = [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' ];
        $months = [];

        for($i = 0; $i < 12; $i++) {
            $m = $this->calcMonths($min, $i);
            $months[$m] = $mn[$m - 1];
        }
        $query = "";
        foreach ($months as $m => $name) {
            $query .= "(SELECT COUNT(*) FROM `orders` WHERE `created_at` BETWEEN '2019-".($m)."-01' AND '2019-".($m + 1)."-01') as `".($name)."`, ";
        }
        $query = substr($query, 0, -2);
        return json_encode(DB::table('users')->select(DB::raw($query))->first());
    }

    private function calcMonths($month, $plus) {
        if($month + $plus < 1) {
            return 12 + ($month + $plus);
        } else if($month + $plus > 12) {
            return ($month + $plus) - 12;
        } else
            return $month + $plus;
    }

    public function new() {
        if(Auth::user()->status_id != 1)
            return redirect(route('agent.home'));
        dd('новый пользователь');
    }

    public static function getLMenus() {
        $websites = [];
        foreach (Auth::user()->websites() as $ws) {
            $websites[] = [
                'name' => $ws->name,
                'link' => route('agent.website', [ 'id' => $ws->id ]),
            ];
        }
        $websites[] = [
            'name' => 'Создать новый',
            'link' => route('agent.website.create'),
            'icon' => 'plus-circle',
        ];
        $items = [
            [
                'name' => 'Главная',
                'link' => route('agent.home'),
                'icon' => 'home',
            ],
            [
                'name' => 'Мои сайты',
                'link' => 'sites',
                'icon' => 'globe',
                'items' => $websites,
            ],
//            [
//                'name' => 'Товары',
//                'link' => 'menu-products',
//                'icon' => 'box',
//                'items' => [
//                    [
//                        'name' => 'Аналитика цен',
//                        'link' => route('agent.product.analytics'),
//                        'icon' => 'chart-area',
//                    ]
//                ]
//            ],
            [
                'name' => 'Управление балансом',
                'link' => route('agent.wallet'),
                'icon' => 'wallet',
            ],
            [
                'name' => 'Партнеры',
                'link' => route('agent.partners'),
                'icon' => 'handshake',
            ],
            [
                'name' => 'Настройки',
                'link' => 'menu-settings',
                'icon' => 'cog',
                'items' => [
                    [
                        'name' => 'Документы',
                        'link' => route('agent.settings.documents'),
                        'icon' => 'id-card',
                    ]
                ]
            ],
        ];
        return $items;
    }

    public function partners(Request $request) {
        $partners = Auth::user()->partners();
        $websitesCount = UserWebsite::whereIn('user_id', array_pluck($partners, 'id'))->where('status_id', 3)->count();
        return view('agent.partners', ['partners' => $partners, 'wc' => $websitesCount]);
    }

}
