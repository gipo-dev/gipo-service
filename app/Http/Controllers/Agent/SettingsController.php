<?php

namespace App\Http\Controllers\Agent;

use App\Image;
use App\UserDocument;
use App\UserRequiredDocument;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use UploadImage;

class SettingsController extends Controller
{
    public function documents() {
        $_documents = Auth::user()->documents()->get();
        $_documents = array_pluck($_documents, 'value', 'document_id');
        $required_documents = UserRequiredDocument::where('juristic_type', Auth::user()->legal_entity)->get();
        return view('agent.settings_document', ['user' => Auth::user(), 'documents' => $_documents,
                                                        'required_documents' => $required_documents]);
    }

    public function documentsUpdate(Request $request) {
        foreach ($request->request as $k => $value) {
            if($k == '_token')
                continue;
            UserDocument::updateOrCreate([
                'user_id' => Auth::user()->id,
                'document_id' => $k,
            ], [
                'value' => $value,
            ]);
        }
        if($request->passport_scan && $request->passport_scan != '') {
            UserDocument::updateOrCreate([
                'user_id' => Auth::user()->id,
                'document_id' => 22,
            ], [
                'value' => Image::upload($request->passport_scan),
            ]);
        }
        return redirect(route('agent.settings.documents'));
    }

    public function changeJuristic() {
        $user = Auth::user();
        $user->legal_entity = !$user->legal_entity;
        $user->save();
        return redirect(route('agent.settings.documents'));
    }
}
