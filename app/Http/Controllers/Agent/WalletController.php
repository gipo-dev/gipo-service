<?php

namespace App\Http\Controllers\Agent;

use App\Telegram\TelegramBot;
use App\Transaction;
use App\WidthdrawRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    public function index() {
        $transations = Transaction::select('transactions.*', 'tt.name as transaction_name')
            ->join('transaction_type as tt', 'tt.id', '=', 'transactions.transaction_type_id')
            ->where('transactions.user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(10);
        $widthdraw_request = WidthdrawRequest::whereUser(Auth::user()->id)->where('status_id', 1)->first();
        $widthdraw_requests = WidthdrawRequest::whereUser(Auth::user()->id)->select('widthdraw_requests.*', 'wrs.name as status_name')
            ->join('widthdraw_request_state as wrs', 'widthdraw_requests.status_id', '=', 'wrs.id')->orderBy('id', 'desc')->get(5);
        return view('agent.wallet', ['transactions' => $transations, 'user' => Auth::user(), 'request' => $widthdraw_request, 'requests' => $widthdraw_requests]);
    }

    public function createWidtdrawRequest(Request $request) {
        $widthdraw_requests = WidthdrawRequest::whereUser(Auth::user()->id)->where('status_id', 1)->first();
        if($request->sum > Auth::user()->wallet()->balance() || $request->sum < 100 || $widthdraw_requests > 0) {
            return redirect(route('agent.wallet'));
        }
        $widthdrawRequest = new WidthdrawRequest();
        $widthdrawRequest->user_id = Auth::user()->id;
        $widthdrawRequest->sum = encrypt($request->sum);
        $widthdrawRequest->status_id = 1;
        $widthdrawRequest->state = 0;
        $widthdrawRequest->save();
        TelegramBot::sendMessage('Новая заявка на вывод средств от пользователя '. Auth::user()->first_name .' на сумму '. $request->sum .' руб.');
        return redirect(route('agent.wallet'));
    }
}
