<?php

namespace App\Http\Controllers\Agent;

use App\Filters\ProductsFilter;
use App\Product;
use App\UserWebsite;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function analytics(Request $request, $website_id)
    {
        if ($request->isMethod('POST')) {
            $website = UserWebsite::find($website_id);
            $client = new Client();
            return json_decode($client->request('POST', $website->f_url . '/index.php?route=extension/feed/yandex_market/mark', [
                'form_params' => [
                    'products' => $request->products,
                    'mark' => $request->mark,
                ],
            ])->getBody());
        }

        $categories = array_pluck(UserWebsite::find($request->route()->id)->getCategories(), 'category_id');
        $products = Product::whereIn('category_id', $categories)->where('enabled', 1);
        $products = (new ProductsFilter($products, $request))->apply()->groupBy('alias')->paginate(20);

        $website = UserWebsite::find($website_id);
        try {
            $client = new Client();
            $pr_ids = implode(', ', array_pluck($products, 'id'));
            try {
                $response = json_decode($client->request('POST', $website->f_url . '/index.php?route=extension/feed/yandex_market/products', [
                    'form_params' => [
                        'page' => 0,
                        'limit' => 500,
                        'where' => ' WHERE `gipu_id` IN (' . $pr_ids . ') ',
                    ],
                ])->getBody());
//                dd($response);
                if(isset($response->products)) {
                    foreach ($response->products as $pr) {
                        $k = array_keys(array_column($products->items(), 'id'), $pr->gipu_id);
                        if (!empty($k))
                            $products[$k[0]]->marked = $pr->marked;
                    }
                }
            } catch (\Exception $ex) {}
        } catch(Exception $ex) {}

        return view('agent.products_analytics', ['products' => $products]);
    }

    public function yml(Request $request, $website_id)
    {
        $website = UserWebsite::find($website_id);

        if ($request->isMethod('POST')) {
            $_data = $request->post();
            foreach ($_data as $k => $v)
                if ($v == null)
                    $_data[$k] = '';

            if ($request['type'] == 'products') {
                $client = new Client();
                $response = json_decode($client->request('POST', $website->f_url . '/index.php?route=extension/feed/yandex_market/' . $_data['to'], [
                    'form_params' => $_data,
                ])->getBody());

                $_products = array();
                foreach ($response->products as $pr) {
                    $pr->price = round($pr->price);
                    $pr->marked = $pr->marked == '1' ? true : false;
                    $_products[$pr->gipu_id] = $pr;
                }

                $response->products = Product::whereIn('id', array_pluck($response->products, 'gipu_id'))->get();
                foreach ($response->products as $k => $product) {
                    $response->products[$k]->opt_price = $response->products[$k]->agent_price;
                    unset($response->products[$k]->agent_price);
                    $response->products[$k]->data = $_products[$product->id];
                }
                return json_encode($response);
            } else {
                $client = new Client();
                $response = json_decode($client->request('POST', $website->f_url . '/index.php?route=extension/feed/yandex_market/' . $_data['to'], [
                    'form_params' => $_data,
                ])->getBody());
                return $response;
            }
        } else {
            return view('agent.yml', ['website' => $website]);
        }
    }

    public function ymlRequest(Request $request, $website_id)
    {

    }
}
