<?php

namespace App\Http\Controllers\Agent;

use App\Order;
use App\Product;
use App\Telegram\TelegramBot;
use App\UserSettingValue;
use App\UserWebsite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class WebsiteController extends Controller
{

    public function index($id)
    {
        $website = UserWebsite::where('id', $id)->first();
        $orders = $website->getOrders(10);
        $settings['opt_markup'] = Auth::user()->setting('1');
        return view('agent.website', ['website' => $website, 'orders' => $orders, 'settings' => $settings]);
    }

    public function generateApiToken($id) {
        $website = UserWebsite::where('id', $id)->first();
        $website->generateApiKey();
        return redirect(route('agent.website', ['id' => $id]));
    }

    public function createPage()
    {
        return view('agent.tariff', ['tariffs' => self::getTariffs()]);
    }

    public function setup(Request $request)
    {
        $tariff = Input::get('tariff');
        if (!$tariff)
            return redirect(route('agent.website.create'));
        return view('agent.websiteInitialSetup', ['tariff' => $tariff]);
    }

    public function selectCategories(Request $request)
    {
        if (!$request->name)
            return redirect(route('agent.website.create'));
        $id = UserWebsite::create([
            'url' => $request->url,
            'name' => $request->name,
            'tariff_id' => $request->tariff,
        ]);
        TelegramBot::sendMessage('Пользователь '. Auth::user()->first_name .' создал новый сайт '. $request->url .' на тарифе '. $request->tariff);
        return redirect(route('agent.website', ['id' => $id]));
    }

    public static function getTariffs()
    {
        $tariffs = [];
        $_tariffs = DB::table('website_tariff')->get();
        $_features = DB::table('website_tariff_features')->get();
        foreach ($_tariffs as $t) {
            $tariffs[$t->id] = [
                'name' => $t->name,
                'description' => $t->description,
                'price' => $t->price,
            ];
        }
        foreach ($_features as $f) {
            $tariffs[$f->tariff_id]['features'][] = [
                'name' => $f->name,
                'description' => $f->description,
            ];
        }
        return $tariffs;
    }

    public function prolong($website_id, $time = 12) {

    }

    public function setOptMarkup(Request $request, $id) {
        if($request->markup < 0) {
            $request->markup = 0;
        }
        UserSettingValue::updateOrCreate(
            ['user_id' => Auth::user()->id, 'setting_id' => 1],
            ['value' => $request->markup]
        );
        return redirect(route('agent.website', $id));
    }

}
