<?php

namespace App\Http\Middleware;

use App\UserWebsite;
use Closure;
use Illuminate\Support\Facades\Auth;

class isUserWebsite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $owner = UserWebsite::getOwnerId($request->route('id'));
        if(Auth::user()->id != $owner)
            return redirect(route('agent.home'));
        return $next($request);
    }
}
