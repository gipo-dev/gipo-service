<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class documents
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if($user->status_id == 1) {
            return redirect(route('agent.new'));
        }

        if(!$user->documents_confirmed && $request->getRequestUri() != '/home/settings/documents') {
            return redirect(route('agent.settings.documents'));
        }
        return $next($request);
    }
}
