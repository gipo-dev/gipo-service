<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ApiController;
use App\User;
use App\UserWebsite;
use Closure;

class apiKeyValidator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ws = null;
        if(!$request->api_key) {
            return ApiController::errorMessage('Неправильный ключ');
        } else {
            $ws = UserWebsite::where('api_token', 'like binary', $request->api_key)
                ->where('status_id', '=', '3')->first();
            if(!$ws) {
                return ApiController::errorMessage('Неправильный API-ключ.');
            }
            $request->website = $ws;
            $request->user = User::find($ws->user_id);
        }

        return $next($request);
    }
}
