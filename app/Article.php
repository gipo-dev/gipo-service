<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    //получение автора
    public static $authors = [];
    public function getAuthorNameAttribute() {
        if(!isset(self::$authors[$this->author_id]))
            self::$authors[$this->author_id] = User::find($this->author_id);
        return self::$authors[$this->author_id]->first_name.' '.self::$authors[$this->author_id]->second_name;
    }

    public function getTextAttribute() {
        if(isset($this->attributes['text'])) {
            $text = $this->attributes['text'];
            $pos = strripos($text, '<p data-');
            if($pos)
                $text = substr($text, 0, $pos);
        } else
            $text = '';
        return $text;
    }

    public function category() {
        return $this->belongsTo('App\ArticleCategory', 'category_id');
    }

    public function tags() {
        return $this->belongsToMany('App\ArticleTag', 'article_to_tag', 'article_id', 'tag_id');
    }

    public function comments() {
        return $this->hasMany('App\ArticleComment', 'article_id');
    }
}

class ArticleCategory extends Model
{
    protected $table = 'article_category';

    public $timestamps = false;

    public function articles() {
        return $this->hasMany('App\Article', 'category_id');
    }
}

class ArticleTag extends Model
{
    protected $table = 'article_tag';

    public $timestamps = false;

    public function articles() {
        return $this->belongsToMany('App\Article', 'article_to_tag', 'tag_id', 'article_id');
    }
}

class ArticleToTag extends Model
{
    protected $table = 'article_to_tag';

    public $timestamps = false;
}

class ArticleComment extends Model
{
    protected $table = 'article_comment';
}