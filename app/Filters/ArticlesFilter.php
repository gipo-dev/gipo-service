<?php

namespace App\Filters;

class ArticlesFilter extends QueryFilter
{
    public function category($value) {
        $this->builder->where('category_id', $value);
    }
}