<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Model;

class ProductsFilter extends QueryFilter
{
    public function name($value) {
        $this->builder->where('name', 'like', "%$value%");
    }

    public function method($value) {
        if($value == 1)
            $this->builder->where('yandex_minimal_price', '>', 0)->orderByRaw('(`agent_price` / `yandex_minimal_price`)', 'ASC');
        else if($value == 2)
            $this->builder->orderByRaw('(`agent_price` - `yandex_minimal_price`)', 'ASC');
        else
            return;
    }
}