<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceList extends Model
{
    protected $connection = 'pricelists';

//    protected $table = 'provider';

    public function products() {
        return PriceListProduct::find(1);
    }
}

class PriceListProduct extends Model
{
    protected $connection = 'pricelists';

    protected $table = 'product';

    public function owner() {
        return $this->belongsTo('App\PriceListProductOwner', 'product_id', 'id');
    }
}

class PriceListProductOwner extends Model
{
    protected $connection = 'pricelists';

    protected $table = 'product_owners';

    public function product() {
        return $this->hasOne('App\PriceListProduct', 'id', 'product_id');
    }
}

class PriceListProvider extends Model
{
    protected $connection = 'pricelists';

    protected $table = 'provider';
}

class PriceListCategoryAlias extends Model {

    protected $connection = 'pricelists';
    protected $table = 'price_category_alias';

    public $timestamps = false;
}

class PriceListProductNew extends Model {
    protected $connection = 'pricelists';
    protected $table = 'price_product_new';

    public $timestamps = false;
}