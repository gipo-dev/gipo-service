<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    private $wallet = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'second_name', 'phone', 'email', 'password', 'last_activity', 'referral'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPhoneAttribute($val)
    {
        $this->attributes['phone'] = '7' . $val;
    }

    public function getLastActivityAttribute() {
        return new Carbon($this->attributes['last_activity']);
    }

    public function getIsOnlineAttribute() {
        return $this->last_activity->diffInMinutes(Carbon::now()) < 5;
    }

    public $documents = null;
    public $websites = null;

    public function documents()
    {
        return $this->hasMany('App\UserDocument');
    }

    public function websites()
    {
        if ($this->websites)
            return $this->websites;
        $this->websites = DB::table('users_websites as uw')->join('website_status as ws', 'ws.id', '=', 'uw.status_id')
            ->select('uw.*', 'ws.name as status_name')->where('user_id', '=', $this->id)->orderBy('id')->get();
        return $this->websites;
    }

    public function wallet()
    {
        if (!$this->wallet) {
            $this->wallet = new UserWallet($this);
        }
        return $this->wallet;
    }

    public function status() {
        return $this->belongsTo('App\UserStatus');
    }

    public function setting($id, $user_id = null) {
        if($user_id == null)
            $user_id = Auth::user()->id;
        $setting = UserSetting::find($id);
        $t = UserSettingValue::where('setting_id', $setting->id)->where('user_id', $user_id)->first();
        $setting->value = $t != null ? $t->value : $t;
        return $setting;
    }

    public function partners() {
        return User::where('referral', Auth::user()->id)->get();
    }

    public function isAdmin() {
        if(Auth::user() && Auth::user()->role_id == 0)
            return true;
        else
            return false;
    }

}

class UserDocument extends Model
{
    protected $table = 'users_documents';

    protected $fillable = array('user_id', 'document_id', 'value');

    public $timestamps = false;
}

class UserRequiredDocument extends Model
{
    protected $table = 'users_required_documents';

    public $timestamps = false;
}

class UserStatus extends Model
{
    protected $table = 'users_status';

    public $timestamps = false;
}

class UserSetting extends Model
{
    protected $table = 'user_settings';

    public $timestamps = false;
}

class UserSettingValue extends Model
{
    protected $table = 'user_setting_values';

    protected $fillable = ['user_id', 'setting_id', 'value'];

    public $timestamps = false;
}