<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $connection = 'main';
    protected $table = 'category';

    public function makeTree($ws, $categories = null, $category_id = 0) {
        if(!$categories) {
            if(request()->isMethod('POST'))
                $website_categories = request()->website->getCategories();
            else
                $website_categories = $ws->getCategories();
            $_categories = $this->where('active', '=', 1)->get();
            foreach ($_categories as $c) {
                $categories[$c->id] = $this->makeValues($c);
                if(isset($website_categories[$c->id])) {
                    $categories[$c->id]['selected'] = true;
                }
            }
            $properties = $this->getProperties(array_pluck($categories, 'id'));
            foreach ($properties as $prop) {
                $categories[$prop->category_id]['properties'][] = [
                    'id' => $prop->id,
                    'name' => $prop->name,
                ];
            }
        }
        $_categories = [];
        foreach ($categories as $category) {
            if($category_id == $category['parent_id']) {
                $_c = $this->makeTree($ws, $categories, $category['id']);
                if(!empty($_c)) {
                    $category['sub'] = $_c;
                    $_categories[$category['id']] = $category;
                }
                else {
                    $_categories[$category['id']] = $category;
                }
            }
        }
        return $_categories;
    }

    public function makeSmallTree($ws, $categories = null, $category_id = 0) {
        if(!$categories) {
            if(request()->isMethod('POST'))
                $website_categories = request()->website->getCategories();
            else
                $website_categories = $ws->getCategories();
            $_categories = $this->whereIn('id', array_pluck($website_categories, 'category_id'))->where('active', '=', 1)->get();
            foreach ($_categories as $c) {
                $categories[$c->id] = $this->makeValues($c);
                if(isset($website_categories[$c->id])) {
                    $categories[$c->id]['selected'] = true;
                }
            }
            $properties = $this->getProperties(array_pluck($categories, 'id'));
            foreach ($properties as $prop) {
                $categories[$prop->category_id]['properties'][] = [
                    'id' => $prop->id,
                    'name' => $prop->name,
                ];
            }
        }
        $_categories = [];
        foreach ($categories as $category) {
            if($category_id == $category['parent_id']) {
                $_c = $this->makeSmallTree($ws, $categories, $category['id']);
                if(!empty($_c)) {
                    $category['sub'] = $_c;
                    $_categories[$category['id']] = $category;
                }
                else {
                    $_categories[$category['id']] = $category;
                }
            }
        }
        return $_categories;
    }

    public function prepare($categories) {
        $_categories = [];
        foreach ($categories as $c) {
            $_categories[$c->id] = $this->makeValues($c);
        }
        $properties = $this->getProperties(array_pluck($categories, 'id'));
        foreach ($properties as $prop) {
            $_categories[$prop->category_id]['properties'][] = [
                'id' => $prop->id,
                'name' => $prop->name,
            ];
        }
        return $_categories;
    }

    public function makeValues(Category $category) {
        $_category = [
          'id' => $category->id,
          'name' => $category->name,
          'sort' => $category->sort,
          'parent_id' => $category->parent_id,
          'alias' => $category->alias,
          'enabled' => $category->active,
          'properties' => [],
        ];
        if(isset($category->selected))
            $_category['selected'] = $category->selected;
        return $_category;
    }

    public function getProperties(array $categories) {
        $properties = DB::connection('main')->table('properties')->select('id', 'name', 'category_id', 'main')->where('active', '=', 1)
            ->whereIn('category_id', $categories)->get();
        return $properties;
    }

    public function properties() {
        $_properties = DB::connection('main')->table('properties')->select('id', 'name', 'category_id', 'main')->where('category_id', $this->id)->get();
        $properties = [];
        foreach ($_properties as $prop) {
            $properties[$prop->id] = [
                'id' => $prop->id,
                'name' => $prop->name,
                'category_id' => $prop->category_id,
                'main' => $prop->main,
            ];
        }
        return $properties;
    }
}