<?php

namespace App;

use App\Filters\ProductsFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $connection = 'main';
    protected $table = 'products';
    public $timestamps = false;

    private $_provider_info = null;

    public function getImagesAttribute() {
        return $this->hasMany('app\ProductImage', 'product_id')->get();
    }

    public function getImageAttribute() {
        $img = $this->hasOne('app\ProductImage', 'product_id')->first();
        if($img) {
            return $img->image;
        }
        return '';
    }

    public function getNameAttribute() {
        return trim($this->attributes['name']);
    }


    public static $markup = null;
    public function makeValues() {
        if(self::$markup == null) {
            self::$markup = request()->user->setting('1', request()->user->id);
        }
        $markup = self::$markup;
        if($markup->value != null && $markup->value != 0) {
            $_price = $this->agent_price * ((100 + $markup->value) / 100);
        } else {
            $_price = $this->recommended_price;
        }
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category_id' => $this->category_id,
            'price' => $_price,
            'purchase_price' => $this->agent_price,
            'images' => array_pluck($this->images, 'image'),
            'alias' => $this->alias,
            'availability' => $this->count,
            'status' => $this->status_id,
        ];
    }
    public function makeAllValues() {
        if(self::$markup == null) {
            self::$markup = request()->user->setting('1', request()->user->id);
        }
        $markup = self::$markup;
        if($markup->value != null && $markup->value != 0) {
            $_price = $this->agent_price * ((100 + $markup->value) / 100);
        } else {
            $_price = $this->recommended_price;
        }
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category_id' => $this->category_id,
            'price' => $_price,
            'purchase_price' => $this->agent_price,
            'images' => array_pluck($this->images, 'image'),
            'alias' => $this->alias,
            'availability' => $this->count,
            'status' => $this->status_id,
        ];
    }

    public function properties() {
        $_properties = DB::connection('main')
            ->table('property_value as pv')
            ->select('pv.*', 'pe.value as e_value')
            ->leftJoin('property_enum as pe', 'pe.id', '=', 'pv.value_enum')
            ->where('product_id', $this->id)->get();
        // dd($_propertires);
        $properties = [];
        foreach ($_properties as $prop) {
            $properties[$prop->id] = [
                'id' => $prop->id,
                'property_id' => $prop->property_id,
                'product_id' => $prop->product_id,
                'value' => $prop->type == 'enum' ? $prop->e_value : $prop->value,
                'description' => $prop->description,
            ];
        }
        return $properties;
    }

    public function getProviderInfoAttribute() {
        $this->_provider_info;
        if($this->_provider_info)
            return $this->_provider_info;
        else
            $this->_provider_info = ProductProvider::where('gipo_index', $this->id)->first();
//            ->join('product_owners', 'product.id', '=', 'product_owners.product_id');
        return $this->_provider_info;
    }


}

class ProductImage extends Model {
    protected $connection = 'main';
    protected $table = 'product_images';

    public function getImageAttribute() {
        return trim($this->attributes['image']);
    }
}

class ProductProvider extends Model {
    protected $connection = 'pricelists';
    protected $table = 'product';

    public function getNameAttribute() {
        return $this->attributes['Name'];
    }
}

class ProductProperties extends Model {
    protected $connection = 'main';
    protected $table = 'properties';
    public $timestamps = false;
}

class ProductPropertyValue extends Model {
    protected $connection = 'main';
    protected $table = 'property_value';
    public $timestamps = false;
}