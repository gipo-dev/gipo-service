<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Email extends Model
{
    public $to;
    public $from = 'info@gipo.ru';
    public $fromName = 'Информация от gipo.ru';
    public $subject;
    public $text;

    public function send() {
        if(!$this->to)
            return false;
        Mail::send(['text' => $this->text], ['name' => $this->subject], function ($message) {
            $message->to($this->to)->subject($this->subject);
            $message->from($this->from, $this->fromName);
        });
    }
}
