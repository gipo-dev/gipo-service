<?php

namespace App;

use Carbon\Carbon;
use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserWebsite extends Model
{
    protected $table = 'users_websites';

    public $data;

    public function getPaidToAttribute() {
        return new Carbon($this->attributes['paid_to']);
    }

    public function getTariffNameAttribute() {
        return UserWebsiteTariff::find($this->tariff_id)->name;
    }

    public function getStatusNameAttribute() {
        return UserWebsiteStatus::find($this->status_id)->name;
    }

    public function getFUrlAttribute() {
        return ($this->attributes['https'] ? 'https://' : 'http://').$this->attributes['url'];
    }

    public static function getList() {
        return DB::table('users_websites as uw')->join('website_status as ws', 'uw.status_id', '=', 'ws.id')->select('uw.*', 'ws.name as status_name')->paginate(20);
    }

    public static function create($data) {
        DB::table('users_websites')->insert([
            'user_id' => Auth::user()->id,
            'url' => $data['url'],
            'name' => $data['name'],
            'tariff_id' => $data['tariff_id'],
            'status_id' => 1,
        ]);
        return DB::getPdo()->lastInsertId();
    }

    public static function getOwnerId($siteId) {
        $resp = DB::table('users_websites')->select('user_id')->where('id', '=', $siteId)->first();
        if(isset($resp->user_id)) {
            return $resp->user_id;
        } else {
            return null;
        }
    }

    public static function getStatusList() {
        return DB::table('website_status')->get();
    }

    public function setCategories($categories) {
        DB::table('users_website_categories')->where('website_id', '=', $this->id)->delete();
        $_categories = [];
        foreach ($categories as $category) {
            $_categories[] = [
                'website_id' => $this->id,
                'category_id' => $category,
            ];
        }
        DB::table('users_website_categories')->insert($_categories);
    }

    public function categories() {
        $categories = Category::find(array_pluck(DB::table('users_website_categories')->where('website_id', '=', $this->id)->get(), 'category_id'));
        $c = new Category();
        $categories = $c->makeSmallTree($c->prepare($categories));
        return $categories;
    }

    public function getCategories() {
        $categories = DB::table('users_website_categories')->where('website_id', '=', $this->id)->get();
        $_categories = [];
        foreach ($categories as $category) {
            $_categories[$category->category_id] = $category;
        }
        return $_categories;
    }

    public function getProductsList() {
        $categories = array_pluck($this->getCategories(), 'category_id');
        return Product::where('status_id', '=', 1)->where('enabled', '=', 1)->where('count', '>', '0')->whereIn('category_id', $categories)->groupBy('alias')->orderBy('id')->get();
    }

    public function generateApiKey() {
        $this->api_token = substr(encrypt(Carbon::now()), 15, 30);
        $this->save();
    }

    public function getOrders($paginate = 20) {
        return Order::select('orders.*', 'os.name as status_name')
            ->join('order_status as os', 'orders.status_id', '=', 'os.id')
            ->where('orders.website_id', $this->id)
            ->orderBy('orders.id', 'desc')
            ->paginate($paginate);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

}

class UserWebsiteTariff extends Model{
    protected $table = 'website_tariff';
}

class UserWebsiteStatus extends Model{
    protected $table = 'website_status';
}