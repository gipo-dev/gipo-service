<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ccategory extends Model
{
    protected $connection = 'products';
    protected $table = 'category';

    public $timestamps = false;

    public static function makeTree($categories, $category_id = 0) {
        $_categories = [];
        foreach ($categories as $category) {
            if($category_id == $category->parent_id) {
                $_c = self::makeTree($categories, $category->id);
                if(!empty($_c)) {
                    $category->sub = $_c;
                    $_categories[$category->id] = $category;
                }
                else {
                    $_categories[$category->id] = $category;
                }
            }
        }
        return $_categories;
    }
}
