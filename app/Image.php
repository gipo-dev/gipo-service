<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Image extends File
{
    public static function upload($image, $addingPath = '/documents') {
        $path = parent::upload($image, $addingPath);
        return $path;
    }
}
