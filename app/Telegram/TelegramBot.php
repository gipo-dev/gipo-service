<?php

namespace App\Telegram;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class TelegramBot
{
    public static $token = "806195869:AAGF5n7XAeNDcZ0krpOMXFvo31Tq2PgWWQE";

    protected static function query($method, $params = []) {
        try {
            $url = "https://api.telegram.org/bot";
            $url .= self::$token;
            $url .= "/".$method;

            if(!empty($params)) {
                $url .= '?'. http_build_query($params);
            }

            $client = new Client([
                'verify' => false,
                'base_uri' => $url,
            ]);

            $result = $client->request('GET');
            return json_decode($result->getBody());
        } catch (\Exception $ex) {
            return null;
        }
    }

    public static function getUpdates() {
        $last_update = TelegramSettings::findByName('last_update')->value;
        $new_last_update = $last_update;

        $updates = self::query('getUpdates', [
            'offset' => $last_update + 1,
        ])->result;
        foreach ($updates as $update) {
            if($update->update_id > $new_last_update)
                $new_last_update = $update->update_id;
        }

        TelegramSettings::setByName('last_update', $new_last_update);
        return $updates;
    }

    public static function sendMessage($text, $chat_id = -332473511) {
        $response = self::query('sendMessage', [
            'text' => $text,
            'chat_id' => $chat_id,
        ]);
        return $response;
    }
}
class TelegramSettings extends Model {
    protected $table = 'telegram_bot';
    public $timestamps = false;

    public static function findByName($name) {
        return self::where('name', $name)->first();
    }

    public static function setByName($name, $val) {
        $p = self::where('name', $name)->first();
        $p->value = $val;
        $p->save();
    }
}
