<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curl extends Model
{
    public static function getPage($url) {
        $page = file_get_contents($url);
        $page = str_ireplace('/bitrix/', 'https://gufu.ru/bitrix/', $page);
        $page = preg_replace('#<div class="bitrix-footer">(.*?)</div>#', '', $page);
        return $page;
    }

    public static function sendRequest($url) {
        $user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}
